/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class NestedModelUtilDimo {

    @SuppressWarnings("unchecked")
    public static <X> X getNestedValue(ModelData model, String property) {
        return (X) getNestedValue(model, getPath(property));
    }

    @SuppressWarnings("unchecked")
    public static <X> X getNestedValue(ModelData model, List<String> paths) {
        Object obj = model.get(paths.get(0));
        if (paths.size() == 1) {
            return (X) obj;
        } else if (obj != null && obj instanceof ModelData) {
            List<String> tmp = new ArrayList<String>(paths);
            tmp.remove(0);
            return (X) getNestedValue((ModelData) obj, tmp);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public static <X> X setNestedValue(ModelData model, String property, Object value) {
        return (X) setNestedValue(model, getPath(property), value);
    }

    @SuppressWarnings("unchecked")
    public static <X> X setNestedValue(ModelData model, List<String> paths, Object value) {
        int index = paths.size() - 1;
        String path = paths.get(index);
        paths.remove(index);
        ModelData m = getNestedValue(model, paths);
        return (X) m.set(path, value);
    }

    public static boolean isNestedProperty(String property) {
        return property != null && property.contains(".");
    }

    private static List<String> getPath(String property) {
        return new ArrayList<>(Arrays.asList(property.split("\\.")));
    }

    /**
     * se envian los doubles porque hay errores en la herramienta que se esta usando 
     * que se contrapola con muchos 
     */
    public static void serializeClient(List<BaseModelData> lstBMD) {
        lstBMD.stream().forEach((bmd) -> {
            serializeClientBMD(bmd);
        });
    }
    
    public static void serializeClientBMD(BaseModelData bmd) {
        Set<String> setBMD = (Set) bmd.getPropertyNames();
        Iterator<String> itera = setBMD.iterator();
        while (itera.hasNext()) {
            String strKey = itera.next();
            if (bmd.get(strKey) != null && bmd.get(strKey) instanceof Double) {
                bmd.set(PUN.DBL + PUN.SEPARAZ + strKey, bmd.get(strKey).toString());
                bmd.remove(strKey);
            }
        }
    }
          
    public static void serializeServer(List<BaseModelData> lstBMD) {
        lstBMD.stream().forEach((bmd) -> {
            serializeServerBMD(bmd);
        });
    }
    
    public static void serializeServerBMD(BaseModelData bmd) {       
        Set<String> setBMD = (Set)bmd.getPropertyNames();
        Iterator<String> itera = setBMD.iterator();
        while(itera.hasNext()) {          
            String strKey = itera.next();
            if(strKey!= null && strKey.split(PUN.SEPARAZ).length == 2 
                    && strKey.split(PUN.SEPARAZ)[0].equals(PUN.DBL)){
                bmd.set(strKey.split(PUN.SEPARAZ)[1], Double.valueOf(bmd.get(strKey).toString()));
                bmd.remove(strKey);
            }
        }  
    }
    
}
