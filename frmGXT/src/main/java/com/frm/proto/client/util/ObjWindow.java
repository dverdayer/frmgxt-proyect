/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.util;

import gwt.material.design.addins.client.window.MaterialWindow;
import gwt.material.design.client.base.MaterialWidget;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialFooter;

/**
 *
 * @author diego
 */
public class ObjWindow {
    
    private MaterialWindow materialWindow;
    private MaterialWidget materialContent;
    private MaterialFooter materialModalFooter;
    
    private MaterialButton materialButton;
    private MaterialButton materialButtonAceptar;

    public ObjWindow(MaterialWindow materialWindow, MaterialWidget materialContent, MaterialFooter materialModalFooter) {
        this.materialWindow = materialWindow;
        this.materialContent = materialContent;
        this.materialModalFooter = materialModalFooter;
    }

    public MaterialWindow getMaterialWindow() {
        return materialWindow;
    }

    public void setMaterialWindow(MaterialWindow materialWindow) {
        this.materialWindow = materialWindow;
    }

    public MaterialWidget getMaterialContent() {
        return materialContent;
    }

    public void setMaterialContent(MaterialWidget materialContent) {
        this.materialContent = materialContent;
    }

    public MaterialFooter getMaterialModalFooter() {
        return materialModalFooter;
    }

    public void setMaterialModalFooter(MaterialFooter materialModalFooter) {
        this.materialModalFooter = materialModalFooter;
    }   

    public MaterialButton getMaterialButton() {
        return materialButton;
    }

    public void setMaterialButton(MaterialButton materialButton) {
        this.materialButton = materialButton;
    }

    public MaterialButton getMaterialButtonAceptar() {
        return materialButtonAceptar;
    }

    public void setMaterialButtonAceptar(MaterialButton materialButtonAceptar) {
        this.materialButtonAceptar = materialButtonAceptar;
    }
        
}
