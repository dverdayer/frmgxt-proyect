/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

/**
 *
 * @author Diego
 */
public class PUN {


    public static final String CODICO_DEPENDENCIA_DOCUMENTO = "111111" ;
    public static final String BTN_TAREA_EXTERNA = "Grabar Tarea Externa";
    // session vencida no se ha cuadrado todavia en en nuevo framework
    public final static String SESSION_VENCIDA = "Session Vencida";
    // redireccion a la pagina principal   deberi ser redirecionada a una variable del index
    public static final String PAGE_INICIAL = "/login";
    public static final String PRM_REQ = "PRM_REQ";
    public static final String USD = "USD";
    public final static String WIDTH_PAGE = "WIDTH_PAGE";
    public final static String HEIGHT_PAGE = "HEIGHT_PAGE";
    public final static String NL_SERIAL = "SERIAL";
    public final static String NL_USER = "USER";
    public final static String NL_FLUJO = "FLUJO";
    public final static String NL_FLUJO_ANTE = "FLUJO_ANTE";
    public final static String SS_XML = "ssXml";
    public final static String SS_RECURSO = "ssRecurso";
    public final static String SS_TIPO_VENTANA = "ssTipoVentana";
    public final static String SS_VARIABLE = "ssVariable";
    public final static String NL_FLUJO_DOCUMENTO = "FLUJO_DOCUMENTO";
    public final static String VAR_FLUJO_DOCUMENTO = "nlFlujoDocumento";
    public final static String NL_APL = "APL";
    public final static String NL_APL_SOURCE = "APL_CS";
    public final static String NL_APL_TARGET = "APL_TT";

    

    // variables que hace pareja con el engine de los flujos
    public static final String ASIGNACION = "asignacion";
    public static final String ASIGNACION_DEMANDA = "asignacion_demanda";

    // parametros de los  marcos principales
    public static final String CENTER_PANEL = "centerPanel";
    public static final String WEST_PANEL = "westPanel";
    public static final String NORTH_PANEL = "northPanel";
    public static final String VIEWPORT = "viewPort";
    public static final String LOGIN_PANEL = "loginPanel";


    public final static String BTN_LOAD = "LOAD";
    public final static String GRID_VTE = "GRID_VTE";
    public final static String _GRID_VTE = "GRID_VTE:";       

    public final static int CANTIDAD_PADDIN = 10;
    public final static String START_PADDIN = "start";
    public final static String PAGE_PADDIN = "limit";
    public final static int TAMANO_LABEL_FIELDS = 150;
    public final static int CONSTANTE_FORM = 50;
    public final static int CANTIDAD_PADDIN_H = 100;
    public final static int CANTIDAD_PADDIN_CBX = 20;
    public final static int WIDTH_PADDIN_CBX = 250;
    public final static String TOTAL_COUNT  = "totalCount";
    
    public final static String MODULO = "MODULO";
//    public final static String FILTER = "FILTRO";
    public final static String FILTER_ = "FILTRO:";
    public final static String FILTERBTN_ = "FILTERBTN_";
    public final static String _FILTER = "y-yFILTRO";

    public final static String WIGET_FORM = "WIGET_FORM";
    public final static String WIGET_GRID = "WIGET_GRID";


    public final static String FORM_CB = "FORM_CB";
    public final static String FORM_CB_  = "FORM_CB:";

    public final static String MODULO_ACTUAL = "MODULO_ACTUAL";

    public final static String HDD= "HIDDEN";
    public final static String HDD_NU= "HIDDEN_NUM";

    public final static String XX= "XX";
    public final static String NN= "NN";
    public final static String NU= "NU";
    public final static String WW= "WW";
    public final static String PW= "PW";


    public final static String TXT = "TEXT";
    public final static String TXT_XX= "TEXT_XX";
    public final static String TXT_XX_PW= "TEXT_XX_PW";
    public final static String TXT_NN= "TEXT_NN";
    public final static String TXT_NN_PW= "TEXT_NN_PW";
    public final static String TXT_VI= "TEXT_VI";

    public final static String TEXT_AREA_ENTIDAD= "Text Area Entidad";
    public final static String SPINNER_ENTIDAD= "Spinner Entidad";

    public final static String PPP = "POPUP";
    public final static String PPP_XX= "POPUP_XX";
    public final static String PPP_NN= "POPUP_NN";
    public final static String PPE = "POPUPE";
    public final static String PPE_XX= "POPUPE_XX";
    public final static String POPUPE_NN= "POPUPE_NN";
    public final static String POPUP_NN_DD= "POPUP_NN_DD"; // custon
    public final static String POPUP_XX_DD= "POPUP_XX_DD"; // custon
    
    public final static String ESPEC= "ESPEC";
    public final static String ESPEC_XX= "ESPEC_XX";
    public final static String ESPEC_NN= "ESPEC_NN";
    
    public final static String TXN= "TXTNUM";
    public final static String TXN_XX_D0= "TXTNUM_XX_D0";
    public final static String TXN_XX_D1= "TXTNUM_XX_D1";
    public final static String TXN_XX_D2= "TXTNUM_XX_D2";
    public final static String TXN_XX_D3= "TXTNUM_XX_D3";
    public final static String TXN_XX_D4= "TXTNUM_XX_D4";
    public final static String TXN_XX_D5= "TXTNUM_XX_D5";
    public final static String TXN_NN_D0= "TXTNUM_NN_D0";
    public final static String TXN_NN_D1= "TXTNUM_NN_D1";
    public final static String TXN_NN_D2= "TXTNUM_NN_D2";
    public final static String TXN_NN_D3= "TXTNUM_NN_D3";
    public final static String TXN_NN_D4= "TXTNUM_NN_D4";
    public final static String TXN_NN_D5= "TXTNUM_NN_D5";

//    public final static String SPINNERINT = "SPINNERINT";
//    public final static String SPINNERINT_NN= "SPINNERINT_NN";
//    public final static String SPINNERINT_XX= "SPINNERINT_XX";
//
//    public final static String SPINNERANIO = "SPINNERANIO";
//    public final static String SPINNERANIO_NN= "SPINNERANIO_NN";
//    public final static String SPINNERANIO_XX= "SPINNERANIO_XX";
    



    public final static String TXTNUMT= "TXTNUMT";
    public final static String TXTNUMT_XX_DO= "TXTNUMT_XX_D0";

    public final static String DATE = "DATE";
    public final static String DATE_XX= "DATE_XX";
    public final static String DATE_NN= "DATE_NN";    

    public final static String TXA= "TEXTA";
    public final static String TXA_XX= "TEXTA_XX";
    public final static String TXA_NN= "TEXTA_NN";

    public final static String RADIO= "RADIO";
    public final static String RADIO_XX= "RADIO_XX";
    public final static String RADIO_NN= "RADIO_NN";

    public final static String RADIOB= "RADIOB";
    public final static String RADIOB_XX= "RADIOB_XX";
    public final static String RADIOB_NN= "RADIOB_NN";

    public final static String APLU = "APLU";
    public final static String ENTIDAD = "ENTIDAD";
    public final static String ENTIDAD_HIJO = "ENTIDAD_HIJO";
    public final static String ENTIDAD_XX= "ENTIDAD_XX";
    public final static String ENTIDAD_NN= "ENTIDAD_NN";
    public final static String ENTIDAD_NN_DD= "ENTIDAD_NN_DD";

    public final static String DISPARO= "DD";


    /*fue hecho para que viajaran todos los datos del formulario actual*/
    public final static String ENTIDADE = "ENTIDADE";
    public final static String ENTIDADE_HIJO = "ENTIDADE_HIJO";
    public final static String ENTIDADE_XX= "ENTIDADE_XX";
    public final static String ENTIDADE_NN= "ENTIDADE_NN";
    /*fin*/

    public final static String CBX= "COMBO";
    public final static String CBX_XX= "COMBO_XX";
    public final static String CBX_NN= "COMBO_NN";    
    
    public final static String CBE= "COMBOE";
    public final static String CBE_XX= "COMBOE_XX";
    public final static String CBE_NN= "COMBOE_NN";    
    
    
    public final static String LBX= "LISTACBX";
    public final static String LBX_XX= "LISTACBX_XX";
    public final static String LBX_NN= "LISTACBX_NN"; 
    public final static String REDIREC = "REDIREC";
    
    // campos definidos como no estandaress...
    public final static String COLOR= "COLOR";
    public final static String COLOR_NN= "COLOR_NN";


    public final static String _GRID_EMER = "_GRID_EMER:";
    public final static String _GRID_TRANS = "_GRID_TRANS:";
    public final static String _GRID_READ = "_GRID_READ:";
    public final static String _GRID_SAVE = "_GRID_SAVE:";
    
    public final static String GRID_EMER = "_GRID_EMER";
    public final static String GRID_TRANS = "_GRID_TRANS";
    public final static String GRID_READ = "_GRID_READ";
    public final static String GRID_SAVE = "_GRID_SAVE";
      


    public final static String _COMBO = "_COMBO:";
    public final static String _ENTIDAD = "_ENTIDAD:";
    public final static String _FORM = "_FORM:";
    public final static String _GRID = "_GRID:";
  
    public final static String _WIN = "_WIN:";
    public final static String _LST_STORE_DELE = "_LST_STORE_DELE:";
    public final static String LST_STORE_DELE = "_LST_STORE_DELE";
     public final static String _LST_STORE_DELE_INDEPEN = "_LST_STORE_DELE_INDEPEN";


    

    public final static String _FORM_TRANS = "_FORM_TRANSy-y";
    public final static String _FORM_TRANS_HIJO = "_FORM_TRANS_HIJOy-y";
//    public final static String _FORM_TRANS_HIJO_P = "_FORM_TRANS_HIJO_Py-y";

    public final static String FORM_TRANS = "_FORM_TRANS";
    public final static String FORM_TRANS_HIJO = "_FORM_TRANS_HIJO";
//    public final static String FORM_TRANS_HIJO_P = "_FORM_TRANS_HIJO";
    
    
    public final static String CHK_NN= "CHECK";
    public final static String CHK= "CHECK";


    /**Tipo de ventanas para los que se definieron en la clase navegacion TipoVentana
     */
    public final static String MAESTRO_TIPO_B= "MAESTRO_TIPO_B";
    public final static String MAESTRO_TIPO_F= "MAESTRO_TIPO_F";
    public final static String VENTANA_TIPO_F= "VENTANA_TIPO_F";
    public final static String VENTANA_TIPO_T= "VENTANA_TIPO_T";
    public final static String VENTANA_TIPO_E= "ENTIDAD_DINAMICO";
    public final static String VENTANA_TIPO_I= "FORMULARIO_DINAMICO";
    public final static String VENTANA_TIPO_P= "FLUJO_PROCESO";

    // ventanas emergentes    
    public final static String VENTAE_CARGAR= "CARGAR";
    public final static String VENTAE_CARGAR_CER= "CARGAR Y CERRAR";


    /*campso entity*/
    public final static String CAMPO_BUSQUEDA_COMBOX= "deEntidadCombox";

    public final static String URL_JSON_TREE = "sodia-war/ServJSONTree?";
    public final static String URL_JSON_LIST = "sodia-war/ServJSONList?";
    public final static String URL_JSON_LIST_TRANS = "sodia-war/ServJSONListTrans?";
    public final static String URL_JSON_COMBO_BOX_TOTAL = "sodia-war/ServJSONCombox?";
    public final static String URL_JSON_FORM = "sodia-war/ServJSONForm?";
    public final static String URL_JSON_COMBO_BOX_AJAX = "sodia-war/ServJSONComboxPading?";




    public final static String ID_COMBO_BOX = "idEntidadCombox";
    public final static String DE_COMBO_BOX = "deEntidadCombox";

    public final static String REG_NEW = "regNew";
    public final static String REG_UPDATE = "regUpdate";
    public final static String REG_DELETE = "regDelete";

    public final static String MENU_NEW = "Insertar";
    public final static String MENU_NEW_RP = "Insertar Reporte";
    public final static String MENU_NEW_DS = "Insertar Dash Board";
    public final static String MENU_UPDATE = "Actualizar";
    public final static String MENU_SET = "Set";
    public final static String MENU_DELETE = "Borrar";
    
    
    
    public final static String EVN_SID_NAV_CLOSE = "EVN_SID_NAV_CLOSE";
    public final static String EVN_SID_NAV_OPEN = "EVN_SID_NAV_OPEN";
    
    

    public final static String REPORTE_TIPO_DASH = "REPORTE-DASH";
    public final static String REPORTE_SPLE = "REPORTE-SPLE";

    public final static String ENTIDAD_PADRE = "ENTIDAD_PADRE";

    public final static String XML = "XML";
    public final static String XML_RECURSO = "XML_RECURSO";
    public final static String XML_CONTROL = "XML_CONTROL";
    public final static String CONTROLADOR = "CONTROLADOR";
    public final static String CONTROLADOR_MAESTRO = "CONTROLADOR_MAESTRO";

    public final static String ACCION = "ACCION";


    public final static String CAMPO_VISUAL_Y = "campoVisual";
    public final static String CAMPO_VISUAL_1 = "campoVisual1";
    public final static String CAMPO_VISUAL_2 = "campoVisual2";

    public static String CAMPO_VISUAL_INT1 = "campoVisulaInt1";
    public static String CAMPO_VISUAL_INT1INT = "campoVisulaInt1In";
    public static String CAMPO_VISUAL_INT2INT = "campoVisulaInt2In";


//    public final static String PK_REG = "PK_REG";
    
    public final static String PK_REG_NATIVE = "pkReg";
    public final static String PK_REG_NATIVE_F = "pkF";
    public final static String EST_REG = "estReg";
    public final static String COUNT_REG = "countReg";


    public final static String EST_REG_PERSISTENCE = "EST_REG_PERSISTENCE"; // cuando hay un registro nuevo
    public final static String EST_REG_MERGE = "EST_REG_MERGE";  // cuando hay una actualizacion de un campo que era persistente
    public final static String REG_NO_UPDATE = "regNoUpdate";  // cuando hay una actualizacion de un campo que era persistente
    public final static String EST_REG_BORRAR = "EST_REG_BORRAR"; // cuando se borra un registro
    public final static String PERSISTENCE = "PERSISTENCE"; // cuando el registro va del servidor a la vista
    public final static String EST_REG_VENTA_EMERGE = "EST_REG_VENTA_EMERGE"; // viene de una ventana emergente o de un control que onchange pero necesitan guardarse como nuevos
    public final static String EST_REG_VENTA_EMERGE_MERGE = "EST_REG_VENTA_EMERGE_MERGE"; // Cuando se actualiza algun controly el registro no se bora ni se crea nuevo.


    /////////////////////////////  tienen que ver con los combos para la trasmicion en el servlet   campos visuales comodines por si falta algo
    public final static String CAMPO_VISUAL = "CAMPO_VISUAL";
    public final static String CAMPOS_ENTIDAD = "CAMPOS_ENTIDAD";
    public final static String QUERY = "query";

    //seÃ±ales
    public final static String SING = "SING";
    public final static String NO_EVENT_CONTROLER = "NO_UPDATE";

    public final static String STR_USER_ACTUAL = "STR_USER_ACTUAL";
    public final static String CALL_BACK = "callback";


    // entidades dinamicas  o formualrios dinamicos
    // separadores
    public final static String SEPARA = "x-x";
    public final static String SEPARAY = "y-y";
    public final static String SEPARAZ = "z-z";

    // parametrso de tuplas de maps para los viajes
    public final static String KEY = "key";
    public final static String VISUAL = "visual";

    public final static String DIBUJO = "dibujo";
    public final static String CTR_MULTIP = "controlMulti";
    public final static String CTR_SIMPLE = "controlSimple";

    // parametros que estaban quemados por el multilenguage   contorles form dinamico ui
    public static String TEXT_FORM_UI_CL = "Text";
    public static String TEXT_FORM_UI_CAL = "Campo Calculado";
    public static String COMBOBOX_FORM_UI_CL = "Combo Box";
    public static String COMBOBOXENT_FORM_UI_CL = "Entidad";
    public static String COMBOBOXSCH_FORM_UI_CL = "Entidad Search";
    public static String RADIO_BTN_FORM_UI_CL = "Radio Button Form";
    public static String CHECK_BOX_FORM_UI_CL = "Check Box Form";
    public static String CHECK_BOX_UI_CL = "Check Box";
    public static String ADJUNTO = "Adjunto";
    public static String TEXTA_FORM_UI_CL= "Text Area";

    public static String CR_ALERTAS = "CR_ALERTAS";
    public static String CR_HORARIOS = "CR_HORARIOS";
    public static String CR_NOTIFICACIONES = "CR_NOTIFICACIONES";


    // los no alineados
    public static String BTN_CBX_CLEAN = "BtnCbxClean";
    public static String BTN_ADJUNTO = "BtnAdj";
    public static String BTN_ADJUNTO_CLEAR = "BtnAdjClean";
    
    // artrfactos flowchart    

    public final static String ENTI_DINA = "ENTI_DINA";
    public final static String FORM_DINA = "FORM_DINA";
    public final static String FLUJO_PROCESO = "FLUJO_PROCESO";
    public final static String MARCA_FLUJO = "MARCA_FLUJO";

    public final static String FLUJO_ENVIADO = "ENVIADO";
    public final static String FLUJO_APROBADO = "APROBADO";
    public final static String FLUJO_RECHAZADO = "RECHAZADO";
    public final static String FLUJO_TRASLADO = "TRASLADO";
    public final static String FLUJO_IGUAL = "IGUAL";


    public final static String UTIL_VIEW = "UTIL_VIEW";
    public final static String UTIL_VIEW_DINA_WF = "UTIL_VIEW_DINA_WF";
    public final static String VIEW = "VIEW";
    public final static String UTIL = "UTIL";


    public final static String TIPO_TERCERO_INTERNO = "INTERNO";
    
    
    public final static String FORMAT_DATE = "yyyy/MM/dd hh:mm:ss";
    
    public final static String SMALL = "s12 m6 l4";
    public final static String MEDIUM = "s12 m12 l6";
    public final static String LARGE = "s12 m12 l12";
    
    
    
    ///////////////////////////////////////// los ParamutilDina
    
    public static String TEXT_FORM_UI = "text";       
    public static String RADIO_FORM_UI = "radio";
    public static String COMBO_FORM_UI = "combo";
    public static String CHECK_FORM_UI = "check";
    public static String COMBOE_FORM_UI = "comboe";


    public static String GRID_FORM_UI = "grid";
    public static String FORME_FORM_UI = "formEn";    
    public static String GRIDE_FORM_UI = "gride";
    public static String FORMF_FORM_UI = "formf";

    //ventana emergente
    public static String CBXSRH_FORM_UI = "cbxsearch";

    public static String PREG_AB_UI = "pregab";
    public static String PREG_MA_UI = "pregma";
    public static String PREG_CE_UI = "pregce";
    public static String PREG_MU_UI = "pregmu";
    public static String PREG_DE_UI = "pregde";
    public static String PREG_CL_UI = "pregcl";

    

    public static String FIELD_FORM = "fiels";


    public static String CAMPO_DB_INT = "INT";
    public static String CAMPO_DB_VARCHAR = "VARCHAR";
    public static String CAMPO_DB_DATETIME = "DATETIME";
    public static String CAMPO_DB_DATE = "DATE";
    public static String CAMPO_DB_DECIMAL = "DECIMAL";

    public static String CAMPO_MY_FECHA = "Fecha";
    public static String CAMPO_MY_ALFA = "Alfanumerico";
    public static String CAMPO_MY_ENTERO = "Numero Entero";
    public static String CAMPO_MY_DECIMALE = "Numero Decimal";
    public static String CAMPO_MY_MONEDA = "Numero Moneda";


    public final static String EDITOR_DINAMICO = "Editor Dinamico";
    public final static String EDITOR_DINAMICO_FORM = "Editor Dinamico Form";


    public final static String DINA_DEPLOY_FORMULARIO = "KA_NL_FORMULARIO_DINAMICO";
    public final static String DINA_DEPLOY_ENTIDAD = "KA_NL_ENTIDAD";
    public final static String DINA_DEPLOY_EVALUACION = "KA_NL_EVALUACIONES";
    public final static String DINA_DEPLOY_REPORTE = "KA_NL_REPORTES";

    public final static String TIPO_VENTANA_ENTIDAD = "ENTIDAD_DINAMICO";
    public final static String TIPO_VENTANA_FORMULARIO = "FORMULARIO_DINAMICO";
    public final static String TIPO_VENTANA_EVALUACION = "EVALUACION_DINAMICO";
    public final static String TIPO_VENTANA_REPORTE = "REPORTE_DINAMICO";

    public final static String ENTIDAD_SEARCH = "Entidad Search";
    public final static String COMBO_BOX = "Combo Box";
    public final static String RADIO_BUTTON_FORM = "Radio Button Form";
    public final static String CHECK_BOX_FORM = "Check Box Form";
    public final static String CHECK_BOX = "Check Box";// este esel control boolean
    public final static String TEXT = "Text";
    public final static String TEXT_AREA = "Text Area";
    public final static String TEXT_CALCULADO = "Campo Calculado";
    public final static String TEXT_CALCULADO_MONEDA = "Campo Calculado Moneda";


    public final static String STRING = "String";
    public final static String NUMBER = "Number";
    public final static String COMPUESTO = "Compuesto";


    public static String TEXT_JS = "text";
    public static String RADIO_JS = "radio";
    public static String COMBO_JS = "combo";
    public static String CHECK_JS = "check_box";
    
    
    public static String TAB = "TAB";
    public static String TAB_ITEM = "TAB-ITEM";
    public static String MT_ROOT = "MT-ROOT";
   
    
    public final static String _PEST_TRANS1 = "_PEST_TRANS1:";
    public final static String _ITEM_TAB = "ITEM_TABy-y";    
    public final static String ITEM_TAB = "ITEM_TAB";  
    public final static String _ITEM_TAB_BODY = "ITEM_TAB_BODYy-y";
    public final static String ITEM_TAB_BODY = "ITEM_TAB_BODY";
    public final static String SELECT_ROW = "SELECT_ROW";
    
    public final static String KA_NL = "kaNl";
    public final static String SS_VALUE_CBX = "ssValueCbx";
    public final static String SS_ID_CBX = "ssIdCbx";
    

    public final static String DATA_VACIO_NO_ORIGEN = "DATA#$%VACIO$%&/NO/=/(ORIGEN";
    public final static String VENTANA_EMER = "VENTANA_EMER";
    public final static String VENTANA_EMER_FILTER = "VENTANA_EMER_FILTER";
    public final static String DBL = "elbuod";
    public final static String MATE_TAB = "MatTab";
    public final static String MATE_TABLE = "MatTable";
    public final static String MATE_TABLE_HIJO = "MatTableHijo";
}

