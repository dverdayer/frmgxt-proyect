/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.teble.sid;


import com.frm.proto.client.table.dto.DataPagerRemote;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IsWidget;
import gwt.material.design.client.ui.pager.MaterialDataPager;
import gwt.material.design.client.ui.pager.actions.PageListBox;
import gwt.material.design.client.ui.pager.actions.PageNumberBox;
import gwt.material.design.client.ui.pager.actions.PageSelection;

/**
 * Provides a selection control to {@link MaterialDataPager} pages.
 * Call {@link MaterialDataPager#setPageSelection(PageSelection)} for applying the component.
 * Two available components are available already, see {@link PageNumberBox} and {@link PageListBox}
 *
 * @author kevzlou7979
 */
public interface PageSelectionSID extends HasValue<Integer>, IsWidget {

    /**
     * Will load and initialize all handlers.
     */
    void load();

    /**
     * Will automatically updated the page number based on the current page
     */
    void updatePageNumber(int currentPage);

    /**
     * Clear or reset the page number
     */
    void clearPageNumber();

    /**
     * Will set the current {@link MaterialDataPager} for it's controller.
     */
    void setPager(DataPagerRemote<?> pager);

    /**
     * Will set the component label
     */
    void setLabel(String label);

    /**
     * Will set the total number of pages
     */
    void updateTotalPages(int totalPages);
}
