/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author diegohernandez
 */
public class EntidadIndepen implements IsSerializable{
    String strEntidad;
    String strRelacion;
    String strControlador;
    BaseModelData  bmd;

    public EntidadIndepen() {

    }
    public EntidadIndepen(String strEntidad, String strRelacion, String strControlador) {
        this.strEntidad = strEntidad;
        this.strRelacion = strRelacion;
        this.strControlador = strControlador;
    }
    
    public BaseModelData getBmd() {
        return bmd;
    }

    public void setBmd(BaseModelData bmd) {
        this.bmd = bmd;
    }    

    public String getStrEntidad() {
        return strEntidad;
    }

    public void setStrEntidad(String strEntidad) {
        this.strEntidad = strEntidad;
    }

    public String getStrRelacion() {
        return strRelacion;
    }

    public void setStrRelacion(String strRelacion) {
        this.strRelacion = strRelacion;
    }

    public String getStrControlador() {
        return strControlador;
    }

    public void setStrControlador(String strControlador) {
        this.strControlador = strControlador;
    }

    
}
