/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.util;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.FormaGUI;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.table.dto.DataPagerRemote;
import com.frm.proto.client.table.dto.EntidadCBX;
import com.frm.proto.client.ui.MaterialDataTableCuston;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.WidgetCollection;
import gwt.material.design.addins.client.autocomplete.MaterialAutoComplete;
import gwt.material.design.addins.client.combobox.MaterialComboBox;
import gwt.material.design.addins.client.fileuploader.MaterialFileUploader;
import gwt.material.design.addins.client.window.MaterialWindow;
import gwt.material.design.client.base.MaterialWidget;
import gwt.material.design.client.data.component.RowComponent;
import gwt.material.design.client.ui.MaterialCheckBox;
import gwt.material.design.client.ui.MaterialColumn;
import gwt.material.design.client.ui.MaterialContainer;
import gwt.material.design.client.ui.MaterialDatePicker;
import gwt.material.design.client.ui.MaterialDoubleBox;
import gwt.material.design.client.ui.MaterialListValueBox;
import gwt.material.design.client.ui.MaterialPanel;
import gwt.material.design.client.ui.MaterialRadioButton;
import gwt.material.design.client.ui.MaterialRow;
import gwt.material.design.client.ui.MaterialTab;
import gwt.material.design.client.ui.MaterialTextArea;
import gwt.material.design.client.ui.MaterialTextBox;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author diego
 */
public final class SGUI {
    
    public static BaseModelData bmdINI = new BaseModelData();
    static{
        bmdINI.set("Seleccione", "Seleccione ...");
    }
    
    public static MaterialRow getFormPanelByComponente(Widget w){
        if(w != null &&  w.getParent() != null){
            if(w.getParent() instanceof MaterialRow){
                return (MaterialRow)w.getParent();
            }else{
                return getFormPanelByComponente(w.getParent());
            }
        }
        return null;
    }
    
    public static DataPagerRemote buDataPagerRemoteByTable(MaterialDataTableCuston mdtc){
        WidgetCollection wc = mdtc.getChildren();
        for(int j=0; j<wc.size(); j++){
            Widget w = wc.get(j);            
            if(w instanceof DataPagerRemote){               
                return (DataPagerRemote)w;
            }
        }
        return null;
    }
    
    public static MaterialRow buFormVentaEmer(MaterialWidget mrModal){
        WidgetCollection wc;
        if(mrModal instanceof MaterialWindow){
             wc = ((MaterialWindow) mrModal).getContent().getChildren();
        }else{
             wc = mrModal.getChildren();
        }
        for(int j=0; j<wc.size(); j++){
            Widget w = wc.get(j);            
            if(wc.get(j) instanceof MaterialPanel){
                MaterialPanel mp = (MaterialPanel)wc.get(j);
                w = mp.getChildren().get(0);
            }
            if(w instanceof MaterialRow){
                MaterialRow mr = (MaterialRow)w;
                if(mr.getId() != null && (mr.getId().split(PUN.SEPARAY)[0].equals(PUN.FORM_TRANS)
                    || mr.getId().split(PUN.SEPARAY)[0].equals(PUN.FORM_TRANS_HIJO))
                ){
                    return mr;
                }
            }
        }
        return null;
    }
    
    static void borrarCampoDepen(MaterialRow mrForm, String strValueCampoBorrarFire){
        
        String vtValueCampoBorrar[] = strValueCampoBorrarFire.split("&");                  
        for(int i=0; i<vtValueCampoBorrar.length; i++){
            String idsR = vtValueCampoBorrar[i].replace(":", "_");
            String strEntidad = idsR.split("_")[0];
            
//            WidgetCollection wc = mrForm.getChildren();
            List<Widget> lstMC = mrForm.getChildrenList();
            for(Widget wc : lstMC){
                MaterialColumn mc = (MaterialColumn)wc;  
                
                if(mc.getChildrenList().size() == 1){ 
                
                    Widget w = mc.getChildrenList().get(0);
                    
                    if(w instanceof MaterialTextBox){
                        MaterialTextBox mt = (MaterialTextBox)w;                                        
                        if(mt.getId().split("_")[0].equals(strEntidad)){
                            mt.setValue(null);
                        }
                    }else if(w instanceof MaterialDoubleBox){
                        MaterialDoubleBox mt = (MaterialDoubleBox)w;                                        
                        if(mt.getId().split("_")[0].equals(strEntidad)){
                            mt.setValue(null);
                        }
                    }else if(w instanceof MaterialDatePicker){
                        MaterialDatePicker mt = (MaterialDatePicker)w;                                        
                        if(mt.getId().split("_")[0].equals(strEntidad)){
                            mt.setValue(null);
                        }
                    }else if(w instanceof MaterialTextArea){
                        MaterialTextArea mt = (MaterialTextArea)w;                                        
                        if(mt.getId().split("_")[0].equals(strEntidad)){
                            mt.setValue(null);
                        }
                    }else if(w instanceof MaterialComboBox){
                        MaterialComboBox mt = (MaterialComboBox)w;
                        if(mt.getId().split("_")[0].equals(strEntidad)){
                            mt.setValue(null);
                        }                    
                    }else if(w instanceof MaterialComboBox){
                        MaterialComboBox mt = (MaterialComboBox)w;
                        if(mt.getId().split("_")[0].equals(strEntidad) ||mt.getElement().getId().split("_")[0].equals(strEntidad)){
                            mt.setValue(null);
                        }                    
                    }else if(w instanceof MaterialAutoComplete){
                        MaterialAutoComplete mt = (MaterialAutoComplete)w;                                        
                        if(mt.getId().split("_")[0].equals(strEntidad)){
                            mt.setValue(null);
                        }
                    }else if(w instanceof MaterialListValueBox){
                        MaterialListValueBox mt = (MaterialListValueBox)w;                                        
                        if(mt.getId().split("_")[0].equals(strEntidad)){
                            mt.setValue(null);
                        }
                    }else if(w instanceof MaterialFileUploader){
                        MaterialFileUploader mt = (MaterialFileUploader)w;                                        
                        if(mt.getId().split("_")[0].equals(strEntidad)){
                            // pendiente para implememtacion
    //                        mt.setValue(null);
                        }
                    }else if(w instanceof MaterialRadioButton){
                        MaterialRadioButton mt = (MaterialRadioButton)w;                                        
                        if(mt.getName().split("_")[0].equals(strEntidad)){
                            // pendiente para implememtacion
                            mt.setValue(null);
                        }
                    }else if(w instanceof MaterialCheckBox){
                        MaterialCheckBox mt = (MaterialCheckBox)w;
                        if(mt.getElement().getId().split("_")[0].equals(strEntidad) || mt.getName().split("_")[0].equals(strEntidad)){
                            mt.setValue(null);
                        }
                    }
                }
            }
            
        }
    }        
    
    public static FormaGUI buGuiParent(Widget w){
        int i=0;
        while(w.getParent() != null && i < 10){
            if(w instanceof FormaGUI){
                return (FormaGUI)w;
            }else{
                w = w.getParent();
            }
            i++;
        }
        return null;
    }
    
    public static MaterialContainer buMCRootParent(Widget w){
        int i=0;
        while(w.getParent() != null && i < 10){
            if(w.getElement().getId().split(PUN.SEPARAY).length > 0 
                    && w.getElement().getId().split(PUN.SEPARAY)[0].equals(PUN.MT_ROOT)
                    && w instanceof MaterialContainer){
                return (MaterialContainer)w;
            }else{
                w = w.getParent();
            }
            i++;
        }
        return null;
    }
    
    public static MaterialDataTableCuston buMCTableParent(Widget w){        
        int i=0;
        while(w.getParent() != null && i < 10){
            if(w instanceof MaterialDataTableCuston){
                return (MaterialDataTableCuston)w;
            }else{
                w = w.getParent();
            }
            i++;
        }
        return null;
    }
    
    public static HTMLPanel buGRootParent(Widget w){
        int i=0;
        while(w.getParent() != null && i < 10){
            if(w instanceof HTMLPanel){
                return (HTMLPanel)w;
            }else{
                w = w.getParent();
            }
            i++;
        }
        return null;
    }
    
    public static MaterialTab buMTHeaderParent(Widget w){
        int i=0;
        while(w.getParent() != null && i < 10){
            if(w instanceof MaterialTab){
                return (MaterialTab)w;
            }else{
                w = w.getParent();
            }
            i++;
        }
        return null;
    }
    
    public static MaterialTab buMTHeaderChild(HTMLPanel gRoot){        
        int countWidget = gRoot.getWidgetCount();        
        for(int i=0; i<countWidget; i++){
            Widget w = gRoot.getWidget(i);            
            if(w instanceof MaterialTab){
                MaterialTab mt = (MaterialTab)w; 
                if(mt.getId().split(PUN.SEPARAY).length > 0 
                        && mt.getId().split(PUN.SEPARAY)[0].equals(PUN.TAB)
                        ){
                    return mt;
                }
            }
        }
        return null;
    }
    
    public static Widget buWidgetChild(MaterialWidget mwParent, String strId){
        List<Widget> lstW = mwParent.getChildrenList();        
        for(Widget w: lstW){
            if(w instanceof MaterialWidget){
                MaterialWidget mw = (MaterialWidget)w;
                if(mw instanceof MaterialColumn){                
                    Widget mw1 = buWidgetChild(mw, strId);
                    if(mw1 != null){
                        return mw1;
                    }                
                }else{
                    String strIdMW = mw.getId();
                    if(strIdMW.equals(strId)){
                        return mw;
                    }
                }
            }else if(w instanceof MaterialCheckBox){
                return w;  
            }
        }
        return null;
    }
    
    public static List<MaterialWidget> getListFormularios(MaterialWidget mwParent){        
        List<Widget> lstW = mwParent.getChildrenList();        
        List<MaterialWidget> lstForm = new ArrayList<>();        
        for(Widget w: lstW){            
            if(w instanceof MaterialRow){
                MaterialRow mr = (MaterialRow)w;
                if(mr.getId() != null && (mr.getId().split(PUN.SEPARAY)[0].equals(PUN.FORM_TRANS)
                    || mr.getId().split(PUN.SEPARAY)[0].equals(PUN.FORM_TRANS_HIJO))
                ){
                    lstForm.add(mr);
                }
            }
        }
        return lstForm;
    }
    
    public static List<MaterialWidget> getListGrid(MaterialWidget mwParent){        
        List<Widget> lstW = mwParent.getChildrenList();        
        List<MaterialWidget> lstForm = new ArrayList<>();        
        for(Widget w: lstW){            
            if(w instanceof MaterialDataTableCuston){
                MaterialDataTableCuston mth = (MaterialDataTableCuston)w;
                String strId = mth.getId();
                if(strId != null && (strId.split(PUN.SEPARA)[0].equals(PUN.MATE_TABLE_HIJO)) ){
                    lstForm.add(mth);                    
                }
            }
        }
        return lstForm;
    }
    
    
    public static MaterialRow buFormByWidget(Widget w){
        int i=0;
        while(w.getParent() != null && i < 10){            
            if(w.getParent() instanceof MaterialRow){                
                MaterialRow mr = (MaterialRow)w.getParent();
                if(mr.getId() != null && (mr.getId().split(PUN.SEPARAY)[0].equals(PUN.FORM_TRANS)
                    || mr.getId().split(PUN.SEPARAY)[0].equals(PUN.FORM_TRANS_HIJO))
                ){
                    return mr;
                }else{
                    w = w.getParent();
                }               
            }else{
                w = w.getParent();
            }
            i++;
        }
        return null;
    }
    
    public static MaterialWindow buModalByWidget(Widget w){
        int i=0;
        while(w.getParent() != null && i < 10){            
            if(w.getParent() instanceof MaterialWindow){                
               return (MaterialWindow)w.getParent();               
            }else{
                w = w.getParent();
            }
            i++;
        }
        return null;
    }
    
    public static BaseModelData getDataForm(MaterialRow mr){
        BaseModelData bmd = new BaseModelData();
        
        List<Widget> lstMC = mr.getChildrenList();
        for(Widget w : lstMC){
            MaterialColumn mc = (MaterialColumn)w;                            
            if(mc.getChildrenList().size() == 1){

                if(mc.getChildrenList().get(0) instanceof MaterialWidget){
                    MaterialWidget mvc = (MaterialWidget)mc.getChildrenList().get(0);

                    if(mvc instanceof MaterialDoubleBox){
                        MaterialDoubleBox mtb = (MaterialDoubleBox)mvc;
                        if(mtb.getValue() != null && !mtb.getValue().equals("")){
                            Double db = mtb.getValueAsNumber();
                            bmd.set(mtb.getId().replace("_", ":"), db);
                        }                                                
                    }else if(mvc instanceof MaterialTextBox){     
                        MaterialTextBox mtb = (MaterialTextBox)mvc;
                        if(mtb.getValue() != null && !mtb.getValue().equals("")){
                            bmd.set(mtb.getId().replace("_", ":"), mtb.getValue());
                        }                                                                                         
                    }else if(mvc instanceof MaterialTextArea){     
                        MaterialTextArea mtb = (MaterialTextArea)mvc;
                        if(mtb.getValue() != null && !mtb.getValue().equals("")){
                            bmd.set(mtb.getId().replace("_", ":"), mtb.getValue());
                        }                       
                    }else if(mvc instanceof MaterialDatePicker){     
                        MaterialDatePicker mtb = (MaterialDatePicker)mvc;
                        if(mtb.getValue() != null && !mtb.getValue().equals("")){
                            bmd.set(mtb.getId().replace("_", ":"), mtb.getValue());
                        }                      
                    }else if(mvc instanceof MaterialListValueBox){     
                        MaterialListValueBox mtb = (MaterialListValueBox)mvc;                        
                        if(mtb.getValue() != null && !mtb.getValue().equals("")){
                            bmd.set(mtb.getId().replace("_", ":"), mtb.getSelectedValue());
                        }                           
                    }
                }else if(mc.getChildrenList().get(0) instanceof Widget  
                    && mc.getChildrenList().get(0) instanceof MaterialCheckBox){     
                    MaterialCheckBox mtb = (MaterialCheckBox)mc.getChildrenList().get(0);
                    if(mtb.getValue() != null && mtb.getValue() ){
                        bmd.set(mtb.getElement().getId().replace("_", ":"), "S");
                    }else{
                        bmd.set(mtb.getElement().getId().replace("_", ":"), "N");
                    }                    
                }                                
            }else if(mc.getChildrenList().size() == 2){

            }else{

            }                            
        }    
        getDataFormDespu(mr, bmd);
        return bmd;
    }
    
    public static void getDataFormDespu(MaterialRow mr, BaseModelData bmd){
        
        List<Widget> lstMC = mr.getChildrenList();
        for(Widget w : lstMC){
            MaterialColumn mc = (MaterialColumn)w;                            
            if(mc.getChildrenList().size() == 1){

                if(mc.getChildrenList().get(0) instanceof MaterialWidget){
                    MaterialWidget mvc = (MaterialWidget)mc.getChildrenList().get(0);
                    
                    if(mvc instanceof MaterialListValueBox){     
                        MaterialListValueBox mtb = (MaterialListValueBox)mvc;                        
                        if(mtb.getValue() != null && !mtb.getValue().equals("")){
                            bmd.set(mtb.getId().replace("_", ":"), mtb.getSelectedValue());
                        }                           
                    }else if(mvc instanceof MaterialComboBox){     
                        MaterialComboBox mtb = (MaterialComboBox)mvc;                        
                        if(mtb.getSingleValue() != null && !mtb.getSingleValue().equals("")){
                            if(mtb.getSingleValue() instanceof BaseModelData){
                                BaseModelData bmdCompo = (BaseModelData)mtb.getSingleValue();                                             
                                
                                String vtIdsHTML = mtb.getId();
                                if(vtIdsHTML != null && vtIdsHTML.split("_").length == 2){
                                    String vtIds[] = vtIdsHTML.split("_");
                                    
                                    bmd.set(vtIdsHTML.replace("_", ":"), bmdCompo.get(vtIds[1]));  
                                    BaseModelData bmdIds = getIdsForm(mr);

                                    Collection<String> strBSKey = bmdCompo.getPropertyNames();
                                    Iterator<String> iteraKey = strBSKey.iterator();
                                    while(iteraKey.hasNext()){
                                        String strKey = iteraKey.next();
                                        String claveCompu = vtIds[0]+":"+strKey;
                                        if(bmdIds.get(claveCompu) != null){
                                            bmd.set(claveCompu, bmdCompo.get(strKey));
                                        }                                   
                                    }                                                               
                                }else if(vtIdsHTML != null && vtIdsHTML.split("_").length == 1){
                                    if(bmdCompo.get("ssIdCbx") != null && !bmdCompo.get("ssIdCbx").equals("")){
                                        bmd.set(vtIdsHTML.replace("_", ":"), bmdCompo.get("ssIdCbx")); 
                                    }else{
                                        bmd.set(vtIdsHTML.replace("_", ":"), null); 
                                    }
                                }else{
//                                    Error de desarrollo;
                                }                                
                            }else if(mtb.getSingleValue() instanceof String){
                                String strValue = (String)mtb.getSingleValue();
                                bmd.set(mtb.getId().replace("_", ":"), strValue);
                            }                            
                        }                           
                    }else if(mvc instanceof MaterialAutoComplete){     

                        MaterialAutoComplete mtb = (MaterialAutoComplete)mvc;
                        String strId =  mtb.getId().replace("_", ":");
                        if(mtb.getValue() != null && !mtb.getValue().isEmpty()  && !mtb.getValue().equals("")){
                            bmd.set(mtb.getId().replace("_", ":"), mtb.getValue().get(0).getDisplayString());
                            List<? extends SuggestOracle.Suggestion> lstSuge = mtb.getValue();
                            for(SuggestOracle.Suggestion su: lstSuge){
                                EntidadCBX cBX = (EntidadCBX)su;
                                BaseModelData bmd1 = cBX.getBmd();
                                
                            }
//                            bmd.set(mtb.getId().replace("_", ":"), mtb.getValue().iterator());                            
                        }                                                
                    }
                }else if(mc.getChildrenList().get(0) instanceof Widget  
                    && mc.getChildrenList().get(0) instanceof MaterialCheckBox){     
                    MaterialCheckBox mtb = (MaterialCheckBox)mc.getChildrenList().get(0);
                    if(mtb.getValue() != null && mtb.getValue() ){
                        bmd.set(mtb.getElement().getId().replace("_", ":"), "S");
                    }else{
                        bmd.set(mtb.getElement().getId().replace("_", ":"), "N");
                    }                    
                }                                
            }else if(mc.getChildrenList().size() == 2){

            }else{

            }                            
        }                            
    }
    
//    public static void getValueByCampo(Widget w){
//        if(w instanceof MaterialComboBox){     
//            MaterialComboBox mtb = (MaterialComboBox)w;                        
//            if(mtb.getSingleValue() != null && !mtb.getSingleValue().equals("")){
//                if(mtb.getSingleValue() instanceof BaseModelData){
//                    BaseModelData bmdCompo = (BaseModelData)mtb.getSingleValue();                                             
//
//                    String vtIdsHTML = mtb.getId();
//                    if(vtIdsHTML != null && vtIdsHTML.split("_").length == 2){
//                        String vtIds[] = vtIdsHTML.split("_");
//
//                        bmd.set(vtIdsHTML.replace("_", ":"), bmdCompo.get(vtIds[1]));  
//                        BaseModelData bmdIds = getIdsForm(mr);
//
//                        Collection<String> strBSKey = bmdCompo.getPropertyNames();
//                        Iterator<String> iteraKey = strBSKey.iterator();
//                        while(iteraKey.hasNext()){
//                            String strKey = iteraKey.next();
//                            String claveCompu = vtIds[0]+":"+strKey;
//                            if(bmdIds.get(claveCompu) != null){
//                                bmd.set(claveCompu, bmdCompo.get(strKey));
//                            }                                   
//                        }                                                               
//                    }else if(vtIdsHTML != null && vtIdsHTML.split("_").length == 1){
//                        if(bmdCompo.get("ssIdCbx") != null && !bmdCompo.get("ssIdCbx").equals("")){
//                            bmd.set(vtIdsHTML.replace("_", ":"), bmdCompo.get("ssIdCbx")); 
//                        }else{
//                            bmd.set(vtIdsHTML.replace("_", ":"), null); 
//                        }
//                    }else{
////                                    Error de desarrollo;
//                    }                                
//                }else if(mtb.getSingleValue() instanceof String){
//                    String strValue = (String)mtb.getSingleValue();
//                    bmd.set(mtb.getId().replace("_", ":"), strValue);
//                }                            
//            }                           
//        }
//    }
            
    
    public static BaseModelData getIdsForm(MaterialRow mr){
        BaseModelData bmd = new BaseModelData();
        
        List<Widget> lstMC = mr.getChildrenList();
        for(Widget w : lstMC){
            MaterialColumn mc = (MaterialColumn)w;                            
            if(mc.getChildrenList().size() == 1){

                if(mc.getChildrenList().get(0) instanceof MaterialWidget){
                    MaterialWidget mvc = (MaterialWidget)mc.getChildrenList().get(0);

                    if(mvc instanceof MaterialDoubleBox){
                        MaterialDoubleBox mtb = (MaterialDoubleBox)mvc;                        
                        bmd.set(mtb.getId().replace("_", ":"), mtb.getId().replace("_", ":"));  
                    }else if(mvc instanceof MaterialTextBox){     
                        MaterialTextBox mtb = (MaterialTextBox)mvc;
                        bmd.set(mtb.getId().replace("_", ":"), mtb.getId().replace("_", ":"));  
                    }else if(mvc instanceof MaterialTextArea){     
                        MaterialTextArea mtb = (MaterialTextArea)mvc;
                        bmd.set(mtb.getId().replace("_", ":"), mtb.getId().replace("_", ":"));                         
                    }else if(mvc instanceof MaterialDatePicker){     
                        MaterialDatePicker mtb = (MaterialDatePicker)mvc;
                        bmd.set(mtb.getId().replace("_", ":"), mtb.getId().replace("_", ":"));  
                    }else if(mvc instanceof MaterialListValueBox){     
                        MaterialListValueBox mtb = (MaterialListValueBox)mvc;                        
                        bmd.set(mtb.getId().replace("_", ":"), mtb.getId().replace("_", ":"));  
                    }else if(mvc instanceof MaterialComboBox){     
                        MaterialComboBox mtb = (MaterialComboBox)mvc;                        
                        bmd.set(mtb.getId().replace("_", ":"), mtb.getId().replace("_", ":"));  
                    }else if(mvc instanceof MaterialAutoComplete){     
                        MaterialAutoComplete mtb = (MaterialAutoComplete)mvc;                        
                        bmd.set(mtb.getId().replace("_", ":"), mtb.getId().replace("_", ":"));  
                    }
                }else if(mc.getChildrenList().get(0) instanceof Widget  
                    && mc.getChildrenList().get(0) instanceof MaterialCheckBox){     
                    MaterialCheckBox mtb = (MaterialCheckBox)mc.getChildrenList().get(0);
                    bmd.set(mtb.getElement().getId().replace("_", ":"), mtb.getElement().getId().replace("_", ":"));                      
                }                                
            }else if(mc.getChildrenList().size() == 2){

            }else{

            }                            
        }                    
        return bmd;
    }
    
    public static void setDataForm(MaterialRow mr, BaseModelData bmd){
        setDataForm(mr, bmd, "");
    }
    
    public static void setDataForm(MaterialRow mr, BaseModelData bmd, String strPrefijo){
        List<Widget> lstMC = mr.getChildrenList();
        for(Widget w : lstMC){
            MaterialColumn mc = (MaterialColumn)w;                            
            if(mc.getChildrenList().size() == 1){

                if(mc.getChildrenList().get(0) instanceof MaterialWidget){
                    MaterialWidget mvc = (MaterialWidget)mc.getChildrenList().get(0);

                    if(mvc instanceof MaterialDoubleBox){
                        MaterialDoubleBox mtb = (MaterialDoubleBox)mvc;
                        String strID =  mtb.getId().replace("_", ":");
                        String strIDAux = strID.replace(strPrefijo, "");
                        if(bmd.get(strIDAux) != null){
                            if(bmd.get(strIDAux).equals(PUN.DATA_VACIO_NO_ORIGEN)){
                                 mtb.setValue(null);
                            }else{
                                if(bmd.get(strIDAux) instanceof Double ){
                                    mtb.setValue((Double)bmd.get(strID));
                                }
                            }
                        }
                    }else if(mvc instanceof MaterialTextBox){     
                        MaterialTextBox mtb = (MaterialTextBox)mvc;
                        String strID =  mtb.getId().replace("_", ":");
                        String strIDAux = strID.replace(strPrefijo, "");
                        if(bmd.get(strIDAux) != null){
                            if(bmd.get(strIDAux).equals(PUN.DATA_VACIO_NO_ORIGEN)){
                                mtb.setValue(null);
                            }else{
                                mtb.setValue(bmd.get(strIDAux).toString());
                            }
                        }                                                                                                
                    }else if(mvc instanceof MaterialTextArea){     
                        MaterialTextArea mtb = (MaterialTextArea)mvc;
                        String strID =  mtb.getId().replace("_", ":");
                        String strIDAux = strID.replace(strPrefijo, "");
                        if(bmd.get(strIDAux) != null){
                            if(bmd.get(strIDAux).equals(PUN.DATA_VACIO_NO_ORIGEN)){
                                mtb.setValue(null);
                            }else{
                                mtb.setValue(bmd.get(strIDAux).toString());
                            }
                        }                        
                    }else if(mvc instanceof MaterialDatePicker){     
                        MaterialDatePicker mtb = (MaterialDatePicker)mvc;
                        String strID =  mtb.getId().replace("_", ":"); 
                        String strIDAux = strID.replace(strPrefijo, "");
                        if(bmd.get(strIDAux) != null){                                                        
                            if(bmd.get(strIDAux).equals(PUN.DATA_VACIO_NO_ORIGEN)){
                                mtb.setValue(null);
                            }else if(bmd.get(strIDAux) instanceof Date){                            
                                mtb.setValue((Date)bmd.get(strIDAux));
                            }else{
                                mtb.setValue(null);
                            }
                        }                        
                    }else if(mvc instanceof MaterialListValueBox){     

                        MaterialListValueBox mtb = (MaterialListValueBox)mvc;
                        String strID =  mtb.getId().replace("_", ":");
                        String strIDAux = strID.replace(strPrefijo, "");
                        String strValue = bmd.get(strIDAux);                        
                        if(strValue != null){                            
                            if(bmd.get(strIDAux).equals(PUN.DATA_VACIO_NO_ORIGEN)){
                                mtb.setSelectedValue(null);
                            }else{
                                int count = mtb.getItemCount();
                                for(int i=0; i<count-1; i++){
                                    if(mtb != null &&  mtb.getValue(i) != null){
                                        Object o = mtb.getValue(i);
                                        BaseModelData bmd1 = (BaseModelData)o;
                                        if(bmd1.get(PUN.SS_ID_CBX).equals(strValue)){
                                            mtb.setSelectedIndex(i);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }else if(mvc instanceof MaterialComboBox){     

                        MaterialComboBox mtb = (MaterialComboBox)mvc;
                        
                        String vtIdsHTML = mtb.getId();                                
                        if(vtIdsHTML != null && vtIdsHTML.split("_").length == 2){                                     
                            String vtIds[] = vtIdsHTML.split("_");
                                                
                            String strId = vtIdsHTML.replace("_", ":");
                            String strValue = bmd.get(strId);                        
                            if(strValue != null){                            
                                if(bmd.get(strId).equals(PUN.DATA_VACIO_NO_ORIGEN)){
                                    mtb.setValue(null, false);
                                }else{
    //                                int count = mtb.getItemCount();
                                    if(mtb.getValue() != null && !mtb.getValue().isEmpty()){
                                        for(int i=0; i<mtb.getValues().size(); i++){
                                            BaseModelData bmdSelect = (BaseModelData)mtb.getValues().get(i);
                                            if(bmdSelect.get(vtIds[1]) != null && bmdSelect.get(vtIds[1]).equals(strValue)){
                                                mtb.setSingleValue(bmdSelect);
                                                break;
                                            }
                                        }
                                    }else{
                                        BaseModelData bmdValue = new BaseModelData();                                        
                                        bmdValue.set(vtIds[1], strValue);
                                        mtb.addItem(strValue, bmdValue);
                                        mtb.setSingleValue(bmdValue);
                                    }
                                }
                            }
                            
                        }else if(vtIdsHTML != null && vtIdsHTML.split("_").length == 1){
                            String strValue = bmd.get(mtb.getId()); 
                            String strId = mtb.getId();
                            if(strValue != null){                            
                                if(bmd.get(strId).equals(PUN.DATA_VACIO_NO_ORIGEN)){
                                    mtb.setValue(null, false);
                                }else{
    //                                int count = mtb.getItemCount();
                                    if(mtb.getValue() != null && !mtb.getValue().isEmpty()){
                                        for(int i=0; i<mtb.getValues().size(); i++){
                                            BaseModelData bmdSelect = (BaseModelData)mtb.getValues().get(i);
                                            if(bmdSelect.get("ssIdCbx") != null && bmdSelect.get("ssIdCbx").equals(strValue)){
                                                mtb.setSingleValue(bmdSelect);
                                                break;
                                            }
                                        }
                                    }else{
                                        BaseModelData bmdValue = new BaseModelData();                                        
                                        bmdValue.set("ssIdCbx", strValue);
                                        bmdValue.set("ssValueCbx", strValue);
                                        bmdValue.set("kaNl", -1);
                                        mtb.addItem(strValue, bmdValue);
                                        mtb.setSingleValue(bmdValue);
                                    }
                                }
                            }
                            
                            
                        }else{
                            // error de desarrollo
                        }
                    }else if(mvc instanceof MaterialAutoComplete){     

                        MaterialAutoComplete mtb = (MaterialAutoComplete)mvc;
                        String strId =  mtb.getId().replace("_", ":");
                        String strIDAux = strId.replace(strPrefijo, "");
                        String strValue = bmd.get(strIDAux);                                                                   

                        if(strValue != null){
                            if(bmd.get(strIDAux).equals(PUN.DATA_VACIO_NO_ORIGEN)){
                                mtb.setItemValues(null);
                            }else{
                                BaseModelData bmd1 = new BaseModelData();
                                bmd1.set(strIDAux, strValue);   //                   
                                List<String> itemValues = new ArrayList<>();
                                itemValues.add(strValue);                                                                
//                                mtb.setItemValues(itemValues, true);
                                
                                EntidadCBX bX = new EntidadCBX(bmd1, strIDAux);
                                List<EntidadCBX> lsvalues = new ArrayList<EntidadCBX>();                                
                                lsvalues.add(bX);                                        
                                mtb.setValue(lsvalues);
                                
                            }
//                        }else{
//                            mtb.setItemValues(null);
                        }
                    }
                }else if(mc.getChildrenList().get(0) instanceof Widget  
                    && mc.getChildrenList().get(0) instanceof MaterialCheckBox){     
                    MaterialCheckBox mtb = (MaterialCheckBox)mc.getChildrenList().get(0);
                    String strId =  mtb.getElement().getId();
                    String strIDAux = strId.replace(strPrefijo, "");
                    if(bmd.get(strIDAux) != null){                            
                        if(bmd.get(strId).equals("S")){
                            mtb.setValue(true);
                        }else{
                            mtb.setValue(false);
                        }
                    }else{
                        mtb.setValue(false);
                    }
                }                                
            }else if(mc.getChildrenList().size() == 2){

            }else{

            }                            
        }                    
    }
    
    public static void setClearForm(MaterialRow mr, BaseModelData bmd){
        List<Widget> lstMC = mr.getChildrenList();
        for(Widget w : lstMC){
            MaterialColumn mc = (MaterialColumn)w;                            
            if(mc.getChildrenList().size() == 1){
                if(mc.getChildrenList().get(0) instanceof MaterialWidget){
                    MaterialWidget mvc = (MaterialWidget)mc.getChildrenList().get(0);

                    if(mvc instanceof MaterialDoubleBox){
                        MaterialDoubleBox mtb = (MaterialDoubleBox)mvc;
                        String strId = mtb.getId();
                        if(bmd.get(strId) != null){
                            mtb.setValue(bmd.get(mtb.getId()));
                        }else{
                            mtb.setValue(null);
                            mtb.clear();
                        }
                    }else if(mvc instanceof MaterialTextBox){     
                        MaterialTextBox mtb = (MaterialTextBox)mvc;
                        String strId = mtb.getId();
                        if(bmd.get(strId) != null){
                            mtb.setValue(bmd.get(mtb.getId()));
                        }else{
                            mtb.setValue(null);
                            mtb.clear();
                        }
                    }else if(mvc instanceof MaterialTextArea){     
                        MaterialTextArea mtb = (MaterialTextArea)mvc;
                        String strId = mtb.getId();
                        if(bmd.get(strId) != null){
                            mtb.setValue(bmd.get(mtb.getId()));
                        }else{
                            mtb.setValue(null);
                            mtb.clear();
                        }
                    }else if(mvc instanceof MaterialDatePicker){     
                        MaterialDatePicker mtb = (MaterialDatePicker)mvc;
                        String strId = mtb.getId();
                        if(bmd.get(strId)!= null){
                            mtb.setValue(bmd.get(mtb.getId()));
                        }else{
                            mtb.setValue(null);
                            mtb.clear();
                        }
                    }else if(mvc instanceof MaterialListValueBox){     
                        MaterialListValueBox mtb = (MaterialListValueBox)mvc;
                        mtb.setSelectedValue(null);                        
                        mtb.clear();
                    }else if(mvc instanceof MaterialComboBox){     
                        MaterialComboBox mtb = (MaterialComboBox)mvc;                        
                        if(mtb.getValue()!= null && !mtb.getValue().isEmpty()){
                            BaseModelData bmd1 = (BaseModelData)mtb.getValue().get(0);
                            mtb.setSingleValue(bmd1);
                        }
//                        mtb.clear();
                    }else if(mvc instanceof MaterialAutoComplete){     
                        MaterialAutoComplete mtb = (MaterialAutoComplete)mvc;                        
                        mtb.setItemValues(null);                        
                        mtb.clear();
                    }
                }else if(mc.getChildrenList().get(0) instanceof Widget  
                    && mc.getChildrenList().get(0) instanceof MaterialCheckBox){     
                    MaterialCheckBox mtb = (MaterialCheckBox)mc.getChildrenList().get(0);                    
                    mtb.setValue(false);                        
                }                                
            }else if(mc.getChildrenList().size() == 2){

            }else{

            }                            
        }                    
    }         
    
    public static String getCampoSerializadoAlert15CBX(String strKey){
        if(strKey.contains("pkReg") || strKey.contains("countReg")
            || strKey.contains("estReg")             
                ){
            return null;        
        }else{
            return strKey;
        }
    }
    
    public static String getOnlyTitlesByComponente(Widget mw){
        if(mw instanceof MaterialTextBox){
            MaterialTextBox mtb = (MaterialTextBox)mw;
            return mtb.getLabel().getText();                
        }else if (mw instanceof MaterialAutoComplete) {
            MaterialAutoComplete mac = (MaterialAutoComplete)mw; 
            return mac.getPlaceholder();                
        }else if (mw instanceof MaterialDatePicker) {
            MaterialDatePicker mdp = (MaterialDatePicker)mw;
            return mdp.getPlaceholder();
        }else if (mw instanceof MaterialDoubleBox) {
            MaterialDoubleBox mdb = (MaterialDoubleBox)mw;
            return mdb.getLabel().getText();
        }else if (mw instanceof MaterialTextArea) {
            MaterialTextArea mdb = (MaterialTextArea)mw;
            return mdb.getLabel().getText();
        }else if (mw instanceof MaterialListValueBox) {
            MaterialListValueBox mdb = (MaterialListValueBox)mw;
            return mdb.getPlaceholder();                                          
        }else if (mw instanceof MaterialComboBox) {
            MaterialComboBox mdb = (MaterialComboBox)mw;
            return mdb.getPlaceholder();                                          
        }else if(mw instanceof MaterialFileUploader) {                               
                /**NUNCA SE FILTRA POR UN ADJUNTO*/
            return "ADJUNTO";
        }else if(mw instanceof MaterialAutoComplete){
            MaterialAutoComplete mdb = (MaterialAutoComplete)mw;
            return mdb.getPlaceholder();                               
        }else if(mw instanceof MaterialRadioButton){
            return "rADIO BUTTON";
        }else if(mw instanceof MaterialCheckBox){
            MaterialCheckBox mdb = (MaterialCheckBox)mw;
            return mdb.getText();                                               
        }else{
            return null;
        }
    }
    
    public static List<BaseModelData> getModelBytTable(MaterialDataTableCuston mTable){
        List<BaseModelData> lstBMD = new ArrayList<>();
        for(int i=0; i<mTable.getRows().size(); i++){
            RowComponent<BaseModelData> rc = mTable.getRow(i);
            BaseModelData bmd1 = rc.getData();
            lstBMD.add(bmd1);
        }
        return lstBMD;
    }
    
}
