/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;

/**
 *
 * @author diego
 */
public class ObjBMDGrid extends ObjBMDFD implements IsSerializable{
    private List<BaseModelData> lstBmd;

    public ObjBMDGrid() {
    }

    public ObjBMDGrid(List<BaseModelData> lstBmd) {
        this.lstBmd = lstBmd;
    }

    public List<BaseModelData> getLstBmd() {
        return lstBmd;
    }

    public void setLstBmd(List<BaseModelData> lstBmd) {
        this.lstBmd = lstBmd;
    }
}
