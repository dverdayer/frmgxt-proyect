/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.util;


import com.frm.proto.client.salpi.BaseModelData;
//import com.extjs.gxt.ui.client.data.BaseModelData;
import java.util.ArrayList;
import java.util.List;
 
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.i18n.client.DateTimeFormat;
import java.util.Date;


/** 
 * A <code>DataReader</code> implementation that reads JSON data using a
 * <code>ModelType</code> definition and produces a set of
 * <code>ModelData</code> instances.
 *
 * <p />
 * Subclasses can override {@link #createReturnData(Object, List, int)} to
 * control what object is returned by the reader. Subclass may override
 * {@link #newModelInstance()} to return any model data subclass.
 *
 * @param <D> the <code>ListLoadResult</code> type being returned by the reader
 */
public class MyJsonReader<D> {

    private ModelType modelType;

    /**
     * Creates a new JSON reader.
     *
     * @param modelType the model type definition
     */
    public MyJsonReader(ModelType modelType) {
        this.modelType = modelType;
    }  

    @SuppressWarnings("unchecked")
    public D read(Object loadConfig, Object data) {
        JSONObject jsonRoot = null;
        if (data instanceof JavaScriptObject) {
            jsonRoot = new JSONObject((JavaScriptObject) data);
        } else {
            jsonRoot = (JSONObject) JSONParser.parse((String) data);
        }

        JSONArray root = (JSONArray) jsonRoot.get(modelType.getRoot());
        int size = root.size();
        ArrayList<BaseModelData> models = new ArrayList<BaseModelData>();
        for (int i = 0; i < size; i++) {
            JSONObject obj = (JSONObject) root.get(i);
            BaseModelData model = newModelInstance();

            for (String keyname : obj.keySet()) {
                String name = keyname;
                Class type = null;
                JSONValue value = obj.get(name);
                if (value == null) {
                    continue;
                }
                if (value.isArray() != null) {
                    // nothing
                } else if (value.isBoolean() != null) {
                    model.set(name, value.isBoolean().booleanValue());
                } else if (value.isNumber() != null) {
                    if (type != null) {
                        Double d = value.isNumber().doubleValue();
                        if (type.equals(Integer.class)) {
                            model.set(name, d.intValue());
                        } else if (type.equals(Long.class)) {
                            model.set(name, d.longValue());
                        } else if (type.equals(Float.class)) {
                            model.set(name, d.floatValue());
                        } else {
                            model.set(name, d);
                        }
                    } else {
                        model.set(name, value.isNumber().doubleValue());
                    }
                } else if (value.isObject() != null) {
                    ReadEntityNivel2((JSONObject)value, model, name);
                    // nothing
                } else if (value.isString() != null) {
                    String s = value.isString().stringValue();
                    if (modelType.getField(name) != null && modelType.getField(name).getType() != null
                        && modelType.getField(name).getType().equals(Date.class)){
                            String [] dtDate = s.split("\\|\\|");
                            if(dtDate.length == 2){
                                String strFecha = dtDate[0];
                                String strHour = dtDate[1];
                                if(strFecha.split("/").length == 3){
                                    Date date = new Date();
                                    date.setYear(Integer.valueOf(strFecha.split("/")[0])-1900);
                                    String strMonth = strFecha.split("/")[1].charAt(0) == '0'? strFecha.split("/")[1].replace("0",""):strFecha.split("/")[1];
                                    date.setMonth(Integer.valueOf(strMonth.trim())-1);
                                    String strDate = strFecha.split("/")[2].charAt(0) == '0'? strFecha.split("/")[2].replace("0",""):strFecha.split("/")[2];
                                    date.setDate(Integer.valueOf(strDate.trim()));
                                    model.set(name, date);
                                }else{
                                    model.set(name, s);
                                }
                            }else{
                                try{
                                    DateTimeFormat format = DateTimeFormat.getFormat("yyyy/MM/dd HH:mm:ss");
                                    Date dt = format.parse(s);
                                    model.set(name, dt);
                                }catch(Exception e){                                    
                                    model.set(name, s);
                                }
                            }
                        
                    } else {
                        model.set(name, s);
                    }
                } else if (value.isNull() != null) {
                    model.set(name, null);
                }
            }
            models.add(model);
        }

        int totalCount = models.size();
        if (modelType.getTotalName() != null) {
            totalCount = getTotalCount(jsonRoot);
        }
        return (D) createReturnData(loadConfig, models, totalCount);
    }

    /**
     * Responsible for the object being returned by the reader.
     *
     * @param loadConfig the load config
     * @param records the list of models
     * @param totalCount the total count
     * @return the data to be returned by the reader
     */
    @SuppressWarnings("unchecked")
    protected Object createReturnData(Object loadConfig, List<BaseModelData> records,
            int totalCount) {
        return (D) records;
    }

    protected int getTotalCount(JSONObject root) {
        if (modelType.getTotalName() != null) {
            JSONValue v = root.get(modelType.getTotalName());
            if (v != null) {
                if (v.isNumber() != null) {
                    return (int) v.isNumber().doubleValue();
                } else if (v.isString() != null) {
                    return Integer.parseInt(v.isString().stringValue());
                }
            }
        }
        return -1;
    }

    /**
     * Returns the new model instances. Subclasses may override to provide an
     * model data subclass.
     *
     * @return the new model data instance
     */
    protected BaseModelData newModelInstance() {
        return new BaseModelData();
    }


    protected void ReadEntityNivel2(JSONObject obj, BaseModelData model, String strKeyF) {
        String str = "";
        for (String strKeyName : obj.keySet()) {
            String strTypeCampo = getKeyDataModel(strKeyName, strKeyF);

            if(strTypeCampo != null && !strTypeCampo.equals("")){
                JSONValue value = obj.get(strKeyName);
                if (value == null) {
                    continue;
                }
                if (value.isArray() != null) {
                    // nothing
                } else if (value.isBoolean() != null) {
                    model.set(strTypeCampo, value.isBoolean().booleanValue());
                } else if (value.isNumber() != null) {
                    model.set(strTypeCampo, value.isNumber().doubleValue());
                } else if (value.isObject() != null) {
                    ReadEntityNivel3((JSONObject)value, model, strTypeCampo);
                } else if (value.isString() != null) {
                    String s = value.isString().stringValue();
                    String name =  strTypeCampo;
                    if (modelType.getField(name) != null && modelType.getField(name).getType() != null
                        && modelType.getField(name).getType().equals(Date.class)){
                            String [] dtDate = s.split("\\|\\|");
                            if(dtDate.length == 2){
                                String strFecha = dtDate[0];
                                String strHour = dtDate[1];
                                if(strFecha.split("/").length == 3){
                                    Date date = new Date();
                                    date.setYear(Integer.valueOf(strFecha.split("/")[0])-1900);
                                    String strMonth = strFecha.split("/")[1].charAt(0) == '0'? strFecha.split("/")[1].replace("0",""):strFecha.split("/")[1];
                                    date.setMonth(Integer.valueOf(strMonth.trim())-1);
                                    String strDate = strFecha.split("/")[2].charAt(0) == '0'? strFecha.split("/")[2].replace("0",""):strFecha.split("/")[2];
                                    date.setDate(Integer.valueOf(strDate.trim()));
                                    model.set(name, date);
                                }else{
                                    model.set(name, s);
                                }
                            }else{
                                model.set(name, s);
                            }

                    } else {
                        model.set(name, s);
                    }
//                    model.set(strTypeCampo, s);
                } else if (value.isNull() != null) {
                    model.set(strTypeCampo, null);
                }
            }

        }

//        return new BaseModelData();
    }

    /**hojala no halla que hacer mas malparidas otra vez por favor not*/
    protected void ReadEntityNivel3(JSONObject obj, BaseModelData model, String strKeyF) {
        String str = "";
        for (String strKeyName : obj.keySet()) {
            String strTypeCampo = getKeyDataModel3(strKeyName, strKeyF);

            if(strTypeCampo != null && !strTypeCampo.equals("")){
                JSONValue value = obj.get(strKeyName);
                if (value == null) {
                    continue;
                }
                if (value.isArray() != null) {
                    // nothing
                } else if (value.isBoolean() != null) {
                    model.set(strTypeCampo, value.isBoolean().booleanValue());
                } else if (value.isNumber() != null) {
                    model.set(strTypeCampo, value.isNumber().doubleValue());
                } else if (value.isObject() != null) {
                    ReadEntityNivel4((JSONObject)value, model, strTypeCampo);
                } else if (value.isString() != null) {
                    String s = value.isString().stringValue();
                    String name =  strTypeCampo;
                    if (modelType.getField(name) != null && modelType.getField(name).getType() != null
                        && modelType.getField(name).getType().equals(Date.class)){
                            String [] dtDate = s.split("\\|\\|");
                            if(dtDate.length == 2){
                                String strFecha = dtDate[0];
                                String strHour = dtDate[1];
                                if(strFecha.split("/").length == 3){
                                    Date date = new Date();
                                    date.setYear(Integer.valueOf(strFecha.split("/")[0])-1900);
                                    String strMonth = strFecha.split("/")[1].charAt(0) == '0'? strFecha.split("/")[1].replace("0",""):strFecha.split("/")[1];
                                    date.setMonth(Integer.valueOf(strMonth.trim())-1);
                                    String strDate = strFecha.split("/")[2].charAt(0) == '0'? strFecha.split("/")[2].replace("0",""):strFecha.split("/")[2];
                                    date.setDate(Integer.valueOf(strDate.trim()));
                                    model.set(name, date);
                                }else{
                                    model.set(name, s);
                                }
                            }else{
                                model.set(name, s);
                            }

                    } else {
                        model.set(name, s);
                    }
//                    model.set(strTypeCampo, s);
                } else if (value.isNull() != null) {
                    model.set(strTypeCampo, null);
                }
            }

        }

//        return new BaseModelData();
    }

    /**hojala no halla que hacer mas malparidas otra vez por favor not*/
    protected void ReadEntityNivel4(JSONObject obj, BaseModelData model, String strKeyF) {
        String str = "";
        for (String strKeyName : obj.keySet()) {
            String strTypeCampo = getKeyDataModel3(strKeyName, strKeyF);

            if(strTypeCampo != null && !strTypeCampo.equals("")){
                JSONValue value = obj.get(strKeyName);
                if (value == null) {
                    continue;
                }
                if (value.isArray() != null) {
                    // nothing
                } else if (value.isBoolean() != null) {
                    model.set(strTypeCampo, value.isBoolean().booleanValue());
                } else if (value.isNumber() != null) {
                    model.set(strTypeCampo, value.isNumber().doubleValue());
                } else if (value.isObject() != null) {

                } else if (value.isString() != null) {
                    String s = value.isString().stringValue();
                    String name =  strTypeCampo;
                    if (modelType.getField(name) != null && modelType.getField(name).getType() != null
                        && modelType.getField(name).getType().equals(Date.class)){
                            String [] dtDate = s.split("\\|\\|");
                            if(dtDate.length == 2){
                                String strFecha = dtDate[0];
                                String strHour = dtDate[1];
                                if(strFecha.split("/").length == 3){
                                    Date date = new Date();
                                    date.setYear(Integer.valueOf(strFecha.split("/")[0])-1900);
                                    String strMonth = strFecha.split("/")[1].charAt(0) == '0'? strFecha.split("/")[1].replace("0",""):strFecha.split("/")[1];
                                    date.setMonth(Integer.valueOf(strMonth.trim())-1);
                                    String strDate = strFecha.split("/")[2].charAt(0) == '0'? strFecha.split("/")[2].replace("0",""):strFecha.split("/")[2];
                                    date.setDate(Integer.valueOf(strDate.trim()));
                                    model.set(name, date);
                                }else{
                                    model.set(name, s);
                                }
                            }else{
                                model.set(name, s);
                            }

                    } else {
                        model.set(name, s);
                    }
//                    model.set(strTypeCampo, s);
                } else if (value.isNull() != null) {
                    model.set(strTypeCampo, null);
                }
            }

        }

//        return new BaseModelData();
    }

    private String getKeyDataModel(String strKeyName, String strKeyF){

        for (int i=0; i<this.modelType.getFieldCount(); i++){
            DataField df = this.modelType.getField(i);
            String strTypeCampo = df.getName();
            String [] vtTypeCampo = strTypeCampo.split(":");
            if(vtTypeCampo != null && (vtTypeCampo.length == 2 || vtTypeCampo.length == 3)
               && strKeyF.equals(vtTypeCampo[0])&& strKeyName.equals(vtTypeCampo[1])){
                return strTypeCampo;
            }
        }
        return null;
    }


    private String getKeyDataModel3(String strKeyName, String strKeyF){

        String [] vtStrKeyF = strKeyF.split(":");
        for (int i=0; i<this.modelType.getFieldCount(); i++){
            DataField df = this.modelType.getField(i);
            String strTypeCampo = df.getName();
            String [] vtTypeCampo = strTypeCampo.split(":");
            if(vtTypeCampo != null && vtTypeCampo.length == 3
               && vtStrKeyF[0].equals(vtTypeCampo[0])&& vtStrKeyF[1].equals(vtTypeCampo[1]) && strKeyName.equals(vtTypeCampo[2])){
                return strTypeCampo;
            }
        }
        return null;
    }

}
