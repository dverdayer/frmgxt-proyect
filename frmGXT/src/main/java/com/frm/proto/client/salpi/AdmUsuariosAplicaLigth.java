/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author diegohernandez
 */
public class AdmUsuariosAplicaLigth implements IsSerializable{
    private Integer kaNlUsuario;
    private String ssLogin;   
    private Integer nlSerial;
    private Integer nlAplicacion;
    // apunta a referencia de usuarios
    private Integer nlTiposUsuario;
    private String ssTipoUsuario;
    // apunta a referencia de terceros
    private Integer nlTercero;
    private String ssEmail;
    private String ssCedula;
    private Integer nlEmpresa;
    private String ssEmpresa;
    private boolean moduloMultiempresa;

    public AdmUsuariosAplicaLigth() {
    }

    public AdmUsuariosAplicaLigth(Integer kaNlUsuario, String ssLogin, Integer nlSerial, Integer nlAplicacion, Integer nlTiposUsuario
            , String ssTipoUsuario, Integer nlTercero, String ssEmail, String ssCedula, Integer nlEmpresa, String ssEmpresa) {
        this.kaNlUsuario = kaNlUsuario;
        this.ssLogin = ssLogin;
        this.nlSerial = nlSerial;
        this.nlAplicacion = nlAplicacion;
        this.nlTiposUsuario = nlTiposUsuario;
        this.ssTipoUsuario = ssTipoUsuario;
        this.nlTercero = nlTercero;
        this.ssEmail = ssEmail;
        this.ssCedula = ssCedula;
        this.ssEmpresa = ssEmpresa;
        this.nlEmpresa = nlEmpresa;
    }

    public Integer getKaNlUsuario() {
        return kaNlUsuario;
    }

    public void setKaNlUsuario(Integer kaNlUsuario) {
        this.kaNlUsuario = kaNlUsuario;
    }

    public Integer getNlTercero() {
        return nlTercero;
    }

    public void setNlTercero(Integer nlTercero) {
        this.nlTercero = nlTercero;
    }
  
    public String getSsLogin() {
        return ssLogin;
    }

    public void setSsLogin(String ssLogin) {
        this.ssLogin = ssLogin;
    }

    public Integer getNlTiposUsuario() {
        return nlTiposUsuario;
    }

    public void setNlTiposUsuario(Integer nlTiposUsuario) {
        this.nlTiposUsuario = nlTiposUsuario;
    }

    public Integer getNlAplicacion() {
        return nlAplicacion;
    }

    public void setNlAplicacion(Integer nlAplicacion) {
        this.nlAplicacion = nlAplicacion;
    }

    public Integer getNlSerial() {
        return nlSerial;
    }

    public void setNlSerial(Integer nlSerial) {
        this.nlSerial = nlSerial;
    }

    public String getSsCedula() {
        return ssCedula;
    }

    public void setSsCedula(String ssCedula) {
        this.ssCedula = ssCedula;
    }

    public String getSsEmail() {
        return ssEmail;
    }

    public void setSsEmail(String ssEmail) {
        this.ssEmail = ssEmail;
    }

    public String getSsTipoUsuario() {
        return ssTipoUsuario;
    }

    public void setSsTipoUsuario(String ssTipoUsuario) {
        this.ssTipoUsuario = ssTipoUsuario;
    }

    public Integer getNlEmpresa() {
        return nlEmpresa;
    }

    public void setNlEmpresa(Integer nlEmpresa) {
        this.nlEmpresa = nlEmpresa;
    }

    public String getSsEmpresa() {
        return ssEmpresa;
    }

    public void setSsEmpresa(String ssEmpresa) {
        this.ssEmpresa = ssEmpresa;
    }

    public boolean isModuloMultiempresa() {
        return moduloMultiempresa;
    }

    public void setModuloMultiempresa(boolean moduloMultiempresa) {
        this.moduloMultiempresa = moduloMultiempresa;
    }
    
}
