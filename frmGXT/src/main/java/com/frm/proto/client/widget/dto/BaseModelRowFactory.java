/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.widget.dto;

import com.frm.proto.client.salpi.BaseModelData;
//import com.extjs.gxt.ui.client.data.BaseModelData;
import gwt.material.design.client.data.DataView;
import gwt.material.design.client.data.component.RowComponent;
import gwt.material.design.client.data.factory.RowComponentFactory;

/**
 *
 * @author d viego
 */
public class BaseModelRowFactory extends RowComponentFactory<BaseModelData> {

//    @Override
//    public RowComponent<BaseModelData> generate(BaseModelData model) {
//        // We won't change the way it loads the RowComponent
//        return super.generate(model);
//    }
    
    @Override
    public RowComponent<BaseModelData> generate(DataView dataView, BaseModelData model) {
        // We won't change the way it loads the RowComponent
        return super.generate(dataView, model);
    }

    @Override
    public String getCategory(BaseModelData model) {
        // We want to override the standard category retrieval
        // This is where we can define a models category.
        // This is useful when we don't want to pollute our
        // object models with the interface HasDataCategory.
//        return model.getCategory();
        return "";
    }
}