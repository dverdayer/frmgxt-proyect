/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.teble.sid;


import com.frm.proto.client.table.dto.DataPagerRemote;
import com.google.gwt.dom.client.Document;
import gwt.material.design.client.base.MaterialWidget;
import gwt.material.design.client.base.constants.TableCssName;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.constants.WavesType;
import gwt.material.design.client.ui.MaterialIcon;
import gwt.material.design.client.ui.html.Span;
import gwt.material.design.client.ui.pager.MaterialDataPager;

/**
 *
 * @author diego
 */
public class ActionsPanelSID extends MaterialWidget {

    private final DataPagerRemote pager;
    protected Span actionLabel = new Span();
    protected MaterialIcon iconNext = new MaterialIcon(IconType.KEYBOARD_ARROW_RIGHT);
    protected MaterialIcon iconPrev = new MaterialIcon(IconType.KEYBOARD_ARROW_LEFT);

    public ActionsPanelSID(DataPagerRemote pager) {
        super(Document.get().createDivElement(), TableCssName.ACTION_PAGE_PANEL);
        this.pager = pager;

        iconNext.addStyleName(TableCssName.ARROW_NEXT);
        iconPrev.addStyleName(TableCssName.ARROW_PREV);
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        // Add the next button
        iconNext.setWaves(WavesType.DEFAULT);
        iconNext.setCircle(true);
        add(iconNext);

        // Add the action label
        add(actionLabel);

        // Add the previous button
        iconPrev.setWaves(WavesType.DEFAULT);
        iconPrev.setCircle(true);
        add(iconPrev);

        // Register the handlers
        registerHandler(iconNext.addClickHandler(clickEvent -> pager.next()));
        registerHandler(iconPrev.addClickHandler(clickEvent -> pager.previous()));
    }

    public Span getActionLabel() {
        return actionLabel;
    }

    public MaterialIcon getIconNext() {
        return iconNext;
    }

    public MaterialIcon getIconPrev() {
        return iconPrev;
    }
}
