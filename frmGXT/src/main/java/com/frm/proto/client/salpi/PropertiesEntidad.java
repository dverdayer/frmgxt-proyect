/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author diego
 */
public class PropertiesEntidad implements IsSerializable {
    String strPropertiEnti;
    String strProperti;
    String strAcompanaString;

    public PropertiesEntidad() {
    }

    public PropertiesEntidad(String strPropertiEnti, String strProperti) {
        this.strPropertiEnti = strPropertiEnti;
        this.strProperti = strProperti;
    }

    public String getStrProperti() {
        return strProperti;
    }

    public void setStrProperti(String strProperti) {
        this.strProperti = strProperti;
    }

    public String getStrPropertiEnti() {
        return strPropertiEnti;
    }

    public void setStrPropertiEnti(String strPropertiEnti) {
        this.strPropertiEnti = strPropertiEnti;
    }

    public String getStrAcompanaString() {
        return strAcompanaString;
    }

    public void setStrAcompanaString(String strAcompanaString) {
        this.strAcompanaString = strAcompanaString;
    }

}
