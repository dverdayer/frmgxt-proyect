/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author diego
 */
public class MaestroEntidad  implements IsSerializable{
    private int[] longCampos;
    private String[] tiposCampos;
    private String[] titulos;
    private String[] ids;
    private String[] idsFilter;
    private boolean[] editables;

    public MaestroEntidad() {
    }

    public String[] getIds() {
        return ids;
    }

    public void setIds(String[] ids) {
        this.ids = ids;
    }

    public int[] getLongCampos() {
        return longCampos;
    }

    public void setLongCampos(int[] longCampos) {
        this.longCampos = longCampos;
    }

    public String[] getTiposCampos() {
        return tiposCampos;
    }

    public void setTiposCampos(String[] tiposCampos) {
        this.tiposCampos = tiposCampos;
    }

    public String[] getTitulos() {
        return titulos;
    }

    public void setTitulos(String[] titulos) {
        this.titulos = titulos;
    }

    public boolean[] getEditables() {
        return editables;
    }

    public void setEditables(boolean[] editables) {
        this.editables = editables;
    }

    public String[] getIdsFilter() {
        return idsFilter;
    }

    public void setIdsFilter(String[] idsFilter) {
        this.idsFilter = idsFilter;
    }

}
