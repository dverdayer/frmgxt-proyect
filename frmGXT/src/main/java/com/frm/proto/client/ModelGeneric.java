/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client;

import com.frm.proto.client.salpi.BaseModelData;
//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.json.client.JSONBoolean;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
//import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
 
/**
 *
 * @author Diego
 */

public class ModelGeneric extends BaseModelData  implements IsSerializable{

    public String ESTADO_DATA_ROW = "ESTADO_DATA_ROW";

    public ModelGeneric(){}

    public ModelGeneric(String ids[], List<Object[]> lstObj ) {
        for(int i=0; i<lstObj.size(); i++){
            Object [] vectObj = lstObj.get(i);
            for(int j=0; j<vectObj.length; j++){
                if(vectObj[j] != null)
                    setCampo(ids[j], vectObj[j]);
                else
                    setCampo(ids[j], "");
            }
        }
    }

    public ModelGeneric(JSONObject jo) {
        Set<String> ids = jo.keySet();
        Iterator<String> itera = ids.iterator();
        while (itera.hasNext()) {
            String strIds = itera.next();
            setCampo(strIds, jo.get(strIds));
        }

    }

    public void setCampo(String keyCampo, Object obj){
        if(obj instanceof JSONNumber){
            JSONNumber jn = (JSONNumber)obj;
            set(keyCampo, jn.doubleValue());
        }else if(obj instanceof JSONBoolean){
            JSONBoolean jb = (JSONBoolean)obj;
            set(keyCampo, jb.booleanValue());
        }else if(obj instanceof JSONString){
            JSONString js = (JSONString) obj;
            set(keyCampo, js.stringValue());
        }
    }

    public Object getCampo(String keyCampo){
        return get(keyCampo);
    }

    public void setCampoNormal(String keyCampo, Object obj){
        set(keyCampo, obj);
    }

    public String toString() {
        return (String)getCampo("ssRecurso");
    }

    public String  getSsRecurso(){
        return toString();
    }
    
    public String getEstadoDataRow(){
        return (String)get("ESTADO_DATA_ROW");
    }
    
    public void setEstadoDataRow(Object obj){
        set("ESTADO_DATA_ROW", obj);
    }


}