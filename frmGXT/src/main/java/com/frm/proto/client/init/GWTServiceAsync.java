/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client.init;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.salpi.AdmHoraServer;
import com.frm.proto.client.salpi.AdmUsuariosAplicaLigth;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.EntidadBMD;
import com.frm.proto.client.salpi.EntidadIndepen;
import com.frm.proto.client.salpi.HijoBaseModel;
import com.frm.proto.client.salpi.MsgFinalUser;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;

/**
 *
 * @author diegohernandez
 */
public interface GWTServiceAsync {

    public abstract void grabarEntidad(String strControlador, String strEntidad, String strModulo, List<BaseModelData> lstBaseModel,
        List<BaseModelData> lstBaseModelDelete,List<EntidadIndepen> lstDeleteIndepen, AsyncCallback<MsgFinalUser> asyncCallback);

    public abstract void grabarEntidadDina(String strControlador, String strEntidad, String strModulo, List<BaseModelData> lstBaseModel,
        List<BaseModelData> lstBaseModelDelete,List<EntidadIndepen> lstDeleteIndepen, AsyncCallback<MsgFinalUser> asyncCallback);

    public abstract void evenSetCtr(String strControlador, String strEntidad, String strId, String strValue ,
            AsyncCallback<String> asyncCallback);

    public abstract void grabarEntidad(String strControlador, String strEntidad, String strModulo, String strFlujo, BaseModelData bmdFP, List<HijoBaseModel> lstHBM,
        List<EntidadIndepen> lstDeleteIndepen, AsyncCallback<MsgFinalUser> asyncCallback);

    public abstract void getUserActual(AsyncCallback<String> asyncCallback);

    public void getUserActualNew(AsyncCallback<AdmUsuariosAplicaLigth> asyncCallback);

    public void getHoraServer(AsyncCallback<AdmHoraServer> asyncCallback);

    public void getCerrarSession(AsyncCallback<MsgFinalUser> asyncCallback);

    public void getSessionActiva(AsyncCallback<MsgFinalUser> asyncCallback);

    public void getCambiarPWD(String strPwd, String strPwdNew, AsyncCallback<MsgFinalUser> asyncCallback);

    public void isSessionActiva(AdmUsuariosAplicaLigth admUserActual, AsyncCallback<MsgFinalUser> asyncCallback);

    public void getEntidad(EntidadBMD bmd, String strXML, AsyncCallback<BaseModelData> asyncCallback);

    public void getEntidadDinamica(EntidadBMD bmd, String strXML, AsyncCallback<BaseModelData> asyncCallback);

    public void getEntidadList(EntidadBMD bmd, String strXML, AsyncCallback<List<BaseModelData>> asyncCallback);

    public void setEmpresa(EntidadBMD bmd, AsyncCallback<MsgFinalUser> asyncCallback);

    public void getASDFASDF(AsyncCallback<String> asdfhgj);
    
    public void myMethodBMDFinal(BaseModelData bmd, AsyncCallback<String> callback);

}
