/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client;

/**
 *
 * @author diego
 */

public class Permisos {

    public boolean insert = false;
    public boolean update = false;
    public boolean delete = false;

    public static final int DELETE = 0;
    public static final int UPDATE = 1;
    public static final int INSERT = 2;

    public Permisos(Integer nlPermisos){
        if(nlPermisos > 7)
            return;

        String strBynario = Integer.toBinaryString(nlPermisos);
        char [] vtBinario = strBynario.toCharArray();
        char [] xor = {0,0,0};
        for(int i=0; i<vtBinario.length ; i++){
            xor[2-i] = vtBinario[(vtBinario.length-1)-i];
        }

        if(xor[INSERT] == '1'){
            insert = true;
        }
        if(xor[UPDATE] == '1'){
            update = true;
        }
        if(xor[DELETE] == '1'){
            delete = true;
        }
    }

    public boolean isDelete() {
        return delete;
    }
    
    public boolean isInsert() {
        return insert;
    }

    public boolean isUpdate() {
        return update;
    }

}
