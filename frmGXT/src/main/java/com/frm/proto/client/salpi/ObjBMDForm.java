/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author diego
 */
public class ObjBMDForm extends ObjBMDFD implements IsSerializable{
    private BaseModelData bmd;

    public ObjBMDForm() {
    }

    public ObjBMDForm(BaseModelData bmd) {
        this.bmd = bmd;
    }

    public BaseModelData getBmd() {
        return bmd;
    }

    public void setBmd(BaseModelData bmd) {
        this.bmd = bmd;
    }

}
