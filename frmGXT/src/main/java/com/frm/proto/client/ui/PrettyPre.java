/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.ui;

/**
 *
 * @author diego
 */
import gwt.material.design.client.ui.html.Pre;

/**
 * @author Sven Jacobs
 */
public class PrettyPre extends Pre {

    public PrettyPre() {
        addStyleName(Styles.PRETTYPRINT);
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        // When the widget loads, force the styling of pretty print
        prettyPrint();
    }

    private native void prettyPrint() /*-{
        $wnd.prettyPrint();
    }-*/;
}
