/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client.salpi;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;

/**
 *
 * @author diego
 */
public class ObjBMDListOpcFD implements IsSerializable{
    private List<ObjectBMDTransFD> objectBMDTransFD;
    private BaseModelData objOpc;

    public ObjBMDListOpcFD() {
    }

    public ObjBMDListOpcFD( List<ObjectBMDTransFD> objectBMDTransFD, BaseModelData objOpc) {
        this.objectBMDTransFD = objectBMDTransFD;
        this.objOpc = objOpc;
    }

    public BaseModelData getObjOpc() {
        return objOpc;
    }

    public void setObjOpc(BaseModelData objOpc) {
        this.objOpc = objOpc;
    }

    public  List<ObjectBMDTransFD> getObjectBMDTransFD() {
        return objectBMDTransFD;
    }

    public void setObjectBMDTransFD( List<ObjectBMDTransFD> objectBMDTransFD) {
        this.objectBMDTransFD = objectBMDTransFD;
    }

}
