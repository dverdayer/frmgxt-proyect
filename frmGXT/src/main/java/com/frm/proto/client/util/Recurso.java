/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.util;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author diego
 */
public class Recurso implements Serializable{
    
    private Double kaNlRecurso;
    private String ssRecurso;
    private String ssXml;
    private String ssTipoVentana;
    private String ssControlador;
    private String ssEntidad;
    private Integer nlPermisos;
    private String ssH;
    private List<Recurso> properties;
    
    public Recurso(){        
    }

    public Double getKaNlRecurso() {
        return kaNlRecurso;
    }

    public void setKaNlRecurso(Double kaNlRecurso) {
        this.kaNlRecurso = kaNlRecurso;
    }

    public String getSsRecurso() {
        return ssRecurso;
    }

    public void setSsRecurso(String ssRecurso) {
        this.ssRecurso = ssRecurso;
    }

    public String getSsXml() {
        return ssXml;
    }

    public void setSsXml(String ssXml) {
        this.ssXml = ssXml;
    }

    public String getSsTipoVentana() {
        return ssTipoVentana;
    }

    public void setSsTipoVentana(String ssTipoVentana) {
        this.ssTipoVentana = ssTipoVentana;
    }

    public List<Recurso> getProperties() {
        return properties;
    }

    public void setProperties(List<Recurso> properties) {
        this.properties = properties;
    }
   
    public Integer getNlPermisos() {
        return nlPermisos;
    }

    public void setNlPermisos(Integer nlPermisos) {
        this.nlPermisos = nlPermisos;
    }

    public String getSsH() {
        return ssH;
    }

    public void setSsH(String ssH) {
        this.ssH = ssH;
    }

    public String getSsControlador() {
        return ssControlador;
    }

    public void setSsControlador(String ssControlador) {
        this.ssControlador = ssControlador;
    }

    public String getSsEntidad() {
        return ssEntidad;
    }

    public void setSsEntidad(String ssEntidad) {
        this.ssEntidad = ssEntidad;
    }
        
}
