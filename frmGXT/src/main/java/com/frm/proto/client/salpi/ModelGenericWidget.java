/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;

/**
 *
 * @author diego
 */
public class ModelGenericWidget extends BaseModelData  implements IsSerializable{

    static final String LABEL = "LABEL";
    static final String ENTIDAD = "ENTIDAD";
    static final String LIST_ENTIDAD = "LIST_ENTIDAD";

    public ModelGenericWidget(){}

    public ModelGenericWidget(String strLabel, String strEntidad, List<BaseModelData> lstBmds){
        set(LABEL, strLabel);
        set(ENTIDAD, strEntidad);
        set(LIST_ENTIDAD, lstBmds);
    }

    public String getStrEntidad() {
        return (String)get(ENTIDAD);
    }
  
    public String getStrLabel() {
        return (String)get(LABEL);
    }
   
    public List<BaseModelData> getLstBmds() {
        return (List<BaseModelData>)get(LIST_ENTIDAD);
    }

}
