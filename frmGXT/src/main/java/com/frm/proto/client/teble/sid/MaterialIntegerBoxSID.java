/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.teble.sid;

import gwt.material.design.client.ui.MaterialNumberBox;

/**
 *
 * @author diego
 */
public class MaterialIntegerBoxSID extends MaterialNumberBox<Integer> {

    public MaterialIntegerBoxSID() {
        setStep("1");
    }

    public MaterialIntegerBoxSID(String placeholder) {
        this();
        setPlaceholder(placeholder);
    }

    public MaterialIntegerBoxSID(String placeholder, int value) {
        this(placeholder);
        setValue(value);
    }

    @Override
    protected Integer parseNumber(double number) {
        if (Double.isNaN(number)) {
            return null;
        }
        return (int) Math.rint(number);
    }
}

