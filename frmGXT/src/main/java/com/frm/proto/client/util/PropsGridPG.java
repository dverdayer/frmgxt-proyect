/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.util;

import com.frm.proto.client.salpi.BaseModelData;
//import com.extjs.gxt.ui.client.data.BaseModelData;


/**
 *
 * @author diego
 */
public class PropsGridPG {
         
    private Recurso recurso;
    private String strEntidad;
    private String[] ids;
    private String[] titulos;
    private String[] tiposCampos;
    private boolean[] trigger;
    private boolean[] editables;
    private String[] gridCL;    
    private BaseModelData bmdVentanaLoad;
    private BaseModelData bmdPB;
    
    public PropsGridPG(){}
    
    public PropsGridPG(Recurso recurso, String strEntidad, String [] ids,
        String[] titulos, String[] tiposCampos){       
        this.recurso = recurso;
        this.ids = ids;
        this.titulos = titulos;
        this.tiposCampos = tiposCampos;
        this.strEntidad = strEntidad;        
    }   

    /*full constructor*/

    public PropsGridPG(Recurso recurso, String strEntidad, String[] ids, String[] titulos, String[] tiposCampos
            , boolean[] trigger, boolean[] editables, String[] gridCL, BaseModelData bmdVentanaLoad, BaseModelData bmdPB) {
        this.recurso = recurso;
        this.strEntidad = strEntidad;
        this.ids = ids;
        this.titulos = titulos;
        this.tiposCampos = tiposCampos;
        this.trigger = trigger;
        this.editables = editables;
        this.gridCL = gridCL;
        this.bmdVentanaLoad = bmdVentanaLoad;
        this.bmdPB = bmdPB;
    }    
    
    public String[] getIds() {
        return ids;
    }

    public void setIds(String[] ids) {
        this.ids = ids;
    }

    public String[] getTitulos() {
        return titulos;
    }

    public void setTitulos(String[] titulos) {
        this.titulos = titulos;
    }

    public String[] getTiposCampos() {
        return tiposCampos;
    }

    public void setTiposCampos(String[] tiposCampos) {
        this.tiposCampos = tiposCampos;
    }
   
    public Recurso getRecurso() {
        return recurso;
    }

    public void setRecurso(Recurso recurso) {
        this.recurso = recurso;
    }

    public String getStrEntidad() {
        return strEntidad;
    }

    public void setStrEntidad(String strEntidad) {
        this.strEntidad = strEntidad;
    }

    public boolean[] getTrigger() {
        return trigger;
    }

    public void setTrigger(boolean[] trigger) {
        this.trigger = trigger;
    }

    public boolean[] getEditables() {
        return editables;
    }

    public void setEditables(boolean[] editables) {
        this.editables = editables;
    }

    public String[] getGridCL() {
        return gridCL;
    }

    public void setGridCL(String[] gridCL) {
        this.gridCL = gridCL;
    }

    public BaseModelData getBmdVentanaLoad() {
        return bmdVentanaLoad;
    }

    public void setBmdVentanaLoad(BaseModelData bmdVentanaLoad) {
        this.bmdVentanaLoad = bmdVentanaLoad;
    }

    public BaseModelData getBmdPB() {
        return bmdPB;
    }

    public void setBmdPB(BaseModelData bmdPB) {
        this.bmdPB = bmdPB;
    }
    
}
