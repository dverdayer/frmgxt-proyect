/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author diego
 */
public class RegObjClient {
    
    protected static Map<String, Object> map = new HashMap<>();
    
    
    @SuppressWarnings("unchecked")
    public static <X> X get(String id) {
        return (X) map.get(id);
    }
    
    
    public static Map<String, Object> getAll() {
        return map;
    }
    
    
    public static void register(String id, Object obj) {
        map.put(id, obj);
    }
    
    public static void unregister(String id) {
        map.remove(id);
    }
    
    public static void unregisterByControlador(String ssControlador) {
        map.entrySet().stream().map((entry) 
                -> entry.getKey()).filter((strKey) 
                        -> (strKey.contains("_"+ssControlador+"_"))).forEach((strKey) -> {
            map.remove(strKey);
        });
//        for (Map.Entry<String, Object> entry : map.entrySet()) {
//            String strKey = entry.getKey();
//            if(strKey.contains("_"+ssControlador+"_")){
//                map.remove(strKey);
//            }
//        }                
    }

    /**
    * Unregisters all registered objects.
    */
    public static void unregisterAll() {
        map.clear();
    }
     
         
}
