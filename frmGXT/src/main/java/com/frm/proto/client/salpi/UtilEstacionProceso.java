/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client.salpi;

/**
 *
 * @author diego
 */
public class UtilEstacionProceso {
    private Integer kaNlFormulario;
    private Integer nlflujo;
    private String consAuditoria;
    private String consProcesoAnterior;
    private Integer nlFormularioResp;

    public UtilEstacionProceso() {
    }

    public UtilEstacionProceso(Integer kaNlFormulario, Integer nlflujo, String consAuditoria) {
        this.kaNlFormulario = kaNlFormulario;
        this.nlflujo = nlflujo;
        this.consAuditoria = consAuditoria;
    }

    public String getConsAuditoria() {
        return consAuditoria;
    }

    public void setConsAuditoria(String consAuditoria) {
        this.consAuditoria = consAuditoria;
    }

    public Integer getKaNlFormulario() {
        return kaNlFormulario;
    }

    public void setKaNlFormulario(Integer kaNlFormulario) {
        this.kaNlFormulario = kaNlFormulario;
    }

    public Integer getNlflujo() {
        return nlflujo;
    }

    public void setNlflujo(Integer nlflujo) {
        this.nlflujo = nlflujo;
    }

    public String getConsProcesoAnterior() {
        return consProcesoAnterior;
    }

    public void setConsProcesoAnterior(String consProcesoAnterior) {
        this.consProcesoAnterior = consProcesoAnterior;
    }

    public Integer getNlFormularioResp() {
        return nlFormularioResp;
    }

    public void setNlFormularioResp(Integer nlFormularioResp) {
        this.nlFormularioResp = nlFormularioResp;
    }

}
