/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client.init;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.salpi.BaseModelData;


/**
 *
 * @author diego
 */
public class MaeClienteSPLE {
    private String[] ids;
    private String[] titulos;
    private String[] tipoCampo;
    private String[] tipoCampoF;
    private int[] longCampos;
    private int[] longCamposF;
    private String strEntidad;
    private boolean [] Editables;
    private String[] idsFilter;
    private BaseModelData paretosBorrar;
    private BaseModelData bmdVL;
    private int numColumns = 1;

    public MaeClienteSPLE(String[] ids, String[] titulos, String[] tipoCampo, int[] longCampos, String strEntidad, boolean[] Editables, String[] idsFilter, BaseModelData paretosBorrar, int numColumns) {
        this.ids = ids;
        this.titulos = titulos;
        this.tipoCampo = tipoCampo;
        this.tipoCampoF = tipoCampo;
        this.longCampos = longCampos;
        this.longCamposF = longCampos;
        this.strEntidad = strEntidad;
        this.Editables = Editables;
        this.idsFilter = idsFilter;
        this.paretosBorrar = paretosBorrar;
        this.numColumns = numColumns;
    }

    public MaeClienteSPLE(String[] ids, String[] titulos, String[] tipoCampo, int[] longCampos, int[] longCamposF, String strEntidad, boolean[] Editables, String[] idsFilter, BaseModelData paretosBorrar, int numColumns) {
        this.ids = ids;
        this.titulos = titulos;
        this.tipoCampo = tipoCampo;
        this.tipoCampoF = tipoCampo;
        this.longCampos = longCampos;
        this.longCamposF = longCamposF;
        this.strEntidad = strEntidad;
        this.Editables = Editables;
        this.idsFilter = idsFilter;
        this.paretosBorrar = paretosBorrar;
        this.numColumns = numColumns;
    }

    public MaeClienteSPLE(String[] ids, String[] titulos, String[] tipoCampo, String[] tipoCampoF, int[] longCampos, int[] longCamposF, String strEntidad, boolean[] Editables, String[] idsFilter, BaseModelData paretosBorrar, BaseModelData bmdVL, int numColumns) {
        this.ids = ids;
        this.titulos = titulos;
        this.tipoCampo = tipoCampo;
        this.tipoCampoF = tipoCampoF;
        this.longCampos = longCampos;
        this.longCamposF = longCamposF;
        this.strEntidad = strEntidad;
        this.Editables = Editables;
        this.idsFilter = idsFilter;
        this.paretosBorrar = paretosBorrar;
        this.bmdVL = bmdVL;
        this.numColumns = numColumns;
    }

    public boolean[] getEditables() {
        return Editables;
    }

    public void setEditables(boolean[] Editables) {
        this.Editables = Editables;
    }

    public String[] getIds() {
        return ids;
    }

    public void setIds(String[] ids) {
        this.ids = ids;
    }

    public int[] getLongCampos() {
        return longCampos;
    }

    public void setLongCampos(int[] longCampos) {
        this.longCampos = longCampos;
    }

    public String getStrEntidad() {
        return strEntidad;
    }

    public void setStrEntidad(String strEntidad) {
        this.strEntidad = strEntidad;
    }

    public String[] getTipoCampo() {
        return tipoCampo;
    }

    public void setTipoCampo(String[] tipoCampo) {
        this.tipoCampo = tipoCampo;
    }

    public String[] getTitulos() {
        return titulos;
    }

    public void setTitulos(String[] titulos) {
        this.titulos = titulos;
    }

    public String[] getIdsFilter() {
        return idsFilter;
    }

    public void setIdsFilter(String[] idsFilter) {
        this.idsFilter = idsFilter;
    }

    public BaseModelData getParetosBorrar() {
        return paretosBorrar;
    }

    public void setParetosBorrar(BaseModelData paretosBorrar) {
        this.paretosBorrar = paretosBorrar;
    }

    public BaseModelData getBmdVL() {
        return bmdVL;
    }

    public void setBmdVL(BaseModelData bmdVL) {
        this.bmdVL = bmdVL;
    }

    public int getNumColumns() {
        return numColumns;
    }

    public void setNumColumns(int numColumns) {
        this.numColumns = numColumns;
    }

    public int[] getLongCamposF() {
        return longCamposF;
    }

    public void setLongCamposF(int[] longCamposF) {
        this.longCamposF = longCamposF;
    }

    public String[] getTipoCampoF() {
        return tipoCampoF;
    }

    public void setTipoCampoF(String[] tipoCampoF) {
        this.tipoCampoF = tipoCampoF;
    }


}
