package com.frm.proto.client.table.dto;

/*
 * #%L
 * GwtMaterial
 * %%
 * Copyright (C) 2015 - 2016 GwtMaterialDesign
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.frm.proto.client.salpi.AdmUsuariosAplicaLigth;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.RegObjClient;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import gwt.material.design.addins.client.autocomplete.base.MaterialSuggestionOracle;
import gwt.material.design.client.ui.MaterialToast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class UserOracle extends MaterialSuggestionOracle{

    private List<User> contacts = new LinkedList<>();

    public void addContacts(List<User> users) {
        contacts.addAll(users);
    }

    @Override
    public void requestSuggestions(final Request request2, final Callback callback) {                   
        final Response resp = new Response();
        int offset = 10;
        int nlCall = RegObjClient.get(PUN.CALL_BACK);
        final String callback2 = "jsonp_callbal"+PUN.SEPARA+nlCall;       
        RegObjClient.register(PUN.CALL_BACK, nlCall+1);    
        
        final AdmUsuariosAplicaLigth aual = (AdmUsuariosAplicaLigth) RegObjClient.get(PUN.STR_USER_ACTUAL); 
        final String strUrl = GWT.getHostPageBaseURL()+ PUN.URL_JSON_LIST                        
                    +"&"+PUN.APLU+"="+aual.getKaNlUsuario()
                    +"&"+PUN.ENTIDAD+"="+"admUsuarios"
                    +"&"+PUN.CONTROLADOR+"="+"CONTROLADOR_AMESTRO"     
                    +"&"+PUN.XML_RECURSO+"="+"NINININININI";      
                
        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, strUrl
                            +"&"+PUN.CALL_BACK+"="+callback2
                            +"&"+PUN.START_PADDIN+"="+offset
                            +"&"+PUN.PAGE_PADDIN+"="+10);
        try {
            com.google.gwt.http.client.Request request1 = builder.sendRequest("admUsuarios", new RequestCallback() {
                @Override
                public void onError(com.google.gwt.http.client.Request request, Throwable exception) {
                   MaterialToast.fireToast("Error en el escuchador: "+strUrl);
                }

                @Override
                public void onResponseReceived(com.google.gwt.http.client.Request request, com.google.gwt.http.client.Response response) {
                    if (200 == response.getStatusCode()) {
                        
                        List<UserSuggestion> list = new ArrayList<>();
                        int i=0;
                        
                        for(User contact : contacts){    
                            
                                list.add(new UserSuggestion(contact));
                                                        
                            i++;
                            if(i>3){
                                break;
                            }
                        }
                        
                        resp.setSuggestions(list);
                        callback.onSuggestionsReady(request2, resp);
                        if(response.getText().indexOf("(") > 0){
//                            String strCallbackId = response.getText().substring(0, response.getText().indexOf("("));
//                            if(strCallbackId.equals(callback)){
//                                String strJsonP = response.getText().substring(response.getText().indexOf("(")+1, response.getText().length()-1);                                                                                                
//                                
//                                MyJsonReader jr = new MyJsonReader(mt);
//                                List lstB = (List<BaseModelData>)jr.read(null, strJsonP);
//                                JSONObject jsonRoot = (JSONObject) JSONParser.parse((String) strJsonP);
//                                JSONValue totalReg = jsonRoot.get(PUN.TOTAL_COUNT);
//                                if(totalReg.isNumber() != null){
//                                    totalRows = ((Double)totalReg.isNumber().doubleValue()).intValue();
//                                }                           
//                                table.setVisibleRange(0, lstB.size());
//                                table.loaded(0, lstB);
//                                updateUi();
//                            }
                        }                                                                                                                
                    }else{

                    }
                }
            });
        } catch (RequestException e) {            
            MaterialToast.fireToast("Error la transmisión AJAX" );
        }
    }
}