/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.teble.sid;

import com.frm.proto.client.table.dto.DataPagerRemote;
import com.google.gwt.event.dom.client.KeyCodes;
//import gwt.material.design.client.ui.pager.actions.PageNumberBox;

/**
 *
 * @author diego
 */
public class PageNumberBoxSID extends AbstractPageSelectionSID<MaterialIntegerBoxSID> implements PageSelectionSID {

//    PageNumberBox
    protected MaterialIntegerBoxSID integerBox = new MaterialIntegerBoxSID();
    public PageNumberBoxSID() {
        super();
    }

    public PageNumberBoxSID(DataPagerRemote<?> pager) {
        super(pager);
    }

    @Override
    protected void setup() {
        integerBox.setMin("1");
        registerHandler(addKeyDownHandler(keyDownEvent -> {
            if (keyDownEvent.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                pager.gotoPage(integerBox.getValue());
            }
        }));
    }

    @Override
    public MaterialIntegerBoxSID getComponent() {
        return integerBox;
    }
}
