/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.teble.sid;


import com.frm.proto.client.table.dto.DataPagerRemote;
import com.google.gwt.dom.client.Document;
import gwt.material.design.client.base.MaterialWidget;
import gwt.material.design.client.base.constants.TableCssName;
import gwt.material.design.client.constants.HideOn;
import gwt.material.design.client.ui.MaterialListValueBox;
import gwt.material.design.client.ui.html.Span;
/**
 *
 * @author diego
 */
public class RowSelectionSID extends MaterialWidget {

    private final DataPagerRemote pager;
    protected Span rowsPerPageLabel = new Span("Rows per page");
    protected MaterialListValueBox<Integer> listPageRows = new MaterialListValueBox<>();

    public RowSelectionSID(DataPagerRemote pager) {
        super(Document.get().createDivElement(), TableCssName.ROWS_PER_PAGE_PANEL);
        this.pager = pager;

        setHideOn(HideOn.HIDE_ON_SMALL_DOWN);
    }

    @Override
    protected void onLoad() {
        super.onLoad();

        add(listPageRows);
        add(rowsPerPageLabel);

        listPageRows.clear();
        for (int limitOption : pager.getLimitOptions()) {
            listPageRows.addItem(limitOption, false);
        }
        listPageRows.reload();

        registerHandler(listPageRows.addValueChangeHandler(valueChangeEvent -> {
            pager.setLimit(valueChangeEvent.getValue());
            pager.updateRowsPerPage(valueChangeEvent.getValue());
        }));
    }
}
