/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author diegohernandez
 */
public class ModelGenericPadinCombo extends BaseModelData  implements IsSerializable {

    public ModelGenericPadinCombo(){
    }

    private String strCampoVisual = "";

    public String toString() {
        return (String)getCampo(strCampoVisual);
    }
    public Object getCampo(String keyCampo){
        return get(keyCampo);
    }

    public String getStrCampoVisual() {
        return strCampoVisual;
    }

    public void setStrCampoVisual(String strCampoVisual) {
        this.strCampoVisual = strCampoVisual;
    }


}
