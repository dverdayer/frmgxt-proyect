/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.teble.sid;

/**
 *
 * @author diego
 */

import gwt.material.design.client.base.NumberBox;
import gwt.material.design.client.base.NumberBox.NumberHandler;
import gwt.material.design.client.constants.InputType;
import gwt.material.design.client.ui.MaterialValueBox;

import static gwt.material.design.jquery.client.api.JQuery.$;

//@formatter:off

/**
 * Material Number Box is the base class for other numeric input boxes, such as {@link MaterialIntegerBox} and
 * {@link MaterialDoubleBox}.
 *
 * @author paulux84
 * @see <a href="http://gwtmaterialdesign.github.io/gwt-material-demo/#textfields">Material MaterialNumberBox</a>
 * @see <a href="https://material.io/guidelines/components/text-fields.html#">Material Design Specification</a>
 */
//@formatter:on
public abstract class MaterialNumberBoxSID<T> extends MaterialValueBoxSID<T> {

    protected MaterialNumberBoxSID() {
//        setup(new NumberBox<>(new NumberHandler<>(this)));
        setType(InputType.NUMBER);
    }

    /**
     * Set step attribute to input element.
     *
     * @param step "any" or number like for example 1 or 2.5 or 100, etc...
     */
    public void setStep(String step) {
        valueBoxBase.getElement().setAttribute("step", step);
    }

    public String getStep() {
        return valueBoxBase.getElement().getAttribute("step");
    }

    public void setMin(String min) {
        valueBoxBase.getElement().setAttribute("min", min);
    }

    public String getMin() {
        return valueBoxBase.getElement().getAttribute("min");
    }

    public void setMax(String max) {
        valueBoxBase.getElement().setAttribute("max", max);
    }

    public String getMax() {
        return valueBoxBase.getElement().getAttribute("max");
    }

    @Override
    public T getValue() {
        if (getValueAsNumber() != null) {
            return parseNumber(getValueAsNumber());
        }
        return null;
    }

    protected abstract T parseNumber(double number);

    /**
     * Returns the value parsed natively by the browser.
     *
     * @return the value set on the component, or NaN if none is set
     */
    public Double getValueAsNumber() {
        String value = (String) $(valueBoxBase.getElement()).val();
        if (value != null && !value.isEmpty()) {
            return Double.parseDouble(value);
        } else {
            return null;
        }
    }
}
