/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.init;

/**
 *
 * @author diego
 */
public enum FormType {
    _FORM_TRANS("_FORM_TRANS"), _FORM_TRANS_HIJO("_FORM_TRANS_HIJO");    
    
    private final String TYPE_FORM;
    
    FormType(final String strFORM) {
        TYPE_FORM = strFORM;
    }
        
    public String getTYPE_FORM(){
        return TYPE_FORM;
    }
}
