/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client.init;

/**
 *
 * @author diego
 */
public class FormColSpan {

    private String filedSet;
    private int PostVectHasta;
    private int numColumns;

    public FormColSpan(int PostVectHasta, int numColumns) {
        this.PostVectHasta = PostVectHasta;
        this.numColumns = numColumns;
    }

    public FormColSpan(String filedSet, int PostVectHasta, int numColumns) {
        this.filedSet = filedSet;
        this.PostVectHasta = PostVectHasta;
        this.numColumns = numColumns;
    }

    public int getPostVectHasta() {
        return PostVectHasta;
    }

    public void setPostVectHasta(int PostVectHasta) {
        this.PostVectHasta = PostVectHasta;
    }

    public int getNumColumns() {
        return numColumns;
    }

    public void setNumColumns(int numColumns) {
        this.numColumns = numColumns;
    }

    public String getFiledSet() {
        return filedSet;
    }

    public void setFiledSet(String filedSet) {
        this.filedSet = filedSet;
    }
    
}
