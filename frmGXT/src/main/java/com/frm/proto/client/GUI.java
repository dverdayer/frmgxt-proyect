/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client;

import com.frm.proto.client.salpi.PUN;
import com.google.gwt.user.client.Window;
import gwt.material.design.client.constants.HideOn;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialFAB;
import gwt.material.design.client.ui.MaterialTab;


/**
 *
 * @author diego
 */
public interface GUI {
     public String getEntidad();
     public String getControlador();
     public int getNlPermisos();
     public String getStrXML(); 
     public void onRender();
     public static void valueNavBar(String strAction, MaterialTab mtabsHeader, MaterialFAB mFab){
        if(strAction.equals(PUN.EVN_SID_NAV_OPEN)){
            int width = Window.getClientWidth();
            mtabsHeader.setStyle("width: "+(width-300)+"px; position: fixed; z-index: 997;");
        }else if(strAction.equals(PUN.EVN_SID_NAV_CLOSE)){
            mtabsHeader.setStyle("position: fixed; z-index: 997;");
        }else if(strAction.equals(PUN.SELECT_ROW) && mFab != null){
            mFab.setVisible(true);
        }
     }
}
