/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client.salpi;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;

/**
 *
 * @author diegohernandez
 */
public class ObjectBMDTransFD implements IsSerializable{
    private Integer nlKey;
    private String strLabel;
    private String estReg;
    private BaseModelData entidad;
    private BaseModelData bmdCampoRefe;
    private List<BaseModelData> lstControls;
    private Integer nlOrden;
    private String ssTipoAgr;
    private String ssIdhtml;
    private List<BaseModelData> data;
    private String ssOnChange;
    private String ssLabelTop;


    public ObjectBMDTransFD() {
    }

    public ObjectBMDTransFD(Integer nlKey, String strLabel, BaseModelData entidad, List<BaseModelData> lstControls, Integer nlOrden, String ssTipoAgr
            , String ssOnChange, String ssLabelTop ) {
        this.strLabel = strLabel;
        this.entidad = entidad;
        this.lstControls = lstControls;
        this.nlOrden = nlOrden;
        this.ssTipoAgr = ssTipoAgr;
        this.nlKey = nlKey;
        this.ssOnChange = ssOnChange;
        this.ssLabelTop = ssLabelTop ;

    }

    public String getStrLabel() {
        return strLabel;
    }

    public void setStrLabel(String strLabel) {
        this.strLabel = strLabel;
    }

    public List<BaseModelData> getLstControls() {
        return lstControls;
    }

    public void setLstControls(List<BaseModelData> lstControls) {
        this.lstControls = lstControls;
    }

    public BaseModelData getEntidad() {
        return entidad;
    }

    public void setEntidad(BaseModelData entidad) {
        this.entidad = entidad;
    }

    public Integer getNlOrden() {
        return nlOrden;
    }

    public void setNlOrden(Integer nlOrden) {
        this.nlOrden = nlOrden;
    }

    public String getSsTipoAgr() {
        return ssTipoAgr;
    }

    public void setSsTipoAgr(String ssTipoAgr) {
        this.ssTipoAgr = ssTipoAgr;
    }

    public Integer getNlKey() {
        return nlKey;
    }

    public void setNlKey(Integer nlKey) {
        this.nlKey = nlKey;
    }

    public String getEstReg() {
        return estReg;
    }

    public void setEstReg(String estReg) {
        this.estReg = estReg;
    }

    public BaseModelData getBmdCampoRefe() {
        return bmdCampoRefe;
    }

    public void setBmdCampoRefe(BaseModelData bmdCampoRefe) {
        this.bmdCampoRefe = bmdCampoRefe;
    }

    public String getSsIdhtml() {
        return ssIdhtml;
    }

    public void setSsIdhtml(String ssIdhtml) {
        this.ssIdhtml = ssIdhtml;
    }

    public List<BaseModelData> getData() {
        return data;
    }

    public void setData(List<BaseModelData> data) {
        this.data = data;
    }

    public String getSsOnChange() {
        return ssOnChange;
    }

    public void setSsOnChange(String ssOnChange) {
        this.ssOnChange = ssOnChange;
    }

    public String getSsLabelTop() {
        return ssLabelTop;
    }

    public void setSsLabelTop(String ssLabelTop) {
        this.ssLabelTop = ssLabelTop;
    }
    
}
