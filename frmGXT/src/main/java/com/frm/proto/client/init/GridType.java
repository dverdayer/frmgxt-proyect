/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.init;

/**
 *
 * @author diego
 */
public enum GridType {
    PAGINADO_SELECT("PAGINADO")
    , PAGINADO_SELECT_ONLY("PAGINADO_ONLY")
    , PAGINADO_PPP("PAGINADO_PPP")
    , PAGINADO_PPP_GRID("PAGINADO_PPP_GRID")
    , PAGINADO_MAESTRO_TIPO_B("PAGINADO_MAESTRO_TIPO_B")
    , SIMPLE_GRID_TO_GRID("SIMPLE_GRID_TO_GRID")  // para los listas hijo que tienen un formulario 
    , SIMPLE_FORM("SIMPLE_FORM");  // para los listas hijo que tienen un formulario 
    
    private final String TYPE_GRID;
    
    GridType(final String strGRID) {
        TYPE_GRID = strGRID;
    }
        
    public String getTYPE_GRID(){
        return TYPE_GRID;
    }
}
