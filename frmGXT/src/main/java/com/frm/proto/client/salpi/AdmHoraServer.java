/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client.salpi;

import com.google.gwt.user.client.rpc.IsSerializable;
/**
 *
 * @author diegohernandez
 */
public class AdmHoraServer implements IsSerializable{
    private int hora;
    private int minuto;
    private int segundo;

    public AdmHoraServer() {
    }

    public AdmHoraServer(int hora, int minuto, int segundo) {
        this.hora = hora;
        this.minuto = minuto;
        this.segundo = segundo;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }

    public int getSegundo() {
        return segundo;
    }

    public void setSegundo(int segundo) {
        this.segundo = segundo;
    }

}
