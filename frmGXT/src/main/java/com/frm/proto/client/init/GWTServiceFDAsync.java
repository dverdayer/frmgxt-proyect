/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client.init;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.MaestroEntidad;
import com.frm.proto.client.salpi.MsgFinalUser;
import com.frm.proto.client.salpi.ObjBMDForm;
import com.frm.proto.client.salpi.ObjBMDGrid;
import com.frm.proto.client.salpi.ObjBMDListOpcFD;
import com.frm.proto.client.salpi.PropertiesEntidad;
import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;

/**
 *
 * @author diegohernandez
 */
public interface GWTServiceFDAsync {

    public abstract void getFormulariosDinamicos(String strEntidad, Integer kaNlFormulario, BaseModelData bmd, AsyncCallback<ObjBMDListOpcFD> lstTFD);

    public abstract void getMaestroEntidad(BaseModelData bms, AsyncCallback<MaestroEntidad> me);

    public abstract void getPropertiesRegal(Integer kaNlFormulario, AsyncCallback<List<PropertiesEntidad>> lstPro);

    public abstract void getCamposByEntidad(Integer kaNlEntidad, AsyncCallback<List<String>> lst);

    public abstract void guardar(String strXML, BaseModelData bmd, Integer kaNlFormulario, List<ObjBMDForm> lstForm
            , List<ObjBMDGrid> lstGrid, List<BaseModelData> lstEliminar, boolean generarVersion, AsyncCallback<MsgFinalUser> mfu);

    public abstract void guardar_process(String strXML, BaseModelData bmd, Integer kaNlFormulario, List<ObjBMDForm> lstForm
            , List<ObjBMDGrid> lstGrid, List<BaseModelData> lstEliminar, AsyncCallback<MsgFinalUser> mfu);

    public abstract void guardar_processWebPage(String strXML, List<ObjBMDForm> lstForm, List<ObjBMDGrid> lstGrid, AsyncCallback<MsgFinalUser> mfu);

    public abstract void guardar(String strEntidadOculta, String strXML, List<BaseModelData> lstEliminar, AsyncCallback<MsgFinalUser> mfu);

    public abstract void getLSUsuariosFlujoByEstado(BaseModelData bmdEst, List<ObjBMDForm> lstForms, List<ObjBMDGrid> lstGrids, AsyncCallback<MsgFinalUser> mfu);


}
