/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client.init;

import com.frm.proto.client.salpi.BaseModelData;
//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.salpi.MaestroEntidad;
import com.frm.proto.client.salpi.MsgFinalUser;
import com.frm.proto.client.salpi.ObjBMDForm;
import com.frm.proto.client.salpi.ObjBMDGrid;
import com.frm.proto.client.salpi.ObjBMDListOpcFD;
import com.frm.proto.client.salpi.PropertiesEntidad;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.List;

/**
 *
 * @author diegohernandez
 */
@RemoteServiceRelativePath("gwtservicefd")
public interface GWTServiceFD extends RemoteService {
    public ObjBMDListOpcFD getFormulariosDinamicos(String strEntidad, Integer kaNlFormulario, BaseModelData bmd);

    public MaestroEntidad getMaestroEntidad(BaseModelData bmd);

    public List<PropertiesEntidad> getPropertiesRegal(Integer kaNlFormulario);

    public List<String> getCamposByEntidad(Integer kaNlEntidad);

    public MsgFinalUser guardar(String strXML, BaseModelData bmd, Integer kaNlFormulario, List<ObjBMDForm> lstForm, List<ObjBMDGrid> lstGrid, List<BaseModelData> lstEliminar, boolean generarVersion);

    public MsgFinalUser guardar_process(String strXML, BaseModelData bmd, Integer kaNlFormulario, List<ObjBMDForm> lstForm, List<ObjBMDGrid> lstGrid, List<BaseModelData> lstEliminar);

    public MsgFinalUser guardar_processWebPage(String strXML, List<ObjBMDForm> lstForm, List<ObjBMDGrid> lstGrid);

    public MsgFinalUser guardar(String strEntidadOculta, String strXML, List<BaseModelData> lstEliminar);

    public MsgFinalUser getLSUsuariosFlujoByEstado(BaseModelData bmd, List<ObjBMDForm> lstForms, List<ObjBMDGrid> lstGrids);

}
