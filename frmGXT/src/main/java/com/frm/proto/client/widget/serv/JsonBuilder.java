/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.widget.serv;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.ModelGeneric;
import com.frm.proto.client.RegObjClient;
import com.frm.proto.client.salpi.AdmUsuariosAplicaLigth;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.table.dto.DataPagerRemote;
import com.frm.proto.client.table.dto.EntidadCBX;
import com.frm.proto.client.ui.MaterialDataTableCuston;
import com.frm.proto.client.util.ModelType;
import com.frm.proto.client.util.MyJsonReader;
import com.frm.proto.client.util.PropsGridPG;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.dom.client.Style;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.frm.proto.client.util.Recurso;
import com.frm.proto.client.util.SGUI;
import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONBoolean;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.ui.SuggestOracle;
import gwt.material.design.addins.client.combobox.MaterialComboBox;
import gwt.material.design.client.base.AbstractValueWidget;
import gwt.material.design.client.base.MaterialWidget;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.constants.WavesType;
import gwt.material.design.client.ui.MaterialCollapsible;
import gwt.material.design.client.ui.MaterialCollapsibleBody;
import gwt.material.design.client.ui.MaterialCollapsibleHeader;
import gwt.material.design.client.ui.MaterialCollapsibleItem;
import gwt.material.design.client.ui.MaterialLink;
import gwt.material.design.client.ui.MaterialListValueBox;
import gwt.material.design.client.ui.MaterialRow;
import gwt.material.design.client.ui.MaterialSideNavPush;
import gwt.material.design.client.ui.MaterialToast;
import gwt.material.design.client.ui.html.ListItem;
import gwt.material.design.client.ui.html.UnorderedList;
import gwt.material.design.gen.IconTypeGenerator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author diego
 */
public final class JsonBuilder {    
    
    public static void refreshArbolNavigator(String strEntityJPA, ModelGeneric modelGeneric, final MaterialSideNavPush sideNav, final Map<Integer, Recurso> recursos) {
        
        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, builderReq(strEntityJPA, modelGeneric));
        try {
            Request request = builder.sendRequest(strEntityJPA, new RequestCallback() {
                @Override
                public void onError(Request request, Throwable exception) {
//                   MessageBox.alert("Alert2","Couldn't retrieve JSON", null);
                }

                @Override
                public void onResponseReceived(Request request, Response response) {
                    if (200 == response.getStatusCode()) {
                        JSONValue value = JSONParser.parse(response.getText());                                             
                        List<Recurso> lstRecurso = parseJsonData(value);

                        if(lstRecurso != null && !lstRecurso.isEmpty()){
                             MaterialCollapsible mc = new MaterialCollapsible();
                            mc.setAccordion(true);
//                                mc.setHideOn(HideOn.HIDE_ON_MED_DOWN);
                            mc.setHoverable(true);
                            for(Recurso recurso: lstRecurso){

                                if(recurso.getProperties() != null && recurso.getProperties().size() > 0){
    //                                MaterialLink ml = new MaterialLink(recurso.getSsRecurso(), "white");                            
                                    MaterialLink ml = new MaterialLink(recurso.getSsRecurso()); 
                                    IconType it = IconType.fromStyleName("terrain");
                                    ml.setIconType(it);
//                                    ml.setIconType(IconType.DRAFTS);
                                    ml.setWaves(WavesType.GREEN);
    //                                ml.setIcon("mdi-navigation-apps");
    //                                ml.setIconPosition("left");                                 
    //                                ml.addStyleName("waves-effect waves-" + "default");
    //                                ml.getElement().getStyle().setDisplay(Style.Display.BLOCK);                                                                                         

    //                                navBar.add(ml);                                   
                                    MaterialCollapsibleHeader mch = new MaterialCollapsibleHeader();
                                    mch.add(ml);

                                    MaterialCollapsibleItem mci = new MaterialCollapsibleItem(); 
                                    MaterialCollapsibleBody mcb = new MaterialCollapsibleBody();
                                    UnorderedList ul = new UnorderedList();

                                    for(Recurso r :recurso.getProperties()){                                                                                                              
                                        MaterialLink mlh = new MaterialLink(r.getSsRecurso(), "black");
                                        mlh.setHref("#VIEW"+ PUN.SEPARA+r.getKaNlRecurso().intValue());
//                                        mlh.addStyleName("waves-effect waves-" + "default");                                    
                                        mlh.setWaves(WavesType.RED);
    //                                    mlh.addStyleName("waves-effect");
                                        mlh.getElement().getStyle().setDisplay(Style.Display.BLOCK);                                                                                           
                                        mlh.getElement().getStyle().setColor("#263238");                                                                                           
                                        mlh.getElement().getStyle().setPaddingLeft(10, Style.Unit.PX);
                                        mlh.getElement().getStyle().setPaddingTop(20, Style.Unit.PX);


                                        recursos.put(r.getKaNlRecurso().intValue(), r);                                    
                                        ul.add(mlh);

                                    }
                                    mci.add(mch); 
                                    mcb.add(ul);
    //                                mch.setWaves(WavesType.DEFAULT);
                                                   
                                    mci.add(mcb);
                                    mc.add(mci);                                                                    


                                }else{
                                    MaterialLink ml = new MaterialLink(recurso.getSsRecurso(), "white");                            
                                    ml.setHref("#VIEW"+ PUN.SEPARA+recurso.getKaNlRecurso().intValue());
                                    ml.addStyleName("waves-effect waves-" + "default");
                                    ml.getElement().getStyle().setDisplay(Style.Display.BLOCK);                                                          

                                    recursos.put(recurso.getKaNlRecurso().intValue(), recurso);                                

                                    ListItem li = new ListItem();
                                    li.add(ml);
                                    sideNav.add(li);
                                }
                            }
                            sideNav.add(mc);
                        }
                    }else{
//                      MessageBox.alert("Alert1", response.getStatusText(), null);
                    }
                }
            });
        } catch (RequestException e) {
//            MessageBox.alert("Alert4", e.getMessage(), null);
        }
    }
    
    /**
     *
     * @param strEntityJPA  Entidad a la que se va a llamar 
     * @param arrStrParam Arreglo de parametros en un model data
     * lo que se hace aqui es la serializacion de parametros para que el escuchador los reciba
     * @return retorna una url con los parametros concatenados
     */
    public static String builderReq (String strEntityJPA, ModelGeneric arrStrParam){
        String strUrl =  PUN.URL_JSON_TREE;
        if(strEntityJPA != null && !strEntityJPA.equals("")){
            strUrl +="entity="+strEntityJPA;
        }else{
            strUrl +="entity=Ninguna";
        }        
        if(arrStrParam != null
                && arrStrParam.getPropertyNames() != null
                && arrStrParam.getPropertyNames().size() > 0){            
            Iterator itera =  arrStrParam.getPropertyNames().iterator();
            String strParamR = "";
            while(itera.hasNext()){
                Object strKey = itera.next();
                Object strValue = arrStrParam.get(strKey.toString());
                strParamR += "&"+strKey+"="+strValue;

            }
            return strUrl+strParamR;
        }

        return strUrl;
    }
    
    public static List<Recurso> parseJsonData(JSONValue json) {       
        List<Recurso> lstRecursos= new ArrayList<>();
        if(json != null){
            JSONArray valueArray  = json.isArray();
            for(int i=0; i<valueArray.size(); i++){
                JSONValue jv = valueArray.get(i);
                JSONObject jo = jv.isObject();
                JSONValue jv2 = jo.get("properties");
                
                Recurso rcs = new Recurso();
                rcs.setSsRecurso(jo.get("ssRecurso").toString().replaceAll("\"", ""));                    
                rcs.setSsTipoVentana(jo.get("ssTipoVentana").toString().replaceAll("\"", ""));                                     
                rcs.setKaNlRecurso(((JSONNumber)jo.get("kaNlRecurso")).doubleValue());
                lstRecursos.add(rcs);
                
                if(jv2 != null){                                                                               
                    List<Recurso> lh = new ArrayList<Recurso>();
                    for(int j=0; j<jv2.isArray().size(); j++){
                        JSONValue jvh = jv2.isArray().get(j);
                        JSONObject joh = jvh.isObject();
                        Recurso rcsh = new Recurso();
                        rcsh.setSsRecurso(joh.get("ssRecurso").toString().replaceAll("\"", ""));                    
                        rcsh.setSsTipoVentana(joh.get("ssTipoVentana").toString().replaceAll("\"", ""));
                        rcsh.setSsXml(joh.get("ssXml").toString().replaceAll("\"", ""));                                        
                        rcsh.setKaNlRecurso(((JSONNumber)joh.get("kaNlRecurso")).doubleValue());
                        rcsh.setSsControlador(joh.get("ssVariable").toString().replaceAll("\"", ""));
                        rcsh.setSsH(joh.get("ssH").toString().replaceAll("\"", ""));
                        Double vlrJson = joh.get("nlPermisos").isNumber().doubleValue();
                        rcsh.setNlPermisos(vlrJson.intValue());
                        lh.add(rcsh);
                    }
                    rcs.setProperties(lh);
                }                
            }            
        }
        return lstRecursos;
    }                  
    
    public static BaseModelData getBMDByJson(String strJson){
        JSONValue JsonV = JSONParser.parse(strJson);
        JSONObject jo = JsonV.isObject();
        BaseModelData bmd = DevolverYParseBMD(jo);
        return bmd;
    }
    
    public static BaseModelData DevolverYParseBMD(JSONObject jo) {
        Set<String> ids = jo.keySet();
        Iterator<String> itera = ids.iterator();
        BaseModelData bmd = new BaseModelData();
        while (itera.hasNext()) {
            String strIds = itera.next();
            setCampo(bmd, strIds, jo.get(strIds));
        }
        return bmd;

    }
    
    public static void setCampo(BaseModelData bmd ,String keyCampo, Object obj){
        if(obj instanceof JSONNumber){
            JSONNumber jn = (JSONNumber)obj;
            bmd.set(keyCampo, jn.doubleValue());
        }else if(obj instanceof JSONBoolean){
            JSONBoolean jb = (JSONBoolean)obj;
            bmd.set(keyCampo, jb.booleanValue());
        }else if(obj instanceof JSONString){
            JSONString js = (JSONString) obj;
            bmd.set(keyCampo, js.stringValue());
        }else if(obj instanceof JSONObject){
            JSONObject jo = (JSONObject) obj;
            String strRootE = keyCampo+"_";
            Set<String> ids = jo.keySet();
            Iterator<String> itera = ids.iterator();
            while(itera.hasNext()){
                String strIds = itera.next();
                Object oh = jo.get(strIds);
                if(oh instanceof JSONNumber){
                    JSONNumber jn = (JSONNumber)oh;
                    bmd.set(strRootE+strIds, jn.doubleValue());
                }else if(oh instanceof JSONBoolean){
                    JSONBoolean jb = (JSONBoolean)oh;
                    bmd.set(strRootE+strIds, jb.booleanValue());
                }else if(oh instanceof JSONString){
                    JSONString js = (JSONString) oh;
                    bmd.set(strRootE+strIds, js.stringValue());
                }
            }   
        }
    }
    
    
    
        
    /*Esto se da para los lisboxx sencillos */
//    public static void refreshLisboxORCbx(String strEntidad, String strTipoCampos, final MaterialListValueBox<BaseModelData> mlb){
    public static void refreshLisboxORCbx(String strEntidad, String strTipoCampos, final MaterialWidget omlbOrcbx){
                
        String strModuloActual =  RegObjClient.get(PUN.MODULO_ACTUAL);
        String srtXmlRecuros = strModuloActual.split(":")[2];
        String[] vtIds =  strEntidad.split(":");
        final String strEntidadSID = strTipoCampos.split("_")[2];

        final String strUrl = GWT.getHostPageBaseURL()+PUN.URL_JSON_COMBO_BOX_TOTAL
                    +PUN.XML_RECURSO+"="+srtXmlRecuros+"&"
                    +PUN.REDIREC+"="+PUN.LBX+"&"
                    +PUN.ENTIDAD+"="+strTipoCampos.split("_")[2];     

        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, strUrl
                        +"&"+PUN.CALL_BACK+"= asdfasdf"
                        +"&"+PUN.START_PADDIN+"="+10
                        +"&"+PUN.PAGE_PADDIN+"="+10);
        try {
            Request request1 = builder.sendRequest("admUsuarios", new RequestCallback() {
                @Override
                public void onError(com.google.gwt.http.client.Request request, Throwable exception) {
                   MaterialToast.fireToast("Error en el escuchador: "+strUrl);
                }

                @Override
                public void onResponseReceived(Request request, com.google.gwt.http.client.Response response) {
                    if (200 == response.getStatusCode()) {

                        ModelType mt = DataPagerRemote.buildMT(strEntidadSID, new String [] {PUN.KA_NL, "ssIdCbx", PUN.SS_VALUE_CBX, "ssAgrupadorCbx"}
                                , new String []{PUN.TXN_NN_D0, PUN.TXT_XX, PUN.CBX_NN, PUN.TXT_XX});

                        String strJsonP = response.getText();
                        MyJsonReader jr = new MyJsonReader(mt);
                        List<BaseModelData> lstB = (List<BaseModelData>)jr.read(null, strJsonP);
                        
//                        lstB.stream().forEach((bmd) -> {
                        boolean swIni=true;
                        for(int i=0; i<lstB.size(); i++){
                            BaseModelData bmd = lstB.get(i);
                            if(omlbOrcbx instanceof MaterialListValueBox){
                                MaterialListValueBox mlb = (MaterialListValueBox)omlbOrcbx;
                                mlb.addItem(bmd, bmd.get(PUN.SS_VALUE_CBX).toString());
                            }else if(omlbOrcbx instanceof MaterialComboBox){
                                if(swIni){                                    
                                    MaterialComboBox mlb = (MaterialComboBox)omlbOrcbx;
                                    mlb.addItem("Seleccione", SGUI.bmdINI);
                                    swIni = !swIni;
                                }
                                MaterialComboBox mlb = (MaterialComboBox)omlbOrcbx;
                                mlb.addItem(bmd.get(PUN.SS_VALUE_CBX).toString(), bmd);
                            }
                        }                                                   
                    }
                }
            });
        } catch (RequestException e) {            
            MaterialToast.fireToast("Error la transmisión AJAX" );
        }
    }              
    
    
    /*Esto se da para los autocompletar sencillos */
    public static void refreshEntidad(final SuggestOracle.Request request2, final SuggestOracle.Callback callback
            , String strEntidad, String strIdComponente, MaterialRow mr){
                
        final SuggestOracle.Response resp = new SuggestOracle.Response();
        /** para las implementaciones anteriores el offset siempre comenzaba en 0 asi debe de estar para las grids
         */
        int offset = 0;
        int nlCall = RegObjClient.get(PUN.CALL_BACK);
        final String callback2 = "jsonp_callbal"+PUN.SEPARA+nlCall;       
        RegObjClient.register(PUN.CALL_BACK, nlCall+1);            
        
        final AdmUsuariosAplicaLigth aual = (AdmUsuariosAplicaLigth) RegObjClient.get(PUN.STR_USER_ACTUAL);     
        String strModuloActual =  RegObjClient.get(PUN.MODULO_ACTUAL);
        String srtXmlRecuros = strModuloActual.split(":")[2];                
        
        final String strEntidadI[] = strIdComponente.split(":");        
        String text = request2.getQuery();
        if((text != null && text.length() > 3)){
            
            String strUrlCBX = "";
            if(mr != null){
                BaseModelData bmd = SGUI.getDataForm(mr);
                if(bmd.getPropertyNames() != null &&  bmd.getPropertyNames().size()>0){
                    for(String strKey:bmd.getPropertyNames()){                                    
                        if(strIdComponente.equalsIgnoreCase(strKey)){
                            continue;
                        }
                        if(SGUI.getCampoSerializadoAlert15CBX(strKey) != null){
                            String strValue = ""+bmd.get(strKey);
                            if(strValue.length() > 20){
                                strValue = strValue.substring(0, 20);
                            }                          
                            strUrlCBX += "&"+PUN.FORM_CB_+strKey+"="+strValue;
//                           }
                        }
                    }
                }
            }
            
            String strUrl = GWT.getHostPageBaseURL()+PUN.URL_JSON_COMBO_BOX_AJAX
                    +PUN.XML_RECURSO+"="+srtXmlRecuros+"&"
                    +PUN.APLU+"="+aual.getKaNlUsuario()+"&"
                    +PUN.ENTIDAD+"="+strEntidadI[0]+"&"
                    +PUN.CAMPO_VISUAL+"="+strEntidadI[1]+"&"
                    +PUN.QUERY+"="+text+"&"
                    +PUN.ENTIDAD_PADRE+"="+strEntidad
                    +strUrlCBX;
            
            RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, strUrl
                                +"&"+PUN.CALL_BACK+"="+callback2
                                +"&"+PUN.START_PADDIN+"="+offset
                                +"&"+PUN.PAGE_PADDIN+"="+0);
                                                
            builder.setRequestData(strEntidad);
            try {
                com.google.gwt.http.client.Request request1 = builder.sendRequest(strEntidad, new RequestCallback() {
                    @Override
                    public void onError(com.google.gwt.http.client.Request request, Throwable exception) {
                       MaterialToast.fireToast("Error en el escuchador: "+strUrl);
                    }

                    @Override
                    public void onResponseReceived(com.google.gwt.http.client.Request request, com.google.gwt.http.client.Response response) {
                        if (200 == response.getStatusCode()) {

                            
//                            String strEntidad, String[] ids, String[] tiposCampos
                            PropsGridPG pgpg = new PropsGridPG(null, strEntidadI[0], new String [] {strEntidadI[1]}
                                    , null, new String []{PUN.CBX_NN});
                            ModelType mt = DataPagerRemote.buildMT(pgpg);
//                            ModelType mt = DataPagerRemote.buildMT(strEntidadI[0], new String [] {strEntidadI[1]}
//                                            , new String []{PUN.CBX_NN});

                            String strJsonP = response.getText().substring(response.getText().indexOf("(")+1, response.getText().length()-1);
                            MyJsonReader jr = new MyJsonReader(mt);
                            List<BaseModelData> lstB = (List<BaseModelData>)jr.read(null, strJsonP);

                            List<EntidadCBX> list = new ArrayList<>();
                            int i=0;

                            for(BaseModelData bmd : lstB){                                
                                list.add(new EntidadCBX(bmd, strEntidadI[1]));                                                        
                                i++;
                                if(i>9){
                                    break;
                                }
                            }                        
                            resp.setSuggestions(list);
                            callback.onSuggestionsReady(request2, resp);
                        }else{

                        }
                    }
                });
            } catch (RequestException e) {            
                MaterialToast.fireToast("Error la transmisión AJAX" );
            }
        }else{
            resp.setSuggestions(new ArrayList<>());
        }
    }  
    
    /*Esto se da para los Comboboxcuston */    
    public static void refreshCBE(String strEntidad, String strIdComponente, MaterialRow mr
            , final MaterialComboBox<BaseModelData> mlb){
                        
        /** para las implementaciones anteriores el offset siempre comenzaba en 0 asi debe de estar para las grids
         */
        int offset = 0;
        int nlCall = RegObjClient.get(PUN.CALL_BACK);
        final String callback2 = "jsonp_callbal"+PUN.SEPARA+nlCall;       
        RegObjClient.register(PUN.CALL_BACK, nlCall+1);            
        
        final AdmUsuariosAplicaLigth aual = (AdmUsuariosAplicaLigth) RegObjClient.get(PUN.STR_USER_ACTUAL);     
        String strModuloActual =  RegObjClient.get(PUN.MODULO_ACTUAL);
        String srtXmlRecuros = strModuloActual.split(":")[2];                
        
        final String strEntidadI[] = strIdComponente.split(":");        

            
        String strUrlCBX = "";
        if(mr != null){
            BaseModelData bmd = SGUI.getDataForm(mr);
            if(bmd.getPropertyNames() != null &&  bmd.getPropertyNames().size()>0){
                for(String strKey:bmd.getPropertyNames()){                                    
                    if(strIdComponente.equalsIgnoreCase(strKey)){
                        continue;
                    }
                    if(SGUI.getCampoSerializadoAlert15CBX(strKey) != null){
                        String strValue = ""+bmd.get(strKey);
                        if(strValue.length() > 20){
                            strValue = strValue.substring(0, 20);
                        }                          
                        strUrlCBX += "&"+PUN.FORM_CB_+strKey+"="+strValue;
//                           }
                    }
                }
            }
        }

        String strUrl = GWT.getHostPageBaseURL()+PUN.URL_JSON_COMBO_BOX_AJAX
                +PUN.XML_RECURSO+"="+srtXmlRecuros+"&"
                +PUN.APLU+"="+aual.getKaNlUsuario()+"&"
                +PUN.ENTIDAD+"="+strEntidadI[0]+"&"
                +PUN.CAMPO_VISUAL+"="+strEntidadI[1]+"&"
                +PUN.ENTIDAD_PADRE+"="+strEntidad
                +strUrlCBX;

        RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, strUrl
                            +"&"+PUN.CALL_BACK+"="+callback2
                            +"&"+PUN.START_PADDIN+"="+offset
                            +"&"+PUN.PAGE_PADDIN+"="+500);

        builder.setRequestData(strEntidad);
        try {
            com.google.gwt.http.client.Request request1 = builder.sendRequest(strEntidad, new RequestCallback() {
                @Override
                public void onError(com.google.gwt.http.client.Request request, Throwable exception) {
                   MaterialToast.fireToast("Error en el escuchador: "+strUrl);
                }

                @Override
                public void onResponseReceived(com.google.gwt.http.client.Request request, com.google.gwt.http.client.Response response) {
                    if (200 == response.getStatusCode()) {

                        PropsGridPG pgpg = new PropsGridPG(null, strEntidadI[0], new String [] {strEntidadI[1]}
                                , null, new String []{PUN.CBX_NN});
                        ModelType mt = DataPagerRemote.buildMT(pgpg);
                        String strJsonP = response.getText().substring(response.getText().indexOf("(")+1, response.getText().length()-1);
                        MyJsonReader jr = new MyJsonReader(mt);
                        List<BaseModelData> lstB = (List<BaseModelData>)jr.read(null, strJsonP);
                        
                        BaseModelData bmdValueActual = null;
                        if(mlb.getSingleValue() != null){
                            bmdValueActual = mlb.getSingleValue();
                        }
                        

                        boolean swIni = true;
                        for(BaseModelData bmd : lstB){                                                       //                                                             
//                            if(swIni){                                
//                                mlb.addItem("Seleccione", SGUI.bmdINI);
//                                swIni = !swIni;
//                            }
                            if(bmdValueActual != null && bmdValueActual.get(strEntidadI[1]) != null 
                                    && bmdValueActual.get(strEntidadI[1]).equals(bmd.get(strEntidadI[1])) ){
                                bmdValueActual.setProperties(bmd.getProperties());
                            }else{                              
                                mlb.addItem(bmd.get(strEntidadI[1]).toString(), bmd);
                            }
                        }
                        mlb.setSingleValue(null);
//                        mlb.clear();
                        mlb.setTabIndex(-1);
                        mlb.setSingleValue(null);                                                                                                                                                                                                                         
                        
                    }else{

                    }
                }
            });
        } catch (RequestException e) {            
            MaterialToast.fireToast("Error la transmisión AJAX" );
        }       
    }  
    
    
    /*Esto se da para los Grid sencillos */    
    public static void loadGridSencilloAll(final MaterialDataTableCuston<BaseModelData> table, BaseModelData bmd){
        int nlCall = RegObjClient.get(PUN.CALL_BACK);
        final String callback = "jsonp_callbal"+PUN.SEPARA+nlCall;       
        RegObjClient.register(PUN.CALL_BACK, nlCall+1);    
        PropsGridPG pgpg = table.getPgpg();
        
        final AdmUsuariosAplicaLigth aual = (AdmUsuariosAplicaLigth) RegObjClient.get(PUN.STR_USER_ACTUAL); 
        String strUrl = GWT.getHostPageBaseURL()+ PUN.URL_JSON_LIST                        
                    +"&"+PUN.APLU+"="+aual.getKaNlUsuario()
                    +"&"+PUN.ENTIDAD+"="+pgpg.getStrEntidad()
                    +"&"+PUN.CONTROLADOR+"="+pgpg.getRecurso().getSsControlador()
                    +"&"+PUN.XML_RECURSO+"="+pgpg.getRecurso().getSsXml();      
        
        if(bmd!= null && bmd.getPropertyNames() != null &&  bmd.getPropertyNames().size()>0){
            for(String strKey:bmd.getPropertyNames()){
//                config.set(strEntidad+":"+strKey, bmd.get(strKey));
                strUrl=strUrl+"&"+pgpg.getRecurso().getSsEntidad()+":"+strKey+"="+bmd.get(strKey);
            }
        }
        
        
        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, strUrl
                            +"&"+PUN.CALL_BACK+"="+callback
//                            +"&"+PUN.START_PADDIN+"="+getCurrentPage()*PUN.CANTIDAD_PADDIN
                            +"&"+PUN.START_PADDIN+"="+0
//                            +"&"+PUN.PAGE_PADDIN+"="+PUN.CANTIDAD_PADDIN);
                            +"&"+PUN.PAGE_PADDIN+"="+0);                                                        
        
        final ModelType mt = DataPagerRemote.buildMT(pgpg);
        final String strUrlf = strUrl;
        try {
            Request request = builder.sendRequest(pgpg.getStrEntidad(), new RequestCallback() {
                @Override
                public void onError(Request request, Throwable exception) {
                   MaterialToast.fireToast("Error en el escuchador: "+strUrlf);
                }

                @Override
                public void onResponseReceived(Request request, Response response) {
                    if (200 == response.getStatusCode()) {
                        if(response.getText().indexOf("(") > 0){
                            String strCallbackId = response.getText().substring(0, response.getText().indexOf("("));
                            if(strCallbackId.equals(callback)){
                                String strJsonP = response.getText().substring(response.getText().indexOf("(")+1, response.getText().length()-1);
                                
                                MyJsonReader jr = new MyJsonReader(mt);
                                List lstB = (List<BaseModelData>)jr.read(null, strJsonP);
                                JSONObject jsonRoot = (JSONObject) JSONParser.parse((String) strJsonP);
                                JSONValue totalReg = jsonRoot.get(PUN.TOTAL_COUNT);
                                if(totalReg.isNumber() != null){
//                                    totalRows = ((Double)totalReg.isNumber().doubleValue()).intValue();
                                } 
//                                people.addAll(lstB);
                                table.setVisibleRange(0, lstB.size());
                                table.loaded(0, lstB);
                            }
                        }                                                                                                                
                    }else{

                    }
                }
            });
        } catch (RequestException e) {            
            MaterialToast.fireToast("Error la transmisión AJAX" );
        }
    }
}
