/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.user.client.rpc.IsSerializable;


/**
 *
 * @author diego
 */
public class EntidadBMD extends BaseModelData  implements IsSerializable{
    private String ssEntidad = null;

    public EntidadBMD() {        
    }

    public EntidadBMD(String ssEntidad) {
        this.ssEntidad = ssEntidad;
    }

    public String getSsEntidad() {
        return ssEntidad;
    }

    public void setSsEntidad(String ssEntidad) {
        this.ssEntidad = ssEntidad;
    }

}

