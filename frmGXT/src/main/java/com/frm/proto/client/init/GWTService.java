/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client.init;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.salpi.AdmHoraServer;
import com.frm.proto.client.salpi.AdmUsuariosAplicaLigth;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.EntidadBMD;
import com.frm.proto.client.salpi.EntidadIndepen;
import com.frm.proto.client.salpi.HijoBaseModel;
import com.frm.proto.client.salpi.MsgFinalUser;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.List;

/**
 *
 * @author diegohernandez
 */
@RemoteServiceRelativePath("gwtservice")
public interface GWTService extends RemoteService {

    public MsgFinalUser grabarEntidad(String strControlador, String strEntidad, String strModulo, List<BaseModelData> lstBaseModel,
            List<BaseModelData> lstBaseModelDelete, List<EntidadIndepen> lstDeleteIndepen);

    public MsgFinalUser grabarEntidadDina(String strControlador, String strEntidad, String strModulo, List<BaseModelData> lstBaseModel,
            List<BaseModelData> lstBaseModelDelete, List<EntidadIndepen> lstDeleteIndepen);

    public String evenSetCtr(String strControlador, String strEntidad, String strId, String strValue);

    public MsgFinalUser grabarEntidad(String strControlador, String strEntidad, String strModulo, String strFlujo, BaseModelData bmdFP ,List<HijoBaseModel> lstHBM,
            List<EntidadIndepen> lstDeleteIndepen);

    public String getUserActual();
    
    public AdmUsuariosAplicaLigth getUserActualNew();

    public AdmHoraServer getHoraServer();

    public MsgFinalUser getCerrarSession();

    public MsgFinalUser getSessionActiva();

    public MsgFinalUser getCambiarPWD(String strPwd, String strPwdNew);

    public MsgFinalUser isSessionActiva(AdmUsuariosAplicaLigth admUserActual);

    public BaseModelData getEntidad(EntidadBMD bmd, String strModulo);

    public BaseModelData getEntidadDinamica(EntidadBMD bmd, String strModulo);

    public List<BaseModelData> getEntidadList(EntidadBMD bmd, String strModulo);

    public MsgFinalUser setEmpresa(EntidadBMD bmd);

    public String getASDFASDF();
    
    public String myMethodBMDFinal(BaseModelData bmd);
    
}
