/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client.init;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.salpi.BaseModelData;


/**
 *
 * @author diego
 */
public class FormClienteSPLE {
    private String titulo;
    private String[] ids;
    private String[] titulos;
    private String[] tipoCampo;
    private boolean[] trigger;
    private boolean [] Editables;
    private String strEntidad;
    private int[] longCampos;
    private BaseModelData paretosBorrar;
        private BaseModelData bmdVL;
    private int numColumns;
    

    public FormClienteSPLE(String titulo, String[] ids, String[] titulos, String[] tipoCampo, int[] longCampos, String strEntidad
            , boolean[] Editables, BaseModelData paretosBorrar, BaseModelData bmdVL, boolean[] trigger, int numColumns) {
        this.titulo = titulo;
        this.ids = ids;
        this.titulos = titulos;
        this.tipoCampo = tipoCampo;
        this.longCampos = longCampos;
        this.strEntidad = strEntidad;
        this.Editables = Editables;
        this.paretosBorrar = paretosBorrar;
        this.bmdVL = bmdVL;
        this.trigger = trigger;
        this.numColumns = numColumns;
    }

    public boolean[] getEditables() {
        return Editables;
    }

    public void setEditables(boolean[] Editables) {
        this.Editables = Editables;
    }

    public BaseModelData getBmdVL() {
        return bmdVL;
    }

    public void setBmdVL(BaseModelData bmdVL) {
        this.bmdVL = bmdVL;
    }

    public String[] getIds() {
        return ids;
    }

    public void setIds(String[] ids) {
        this.ids = ids;
    }

    public int[] getLongCampos() {
        return longCampos;
    }

    public void setLongCampos(int[] longCampos) {
        this.longCampos = longCampos;
    }

    public BaseModelData getParetosBorrar() {
        return paretosBorrar;
    }

    public void setParetosBorrar(BaseModelData paretosBorrar) {
        this.paretosBorrar = paretosBorrar;
    }

    public String getStrEntidad() {
        return strEntidad;
    }

    public void setStrEntidad(String strEntidad) {
        this.strEntidad = strEntidad;
    }

    public String[] getTipoCampo() {
        return tipoCampo;
    }

    public void setTipoCampo(String[] tipoCampo) {
        this.tipoCampo = tipoCampo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String[] getTitulos() {
        return titulos;
    }

    public void setTitulos(String[] titulos) {
        this.titulos = titulos;
    }

    public boolean[] getTrigger() {
        return trigger;
    }

    public void setTrigger(boolean[] trigger) {
        this.trigger = trigger;
    }

    public int getNumColumns() {
        return numColumns;
    }

    public void setNumColumns(int numColumns) {
        this.numColumns = numColumns;
    }

}
