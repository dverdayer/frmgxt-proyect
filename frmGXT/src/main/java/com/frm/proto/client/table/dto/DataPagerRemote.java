/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.table.dto;
 

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.salpi.AdmUsuariosAplicaLigth;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.RegObjClient;
import com.frm.proto.client.ui.MaterialDataTableCuston;
import com.frm.proto.client.util.DataField;
import com.frm.proto.client.util.ModelType;
import com.frm.proto.client.util.MyJsonReader;
import com.frm.proto.client.util.PropsGridPG;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import gwt.material.design.client.data.DataSource;
import gwt.material.design.client.ui.MaterialToast;
import gwt.material.design.client.ui.pager.HasPager;
import gwt.material.design.client.ui.pager.MaterialDataPager;
import gwt.material.design.client.ui.pager.actions.ActionsPanel;
import gwt.material.design.client.ui.pager.actions.PageNumberBox;
import gwt.material.design.client.ui.pager.actions.PageSelection;
import gwt.material.design.client.ui.pager.actions.RowSelection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import java.util.List;
import java.util.Set;

/**
 * Material Data Pager - a simple pager for Material Data Table component
 *
 * @author kevzlou7979
 */
public class DataPagerRemote<T> extends MaterialDataPager<T> implements HasPager {

    

    protected MaterialDataTableCuston<T> table;
    protected DataSource<T> dataSource;

    protected int offset = 0;
    protected int limit = 0;
    protected int currentPage = 1;
    protected int totalRows = 0;
    protected int[] limitOptions = new int[]{20, 50, 100};

    private ActionsPanel actionsPanel = new ActionsPanel(this);
    private RowSelection rowSelection = new RowSelection(this);
    private PageSelection pageSelection;
    private List<T> data;
    private PropsGridPG pgpg;
    private ModelType mt;
    
    private BaseModelData bmdFilter;

    public DataPagerRemote() {
        super();
    }

    public DataPagerRemote(MaterialDataTableCuston<T> table, PropsGridPG pgpg) {
        this();
        this.table = table;
        this.pgpg = pgpg;
        this.mt = buildMT(pgpg); 
    }

    /**
     * Initialize the data pager for navigation
     */
    @Override
    protected void onLoad() {
        
        if (limit == 0) {
            limit = limitOptions[0];
        }
        
        super.onLoad();        
        super.remove(super.getActionsPanel());
        super.remove(super.getPageSelection());
        super.remove(super.getRowSelection());     

        add(actionsPanel);
        add(rowSelection);

        if (pageSelection == null) {
            pageSelection = new PageNumberBox(this);
        }
        add(pageSelection);

//        firstPage();
        
//        super.onLoad();   


    }

    public void updateRowsPerPage(int limit) {
        if ((totalRows / currentPage) < limit) {
            lastPage();
            return;
        }
        gotoPage(pageSelection.getValue());
    }

    @Override
    public void next() {
        currentPage++;
        gotoPage(currentPage);
    }

    @Override
    public void previous() {
        currentPage--;
        gotoPage(currentPage);
    }

    @Override
    public void lastPage() {
        gotoPage(getTotalPages());

        pageSelection.updatePageNumber(currentPage - 1);
    }

    public int getTotalPages() {
        if (isExcess()) {
            return ((totalRows / limit) + 1);
        } else {
            return totalRows / limit;
        }
    }

    @Override
    public void firstPage() {
        gotoPage(1);
    }

    @Override
    public void gotoPage(int page) {
        this.currentPage = adjustToPageLimits(page);
        doLoad((currentPage * limit) - limit, limit);
    }

    /**
     * Adjusts the input page number to the available page range
     */
    private int adjustToPageLimits(int page) {
        if (page <= 1) {
            return 1;
        } else if (page > getTotalPages()) {
            return getTotalPages();
        }

        return page;
    }

    @Override
    public int getCurrentPage() {
        return currentPage;
    }

    @Override
    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    public boolean isNext() {
        return offset + limit < totalRows;
    }

    @Override
    public boolean isPrevious() {
        return offset - limit >= 0;
    }

    @Override
    public PageSelection getPageSelection() {
        return pageSelection;
    }

    @Override
    public void setPageSelection(PageSelection pageSelection) {
        this.pageSelection = pageSelection;
        pageSelection.setPager(this);
    }

    /**
     * Set the limit as an array of options to be populated inside the rows per page listbox
     */
    public void setLimitOptions(int... limitOptions) {
        this.limitOptions = limitOptions;
    }

    /**
     * Check whether there are excess rows to be rendered with given limit
     */
    public boolean isExcess() {
        return totalRows % limit > 0;
    }

    /**
     * Check whether the pager is on the last currentPage.
     */
    public boolean isLastPage() {
        return currentPage == (totalRows / limit) + 1;
    }

    /**
     * Load the datasource within a given offset and limit
     */
    protected void doLoad(int offset, int limit) {
//        dataSource.load(new LoadConfig<>(offset, limit, table.getView().getSortContext(),
//                table.getView().getOpenCategories()), new LoadCallback<T>() {
//            @Override
//            public void onSuccess(LoadResult<T> loadResult) {
//                setOffset(loadResult.getOffset());
//                totalRows = loadResult.getTotalLength();
//                table.setVisibleRange(loadResult.getOffset(), loadResult.getData().size());
//                table.loaded(loadResult.getOffset(), loadResult.getData());
//                updateUi();
//            }
//
//            @Override
//            public void onFailure(Throwable caught) {
//                GWT.log("Load failure", caught);
//                //TODO: What we need to do on failure? May be clear table?
//            }
//        });

        this.offset = offset;
        this.limit = limit;
        getAllOrderDTO(offset);

//         this.currentPage = adjustToPageLimits(1);
//         this.offset = this.limit;
//         getAllOrderDTO(this.offset);

    }

    /**
     * Set and update the ui fields of the pager after the datasource load callback
     */
    protected void updateUi() {
        pageSelection.updatePageNumber(currentPage);
        pageSelection.updateTotalPages(getTotalPages());

        // Action label (current selection) in either the form "x-y of z" or "y of z" (when page has only 1 record)
        int firstRow = offset + 1;
        int lastRow = (isExcess() & isLastPage()) ? totalRows : (offset + limit);
        actionsPanel.getActionLabel().setText((firstRow == lastRow ? lastRow : firstRow + "-" + lastRow) + " of " + totalRows);

        actionsPanel.getIconNext().setEnabled(true);
        actionsPanel.getIconPrev().setEnabled(true);

        if (!isNext()) {
            actionsPanel.getIconNext().setEnabled(false);
        }

        if (!isPrevious()) {
            actionsPanel.getIconPrev().setEnabled(false);
        }
    }

    public DataSource<T> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public int[] getLimitOptions() {
        return limitOptions;
    }

    public ActionsPanel getActionsPanel() {
        return actionsPanel;
    }

    public void setActionsPanel(ActionsPanel actionsPanel) {
        this.actionsPanel = actionsPanel;
    }

    public RowSelection getRowSelection() {
        return rowSelection;
    }

    public void setRowSelection(RowSelection rowSelection) {
        this.rowSelection = rowSelection;
    }
    
    public void setBMDFilter(BaseModelData bmdFilter) {
        this.bmdFilter = bmdFilter;
    }
    
    public BaseModelData getBMDFilter() {
        return bmdFilter;
    }        

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public PropsGridPG getPgpg() {
        return pgpg;
    }

    public void setPgpg(PropsGridPG pgpg) {
        this.pgpg = pgpg;
    }

    public BaseModelData getBmdFilter() {
        return bmdFilter;
    }

    public void setBmdFilter(BaseModelData bmdFilter) {
        this.bmdFilter = bmdFilter;
    }

    public ModelType getMt() {
        return mt;
    }

    public void setMt(ModelType mt) {
        this.mt = mt;
    }
          
    public void getAllOrderDTO(int offset) {  
        data = new ArrayList<>();
        
        int nlCall = RegObjClient.get(PUN.CALL_BACK);
        final String callback = "jsonp_callbal"+PUN.SEPARA+nlCall;       
        RegObjClient.register(PUN.CALL_BACK, nlCall+1);    
        
        final AdmUsuariosAplicaLigth aual = (AdmUsuariosAplicaLigth) RegObjClient.get(PUN.STR_USER_ACTUAL); 
        
        String strfilter = "";
        if(bmdFilter != null){
            Set<String> setBMD = (Set)bmdFilter.getPropertyNames();
            Iterator<String> itera = setBMD.iterator();
            while(itera.hasNext()) {          
                String strKey = itera.next();                                                        
                strfilter = strfilter+"&"+strKey+"="+bmdFilter.get(strKey);
            } 
        }
        
        final String strUrl = GWT.getHostPageBaseURL()+ PUN.URL_JSON_LIST                        
                    +"&"+PUN.APLU+"="+aual.getKaNlUsuario()
                    +"&"+PUN.ENTIDAD+"="+pgpg.getStrEntidad()
                    +"&"+PUN.CONTROLADOR+"="+pgpg.getRecurso().getSsControlador()
                    +"&"+PUN.XML_RECURSO+"="+pgpg.getRecurso().getSsXml()
                    +strfilter; 
        
        
                
        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, strUrl
                            +"&"+PUN.CALL_BACK+"="+callback
//                            +"&"+PUN.START_PADDIN+"="+getCurrentPage()*PUN.CANTIDAD_PADDIN
                            +"&"+PUN.START_PADDIN+"="+offset
//                            +"&"+PUN.PAGE_PADDIN+"="+PUN.CANTIDAD_PADDIN);
                            +"&"+PUN.PAGE_PADDIN+"="+limit);
        try {
            Request request = builder.sendRequest(pgpg.getStrEntidad(), new RequestCallback() {
                @Override
                public void onError(Request request, Throwable exception) {
                   MaterialToast.fireToast("Error en el escuchador: "+strUrl);
                }

                @Override
                public void onResponseReceived(Request request, Response response) {
                    if (200 == response.getStatusCode()) {
                        if(response.getText().indexOf("(") > 0){
                            String strCallbackId = response.getText().substring(0, response.getText().indexOf("("));
                            if(strCallbackId.equals(callback)){
                                String strJsonP = response.getText().substring(response.getText().indexOf("(")+1, response.getText().length()-1);
                                
                                MyJsonReader jr = new MyJsonReader(mt);
                                List lstB = (List<BaseModelData>)jr.read(null, strJsonP);
                                JSONObject jsonRoot = (JSONObject) JSONParser.parse((String) strJsonP);
                                JSONValue totalReg = jsonRoot.get(PUN.TOTAL_COUNT);
                                if(totalReg.isNumber() != null){
                                    totalRows = ((Double)totalReg.isNumber().doubleValue()).intValue();
//                                    displayItem = PUN.CANTIDAD_PADDIN;
                                }                           
                                table.setVisibleRange(0, lstB.size());
                                table.loaded(0, lstB);
                                setData(lstB);
//                                table.setVisibleRange(0, 4);
                                updateUi();
                            }
                        }                                                                                                                
                    }else{

                    }
                }
            });
        } catch (RequestException e) {            
            MaterialToast.fireToast("Error la transmisión AJAX" );
        }
    }
    
    public static ModelType buildMT(PropsGridPG pgpg){
        ModelType type = new ModelType();
        type.setRoot(pgpg.getStrEntidad());
        type.setTotalName(PUN.TOTAL_COUNT);
        for(int i=0; i<pgpg.getIds().length; i++){
           if(pgpg.getIds()[i].split(":").length == 2 && pgpg.getIds()[i].split(":")[0].equals(pgpg.getStrEntidad())
                && (pgpg.getIds()[i].split(":")[1].equals(PUN.CAMPO_VISUAL_1) || pgpg.getIds()[i].split(":")[1].equals(PUN.CAMPO_VISUAL_2)
                || pgpg.getIds()[i].split(":")[1].equals(PUN.CAMPO_VISUAL_INT1)) ) {
                String idsFalse = pgpg.getIds()[i].split(":")[1];
                type.addField(idsFalse, idsFalse);
            }else if(pgpg.getTiposCampos()[i].split("_")[0].equals(PUN.DATE)){
                DataField date = new DataField(pgpg.getIds()[i], pgpg.getIds()[i]);
                date.setType(Date.class);
                type.addField(date);
            } else{
                type.addField(pgpg.getIds()[i], pgpg.getIds()[i]);
            }        
        }
        return type;
    }
    
    public static ModelType buildMT(String strEntidad, String[] ids, String[] tiposCampos){
        ModelType type = new ModelType();
        type.setRoot(strEntidad);
        type.setTotalName(PUN.TOTAL_COUNT);
        for(int i=0; i<ids.length; i++){
           if(ids[i].split(":").length == 2 && ids[i].split(":")[0].equals(strEntidad)
                && (ids[i].split(":")[1].equals(PUN.CAMPO_VISUAL_1) || ids[i].split(":")[1].equals(PUN.CAMPO_VISUAL_2)
                || ids[i].split(":")[1].equals(PUN.CAMPO_VISUAL_INT1)) ) {
                String idsFalse = ids[i].split(":")[1];
                type.addField(idsFalse, idsFalse);
            }else if(tiposCampos[i].split("_")[0].equals(PUN.DATE)){
                DataField date = new DataField(ids[i], ids[i]);
                date.setType(Date.class);
                type.addField(date);
            } else{
                type.addField(ids[i], ids[i]);
            }        
        }
        return type;
    }
}