/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.init;

import com.frm.proto.client.FormaGUI;
import com.frm.proto.client.RegObjClient;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.salpi.Pareto;
import com.frm.proto.client.salpi.VentanaLoad;
import com.frm.proto.client.table.dto.DataPagerRemote;
import com.frm.proto.client.ui.MaterialDataTableCuston;
import com.frm.proto.client.util.LsMethod;
import com.frm.proto.client.util.ObjWindow;
import com.frm.proto.client.util.PropsGridPG;
import com.frm.proto.client.util.SGUI;
import com.frm.proto.client.util.SWGE;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.constants.WavesType;
import gwt.material.design.client.data.events.RowSelectEvent;
import gwt.material.design.client.data.events.RowSelectHandler;
import gwt.material.design.client.ui.MaterialContainer;
import gwt.material.design.client.ui.MaterialIcon;
import gwt.material.design.client.ui.MaterialRow;
import java.util.List;

/**
 *
 * @author diego
 */
public final class FachaSWGE {
    
    public static MaterialDataTableCuston dibujarGridMaestroTipoB(PropsGridPG pgpg, MaterialContainer mcPpal){                                        
        MaterialDataTableCuston mdt = SWGE.armarGridPageMaeTipoB(pgpg, mcPpal);                                       
        
        Panel panelTop1 = mdt.getScaffolding().getToolPanel();        
//        FachaSWGE.cleanTopPanelMC(panelTop1);
        
        FachaSWGE.colocarFileUploadMC(panelTop1);
        FachaSWGE.colocarFilterMC(panelTop1);
        FachaSWGE.colocarAddMC(panelTop1);
        FachaSWGE.colocarSelectFilaMC(mdt);
        
        return mdt;
    }
    
    public static void cleanTopPanelMC(Panel panelTop1){
//        Iterator<Widget> itera = panelTop1.iterator();        
//        while (itera.hasNext()){
//            Widget w = itera.next();
//            if(!(w instanceof Panel)){
//                w.setVisible(false);
//            }            
//        }
    }
    
    public static void colocarFileUploadMC(Panel panelTop1){
        MaterialIcon mtFileDown = new MaterialIcon(IconType.FILE_DOWNLOAD);
        mtFileDown.setWaves(WavesType.LIGHT);        
        mtFileDown.setCircle(true);        
        panelTop1.add(mtFileDown);
        mtFileDown.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {                
                
                MaterialIcon macInside = (MaterialIcon)event.getSource();       
                MaterialDataTableCuston mTable = SGUI.buMCTableParent(macInside);                                 
                PropsGridPG pG = mTable.getPgpg();
                              
                DataPagerRemote dpr = SGUI.buDataPagerRemoteByTable(mTable);                                               
                BaseModelData bmd = dpr.getBMDFilter();
                
                String strParam = "";
                if(bmd != null && bmd.getPropertyNames() != null &&  bmd.getPropertyNames().size()>0){
                    for(String strKey:bmd.getPropertyNames()){
                        if(bmd.get(strKey) != null && !bmd.get(strKey).equals("")){
                            strParam = strParam+"&"+strKey+"="+bmd.get(strKey);
                        }
                    }
                }    
                
                String strIDS = "";
                String strTTS = "";
                for(int i=0; i<pG.getTitulos().length; i++){
                    strIDS = strIDS+(strIDS.equals("")?pG.getIds()[i]:PUN.SEPARA+pG.getIds()[i]);
                    strTTS = strTTS+(strTTS.equals("")?pG.getTitulos()[i]:PUN.SEPARA+pG.getTitulos()[i]);
                }                
                String srtXmlRecurso = pG.getRecurso().getSsXml();
                com.google.gwt.user.client.Window.open(GWT.getHostPageBaseURL()
                        +"/ServDownloadEntidad?type-export=export_csv"+strParam
                        +"&strIDSXXXXX="+strIDS+"&strTTSXXXXX="+strTTS
                        +"&"+PUN.XML_RECURSO+"="+srtXmlRecurso
                        +"&"+PUN.ENTIDAD+"="+pG.getStrEntidad(), "_blank"
                                , "status=0,toolbar=0,menubar=0,location=0");  
                
                
            }
        });
    }
    
    public static void colocarFilterMC(Panel panelTop1){
        MaterialIcon miFilter = new MaterialIcon(IconType.FILTER_LIST);
        miFilter.setWaves(WavesType.LIGHT);        
        miFilter.setCircle(true);        
        panelTop1.add(miFilter);  
        
        miFilter.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {                
                
                MaterialIcon macInside = (MaterialIcon)event.getSource();
                MaterialContainer mcRoot = SGUI.buMCRootParent(macInside);        
                MaterialDataTableCuston mTable = SGUI.buMCTableParent(macInside); 
                                
                PropsGridPG pG = mTable.getPgpg();
                ObjWindow om = null;
                if(RegObjClient.get(PUN.VENTANA_EMER_FILTER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad()) != null){
                    om = (ObjWindow)RegObjClient.get(PUN.VENTANA_EMER_FILTER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad());                                        
                }else{                
                    om = SWGE.armarModalFilter(mcRoot, mTable);
                    RegObjClient.register(PUN.VENTANA_EMER_FILTER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad(), om);                    
                } 
                om.getMaterialWindow().open();
            }
        });
    }
    
    
    public static void colocarAddMCGrid(Panel panelTop1, final PropsGridPG pgpgPadre){
        MaterialIcon miAdd = new MaterialIcon(IconType.ADD);
        miAdd.setWaves(WavesType.LIGHT);        
        miAdd.setCircle(true);        
        panelTop1.add(miAdd);
        
        miAdd.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                MaterialIcon macInside = (MaterialIcon)event.getSource();
                MaterialContainer mcRoot = SGUI.buMCRootParent(macInside);        
                MaterialDataTableCuston mTable = SGUI.buMCTableParent(macInside);
                
                
                                                                                
                if( pgpgPadre.getBmdVentanaLoad() == null || pgpgPadre.getBmdVentanaLoad().get(pgpgPadre.getStrEntidad()) == null){
                    // error de desarrollo.... no existe laventan load para armar la ventana emergente
                    return;
                }
                
                int height = Window.getClientHeight()-250;
                VentanaLoad vl = pgpgPadre.getBmdVentanaLoad().get(pgpgPadre.getStrEntidad());
                
                final ObjWindow om = SWGE.armarModalMAC(mcRoot, mTable, vl);
                
                PropsGridPG pgpg = new PropsGridPG(pgpgPadre.getRecurso(), vl.getStrEntidad(), vl.getVtIds(), vl.getVtTitles(), vl.getVtTiposCampos());                            
                MaterialDataTableCuston mdt = SWGE.armarGridGENE(pgpg, om.getMaterialContent(), height, GridType.PAGINADO_PPP_GRID, 0);   
                
                Panel panelTop1 = mdt.getScaffolding().getToolPanel();
                FachaSWGE.cleanTopPanelMC(panelTop1);
                FachaSWGE.colocarFilterMC(panelTop1);

                mdt.addRowSelectHandler(new RowSelectHandler() {
                    @Override
                    public void onRowSelect(RowSelectEvent event) {
                        MaterialDataTableCuston mtAct = (MaterialDataTableCuston)event.getSource();
                        BaseModelData bmd = (BaseModelData)event.getModel();

//                        List<BaseModelData> lstSelect = mtAct.getSelectedRowModels(true);
                        
                        FormaGUI frmGUI = SGUI.buGuiParent(mtAct);                                                                          
                        frmGUI.getTrigger(bmd, "1", PUN.SELECT_ROW);                                                    
                        MaterialRow mr = SGUI.buFormByWidget(macInside);

                        BaseModelData bmd1 = new BaseModelData();
                        for(Pareto p: vl.getVtPareto()){
                            if(bmd.get(p.getTarget()) != null){
                                bmd1.set(p.getSource(), bmd.get(p.getTarget()));
                            }else{
                                bmd1.set(p.getSource(), PUN.DATA_VACIO_NO_ORIGEN);
                            }
                        }                                
                        SGUI.setDataForm(mr, bmd1);
                        om.getMaterialWindow().close();                                
                    }
                });                      
                om.getMaterialWindow().open();
            }
        });
    }
    
    public static void colocarAddMC(Panel panelTop1){
        colocarAddMC(panelTop1, false);
    }
    
    public static void colocarAddMCHijo(Panel panelTop1){
        colocarAddMC(panelTop1, true);
    }
    
    static void colocarAddMC(Panel panelTop1, boolean hijo){
        MaterialIcon miAdd = new MaterialIcon(IconType.ADD);
        miAdd.setWaves(WavesType.LIGHT);        
        miAdd.setCircle(true);        
        panelTop1.add(miAdd);                        
        miAdd.addClickHandler(LsMethod.clickHAnderVentanaEmergente(hijo));
    }
        
    public static void colocarAddMCFormasGUI(Panel panelTop1, Integer tab){
        MaterialIcon miAdd = new MaterialIcon(IconType.ADD);
        miAdd.setWaves(WavesType.LIGHT);        
        miAdd.setCircle(true);        
        panelTop1.add(miAdd);                        
        miAdd.addClickHandler(LsMethod.listenerNuevoLista(tab));
    }
    
    public static void colocarSelectFilaMC(MaterialDataTableCuston mdt){
        colocarSelectFilaMC(mdt, false);
    }
    
    public static void colocarSelectFilaMCHijo(MaterialDataTableCuston mdt){
        colocarSelectFilaMC(mdt, true);
    }
    
    static void colocarSelectFilaMC(MaterialDataTableCuston mdt, boolean hijo){
        mdt.addRowSelectHandler(new RowSelectHandler() {
            @Override
            public void onRowSelect(RowSelectEvent event) {
                
//                event.getEvent().stopImmediatePropagation();
//                event.getEvent().stopPropagation();                
                MaterialDataTableCuston mtAct = (MaterialDataTableCuston)event.getSource();
                BaseModelData bmd = (BaseModelData)event.getModel();
                List<Widget> lst = mtAct.getChildrenList();
                int index = -1;
                if(lst.size() == 4){
                    DataPagerRemote<BaseModelData> pager = (DataPagerRemote<BaseModelData>)lst.get(3);
                    int i=0;
                    for(BaseModelData bmd1: pager.getData()){                        
                        if(bmd1 == bmd){
                            index = i;
                            break;
                        }
                        i++;
                    }            
                }
                
                MaterialContainer mcRoot = SGUI.buMCRootParent(mtAct);         
                PropsGridPG pG = mtAct.getPgpg();
                ObjWindow om = null;
                if(RegObjClient.get(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad()) != null){
                    om = (ObjWindow)RegObjClient.get(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad());                    
                    // get Form
                    MaterialRow mrForm = SGUI.buFormVentaEmer(om.getMaterialWindow());
                    // clean Form     
                    SGUI.setClearForm(mrForm, new BaseModelData());                    
                    SGUI.setDataForm(mrForm, bmd);
                }else{                
                    om = SWGE.armarModalMACForm(mcRoot, pG.getRecurso().getSsControlador(), pG.getStrEntidad(), mtAct, hijo);   
                    RegObjClient.register(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad(), om);
                    MaterialRow mrForm = SWGE.armarFormHijoModal(pG.getRecurso().getSsControlador(), pG.getStrEntidad()
                        , pG.getIds(), pG.getTitulos()
                        , pG.getTiposCampos(),pG.getTrigger()
                        , pG.getEditables(), pG.getGridCL(), pG.getRecurso()
                        , pG.getBmdVentanaLoad(), pG.getBmdPB()
                        , hijo);                                
                    om.getMaterialContent().add(mrForm);         
                    SGUI.setDataForm(mrForm, bmd);
                } 
                om.getMaterialWindow().open();
                
            }
        });
    }

    
}
