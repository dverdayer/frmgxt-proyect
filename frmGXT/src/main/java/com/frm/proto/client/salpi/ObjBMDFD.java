/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author diego
 */
public class ObjBMDFD implements IsSerializable{
    private String strType;
    private Integer kaNl;

    public ObjBMDFD() {
    }

    public ObjBMDFD(String strType, Integer kaNl) {
        this.strType = strType;
        this.kaNl = kaNl;
    }

    public Integer getKaNl() {
        return kaNl;
    }

    public void setKaNl(Integer kaNl) {
        this.kaNl = kaNl;
    }

    public String getStrType() {
        return strType;
    }

    public void setStrType(String strType) {
        this.strType = strType;
    }

}
