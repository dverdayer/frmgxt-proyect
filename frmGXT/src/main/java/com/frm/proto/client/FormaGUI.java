/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client;
 
import com.frm.proto.client.salpi.BaseModelData;
//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.salpi.EntidadIndepen;
import com.frm.proto.client.salpi.HijoBaseModel;
import java.util.List;

/**
 *
 * @author diego
 */
public interface FormaGUI extends GUI{
    public boolean validatorBeforeSave(String strEntidad, BaseModelData bmdFP, List<HijoBaseModel> lstHijos, List<EntidadIndepen> lstDeleteIndepen);    
    public void getTrigger(Object c, String strIdControl, String strAction);    
}
