/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.util;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.FormaGUI;
import com.frm.proto.client.RegObjClient;
import com.frm.proto.client.init.SERVICES;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.EntidadIndepen;
import com.frm.proto.client.salpi.HijoBaseModel;
import com.frm.proto.client.salpi.MsgFinalUser;
import com.frm.proto.client.salpi.NestedModelUtilDimo;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.table.dto.DataPagerRemote;
import com.frm.proto.client.ui.MaterialDataTableCuston;
import com.frm.proto.client.widget.serv.JsonBuilder;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import gwt.material.design.addins.client.autocomplete.MaterialAutoComplete;
import gwt.material.design.addins.client.combobox.events.SelectItemEvent;
import gwt.material.design.client.base.MaterialWidget;
import gwt.material.design.client.data.component.RowComponent;
import gwt.material.design.client.data.events.RowSelectEvent;
import gwt.material.design.client.data.events.RowSelectHandler;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialContainer;
import gwt.material.design.client.ui.MaterialFAB;
import gwt.material.design.client.ui.MaterialIcon;
import gwt.material.design.client.ui.MaterialRow;
import gwt.material.design.client.ui.MaterialTab;
import gwt.material.design.client.ui.MaterialTabItem;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.MaterialToast;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author diego
 */
public class LsMethod {
     public static ChangeHandler listenerDenpen(final String strValueCampoBorrar){
        ChangeHandler ch = new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {//                
                Widget w = (MaterialTextBox)event.getSource();                             
                MaterialRow frm = SGUI.getFormPanelByComponente(w);
                SGUI.borrarCampoDepen(frm, strValueCampoBorrar);
            }
        };
        return ch;
    }
     
    public static ValueChangeHandler listenerDenpenAC(final String strValueCampoBorrar){
        ValueChangeHandler ch = new ValueChangeHandler() {
//            @Override
//            public void onChange(ChangeEvent event) {//                
//                Widget w = (MaterialTextBox)event.getSource();                             
//                MaterialRow frm = SGUI.getFormPanelByComponente(w);
//                SGUI.borrarCampoDepen(frm, strValueCampoBorrar);
//            }
            @Override
            public void onValueChange(ValueChangeEvent event) {
                Widget w = (MaterialAutoComplete)event.getSource();                             
                MaterialRow frm = SGUI.getFormPanelByComponente(w);
                SGUI.borrarCampoDepen(frm, strValueCampoBorrar);
            }
        };
        return ch;
    }
     
    public static ValueChangeHandler<Boolean> listenerDenpenCHK(final String strValueCampoBorrar){
        ValueChangeHandler<Boolean> ch = new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                Widget mtb = (MaterialTextBox)event.getSource();                         
                MaterialRow frm = SGUI.getFormPanelByComponente(mtb);
                SGUI.borrarCampoDepen(frm, strValueCampoBorrar);
            }
        };
        return ch;
    }
    
    public static ChangeHandler listenerOnchange(final String strAccion){
        ChangeHandler ch = new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {//
                Widget w = (Widget)event.getSource();
                FormaGUI fgui = SGUI.buGuiParent(w);
                fgui.getTrigger(event.getSource(), w.getElement().getId(), strAccion);
            }
        };
        return ch;
    }
    
    public static ValueChangeHandler listenerOnSelect(final String strAccion){
        ValueChangeHandler  vce = new ValueChangeHandler<BaseModelData>() {
            @Override
            public void onValueChange(ValueChangeEvent<BaseModelData> event) {
                Widget w = (Widget)event.getSource();
                FormaGUI fgui = SGUI.buGuiParent(w);
                fgui.getTrigger(event.getSource(), w.getElement().getId(), strAccion);
            }
        };
        return vce;
    }
    
    public static ValueChangeHandler<Boolean> listenerOnchangeCHK(final String strAccion){
         ValueChangeHandler<Boolean> ch = new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                Widget w = (Widget)event.getSource();
                FormaGUI fgui = SGUI.buGuiParent(w);
                fgui.getTrigger(event.getSource(), w.getElement().getId(), strAccion);
            }
        };
        return ch;
    }
    
    public static RowSelectHandler listenerSetFilaSelectLista(final Integer tab){
        return new RowSelectHandler() {
            @Override
            public void onRowSelect(RowSelectEvent event) {
                MaterialDataTableCuston mtAct = (MaterialDataTableCuston)event.getSource();
                BaseModelData bmd = (BaseModelData)event.getModel();
                List<Widget> lst = mtAct.getChildrenList();
                int index = -1;
                if(lst.size() == 4){
                    DataPagerRemote<BaseModelData> pager = (DataPagerRemote<BaseModelData>)lst.get(3);
                    int i=0;
                    for(BaseModelData bmd1: pager.getData()){                        
                        if(bmd1 == bmd){
                            index = i;
                            break;
                        }
                        i++;
                    }
                }
                
                FormaGUI frmGUI = SGUI.buGuiParent(mtAct);                
                MaterialContainer mcRoot = SGUI.buMCRootParent(mtAct);                
                HTMLPanel hp = SGUI.buGRootParent(mtAct);                
                MaterialTab mtHeader = SGUI.buMTHeaderChild(hp);
                
//                String strIdItem =  ((MaterialTabItem)mtHeader.getChildrenList().get(1)).getId();
                String strIdItemSelect =  ((MaterialTabItem)mtHeader.getChildrenList().get(tab)).getId();
                mtHeader.selectTab(PUN._ITEM_TAB_BODY+strIdItemSelect.split(PUN.SEPARAY)[1]);                
                bmd.set("strTab", tab+"");
                frmGUI.getTrigger(bmd, "1", PUN.SELECT_ROW);
                
                for(int i=1; i<mtHeader.getChildrenList().size(); i++){
                                                    
                    String strIdItemItera =  ((MaterialTabItem)mtHeader.getChildrenList().get(i)).getId();                    
                    MaterialRow mrItemBody = (MaterialRow)SGUI.buWidgetChild(mcRoot, PUN._ITEM_TAB_BODY+strIdItemItera.split(PUN.SEPARAY)[1]);                                                
                    List<MaterialWidget> lstForm = SGUI.getListFormularios(mrItemBody);                                                    

                    for(MaterialWidget mv : lstForm){
                        MaterialRow mr = (MaterialRow)mv;
                        if(mr.getId().split(PUN.SEPARAY)[0].equals(PUN.FORM_TRANS)){
                            SGUI.setDataForm(mr, bmd);
                            break;
                        }
                    }

                    /// falta hacer los formularios hijos aqui
                    
                    List<MaterialWidget> lstGrid = SGUI.getListGrid(mrItemBody);
                    for(MaterialWidget mv : lstGrid){
                        MaterialDataTableCuston<BaseModelData> table = (MaterialDataTableCuston)mv;  
                        table.clearRows(true);
                        JsonBuilder.loadGridSencilloAll(table, bmd);
                    }
                }
                
            }
        };
    }
    
    public static ClickHandler listenerNuevoLista(final Integer tab){
        return new ClickHandler(){
            @Override
            public void onClick(ClickEvent event) {
//                MaterialDataTableCuston mtAct = (MaterialIcon)event.getSource();
                MaterialDataTableCuston mtAct = SGUI.buMCTableParent((MaterialIcon)event.getSource());
                BaseModelData bmd = new BaseModelData();
                
                
                FormaGUI frmGUI = SGUI.buGuiParent(mtAct);                
                MaterialContainer mcRoot = SGUI.buMCRootParent(mtAct);                
                HTMLPanel hp = SGUI.buGRootParent(mtAct);                
                MaterialTab mtHeader = SGUI.buMTHeaderChild(hp);
                
                String strIdItemSelect =  ((MaterialTabItem)mtHeader.getChildrenList().get(tab)).getId();
                mtHeader.selectTab(PUN._ITEM_TAB_BODY+strIdItemSelect.split(PUN.SEPARAY)[1]);                
                bmd.set("strTab", tab+"");
                bmd.set("strTab", tab+"");
                bmd.set(PUN.EST_REG, PUN.EST_REG_PERSISTENCE);
                frmGUI.getTrigger(bmd, "1", PUN.SELECT_ROW);
                
                for(int i=1; i<mtHeader.getChildrenList().size(); i++){
                                                    
                    String strIdItemItera =  ((MaterialTabItem)mtHeader.getChildrenList().get(tab)).getId();                    
                    MaterialRow mrItemBody = (MaterialRow)SGUI.buWidgetChild(mcRoot, PUN._ITEM_TAB_BODY+strIdItemItera.split(PUN.SEPARAY)[1]);                                                
                    List<MaterialWidget> lstForm = SGUI.getListFormularios(mrItemBody);                                                    

                    for(MaterialWidget mv : lstForm){
                        MaterialRow mr = (MaterialRow)mv;
                        SGUI.setClearForm(mr, new BaseModelData());
                        if(mr.getId().split(PUN.SEPARAY)[0].equals(PUN.FORM_TRANS)){
                            SGUI.setDataForm(mr, bmd);
                            break;
                        }
                    }
                    /// falta hacer los formularios hijos aqui                    
                    List<MaterialWidget> lstGrid = SGUI.getListGrid(mrItemBody);
                    for(MaterialWidget mv : lstGrid){
                        MaterialDataTableCuston<BaseModelData> table = (MaterialDataTableCuston)mv;  
                        table.clearRows(true);
                    }
                }
                
            }
        };
    }
    
    
    public static ClickHandler grabarTrans(final MaterialTab mtabsHeader, MaterialFAB mFab, Recurso recurso){
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {                                                                
                MaterialButton macInside = (MaterialButton)event.getSource();                                  
//                MaterialTab mtHeader = SGUI.buMTHeaderParent(macInside);
                MaterialContainer mcRoot = SGUI.buMCRootParent(macInside); 
               
                FormaGUI frmGUI = SGUI.buGuiParent(macInside);
                String strControlador = "";
                String strEntidad = "";
                BaseModelData bmdFP = null;
                List<HijoBaseModel> lstHijos = new ArrayList<HijoBaseModel>();
                List<EntidadIndepen> lstDeleteIndepenMae = null;        

                for(int i=1; i<mtabsHeader.getChildrenList().size(); i++){

                    String strIdItemItera =  ((MaterialTabItem)mtabsHeader.getChildrenList().get(i)).getId();                    
                    MaterialRow mrItemBody = (MaterialRow)SGUI.buWidgetChild(mcRoot, PUN._ITEM_TAB_BODY+strIdItemItera.split(PUN.SEPARAY)[1]);                                                
                    List<MaterialWidget> lstForm = SGUI.getListFormularios(mrItemBody);                                                    
                   
                    for(MaterialWidget mv : lstForm){
                        MaterialRow mr = (MaterialRow)mv;
                        String [] vtCompGXT = mr.getId().split(PUN.SEPARAY);
                        if(vtCompGXT.length == 3){
                            if(vtCompGXT[0].equals(PUN.FORM_TRANS)){
                                
                                strControlador = vtCompGXT[1];
                                strEntidad = vtCompGXT[2];                                
//                                if(!validacionCamposNulosReglasNEgocio(form)){                            
//                                    MessageBox.alert("Engine Validations", "Debe llenar los campos que son requeridos", null);
//                                    return;
//                                }                                
                                bmdFP = SGUI.getDataForm(mr);
                                break;
                            }else if(vtCompGXT[0].equals(PUN.FORM_TRANS_HIJO)){
//                                if(!validacionCamposNulosReglasNEgocio(form)){
//                                    MessageBox.alert("Engine Validations", "Debe llenar los campos que son requeridos en el Formulario", null);
//                                    return;
//                                }
                                BaseModelData bmdFH = SGUI.getDataForm(mr);
                                NestedModelUtilDimo.serializeClientBMD(bmdFH);
                                HijoBaseModel hbm = new HijoBaseModel();
                                hbm.setStrEntidadHijo(vtCompGXT[2]);
                                hbm.setBaseModelG(bmdFH);
                                lstHijos.add(hbm);
                            }
                        }
                    }

                    /// falta hacer los formularios hijos aqui

                    List<MaterialWidget> lstGrid = SGUI.getListGrid(mrItemBody);
                    for(MaterialWidget mv : lstGrid){
                        MaterialDataTableCuston<BaseModelData> mTable = (MaterialDataTableCuston)mv;  
                        
//                        table.setId(PUN.MATE_TABLE_HIJO+PUN.SEPARA+pgpg.getStrEntidad()+PUN.SEPARA+pgpg.getRecurso().getSsControlador());
                        
                        String [] vtCompGXT = mTable.getId().split(PUN.SEPARA);                        
                        if (vtCompGXT.length == 3 && vtCompGXT[0].equals(PUN.MATE_TABLE_HIJO)) {
                            
//                            String strMsgValidacion = validacionCamposNulosReglasNEgocio(gridSave);
//                            if(!strMsgValidacion.equals("")){
//                                TabItem ti = devolverTabItem(gridSave);
//                                TabPanel tp = devolverTab(ti);
//                                tp.setSelection(ti);
//                                MessageBox.alert("Engine Validations Client", strMsgValidacion, null);
//                                return;
//                            }

                            List<BaseModelData> lstBaseModel = new ArrayList<>();
                            for(int k=0; k<mTable.getRows().size(); k++){
                                RowComponent<BaseModelData> rc = mTable.getRow(k);
                                BaseModelData bmd1 = rc.getData();                                
                                if(bmd1.get(PUN.EST_REG) != null
                                    && (bmd1.get(PUN.EST_REG).equals(PUN.EST_REG_VENTA_EMERGE)
                                        || bmd1.get(PUN.EST_REG).equals(PUN.EST_REG_PERSISTENCE))
                                    ){    
                                    bmd1.set(PUN.EST_REG, PUN.EST_REG_PERSISTENCE);                                    
                                    lstBaseModel.add(bmd1);
                                }else if(bmd1.get(PUN.EST_REG) != null
                                    && (bmd1.get(PUN.EST_REG).equals(PUN.EST_REG_VENTA_EMERGE_MERGE)
                                        || bmd1.get(PUN.EST_REG).equals(PUN.EST_REG_MERGE))
                                    ){    
                                    bmd1.set(PUN.EST_REG, PUN.EST_REG_MERGE);
                                    lstBaseModel.add(bmd1);
                                }
                            }                                                       
                                                            
                            List<BaseModelData> lstRegDel = new ArrayList<>();
//                            if(RegObjClient.get(PUN._LST_STORE_DELE+strControlador+":"+strEntidad) != null){
//                                lstRegDel = (List<BaseModelData>)RegObjClient.get(PUN._LST_STORE_DELE+strControlador+":"+strEntidad);
                            if(RegObjClient.get(PUN._LST_STORE_DELE+strControlador+":"+vtCompGXT[2]) != null){
                                lstRegDel = (List<BaseModelData>)RegObjClient.get(PUN._LST_STORE_DELE+strControlador+":"+vtCompGXT[2]);
                                for(int k=0; k<lstRegDel.size(); k++){
                                    BaseModelData bmd = lstRegDel.get(k);                                    
                                    if(bmd.get(PUN.EST_REG) != null){
                                        String strEstReg = bmd.get(PUN.EST_REG);
                                        if(strEstReg.equals(PUN.EST_REG_PERSISTENCE)){
                                            lstRegDel.remove(k);
                                            k--;
                                        }
                                    }
                                }
                            }

                            HijoBaseModel hbm = new HijoBaseModel();
                            hbm.setStrEntidadHijo(vtCompGXT[2]);
                            NestedModelUtilDimo.serializeClient(lstBaseModel);
                            hbm.setLstBaseModelG(lstBaseModel);
                            NestedModelUtilDimo.serializeClient(lstRegDel);
                            hbm.setLstBaseModelD(lstRegDel);
                            lstHijos.add(hbm);

                        }
                        
                    }
                }
                
                List<EntidadIndepen> lstDeleteIndepen = new ArrayList<EntidadIndepen>();
                if(RegObjClient.get(PUN._LST_STORE_DELE_INDEPEN) != null){
                    List<EntidadIndepen> lstEI = RegObjClient.get(PUN._LST_STORE_DELE_INDEPEN);
                    for(int i=0; i<lstEI.size(); i++){
                        EntidadIndepen ei = lstEI.get(i);
                        if(ei.getStrRelacion().equals(strEntidad) && ei.getStrControlador().equals(strControlador)){
                            
                            BaseModelData bmd = ei.getBmd();
                            Collection<String> col =  bmd.getPropertyNames();
                            Iterator<String> iteraBMD = col.iterator();
                            while(iteraBMD.hasNext()){
                                String strKeyBMD = iteraBMD.next();
                                Object o = ei.getBmd().get(strKeyBMD);
                                if(o != null && o instanceof Boolean){
                                    if((Boolean)o){
                                        ei.getBmd().set(strKeyBMD, "S");
                                    }else{
                                        ei.getBmd().set(strKeyBMD, "N");
                                    }
                                }
                            }
                            NestedModelUtilDimo.serializeClientBMD(bmd);
                            lstDeleteIndepen.add(ei);
    //                        lstEI.remove(ei);
    //                        i=i-1;
                        }
                    }
                }
                
                
                if(frmGUI != null){
                    if(!frmGUI.validatorBeforeSave(strEntidad, bmdFP, lstHijos, lstDeleteIndepen)){
                        return;
                    }
                }else{
                    // esto es un error si pasa
//                    forG = devolverFormasArribaGUI();
//                    if(forG == null){
//                        MessageBox.alert("Error Nucleo Cliente JS frmGXT", "Error enviando los datos al Servidor Estructura no valida" , null);
//                        return;
//                    }
                }               

                final String strControladorF = strControlador;
                final String strstrEntidadF = strEntidad;

                if(lstDeleteIndepenMae != null){
                    lstDeleteIndepen.addAll(lstDeleteIndepenMae);
                }

//                final MessageBox box = MessageBox.wait("Progreso",
//                    "Guardando Datos , Por favor espere...", "Guardando...");                             
                NestedModelUtilDimo.serializeClientBMD(bmdFP);
                
                SERVICES.getService().grabarEntidad(strControladorF, strstrEntidadF, frmGUI.getStrXML(), null, bmdFP, lstHijos, lstDeleteIndepen, new AsyncCallback<MsgFinalUser>() {
//                SERVICES.getService().grabarEntidad(strControladorF, strstrEntidadF, frmGUI.getStrXML(), null, null, null, null, new AsyncCallback<MsgFinalUser>() {
                    @Override
                    public void onFailure(Throwable caught) {                       
                        MaterialToast.fireToast("error de comunicacion", 1000); 
                    }

                    @Override
                    public void onSuccess(final MsgFinalUser _mensaje) {
                        if(_mensaje.isExito()){
//                            box.close();
//                            selectTabItemLista(strstrEntidadF, strControladorF);
                            mtabsHeader.selectTab(PUN._ITEM_TAB_BODY+recurso.getSsControlador()+PUN.SEPARA+recurso.getSsEntidad()+PUN.SEPARA+"Lista");
                            mFab.setVisible(false);
                            if(RegObjClient.get(PUN._LST_STORE_DELE+strControladorF+":"+strstrEntidadF) != null){
                                List<BaseModelData> lis = (List)RegObjClient.get(PUN._LST_STORE_DELE+strControladorF+":"+strstrEntidadF);
                                RegObjClient.unregister(PUN._LST_STORE_DELE+strControladorF+":"+strstrEntidadF);
                                lis = null;
                            }


                            if(RegObjClient.get(PUN._LST_STORE_DELE_INDEPEN) != null){
                                List<EntidadIndepen> lstEI = RegObjClient.get(PUN._LST_STORE_DELE_INDEPEN);
                                for(int i=0; i<lstEI.size(); i++){
                                    EntidadIndepen ei = lstEI.get(i);
                                    if(ei.getStrRelacion().equals(strstrEntidadF) && ei.getStrControlador().equals(strControladorF)){
                                        ei.getBmd();
                                        lstEI.remove(ei);
                                        i=i-1;
                                    }
                                }
                            }

//                            InfoConfig config = new InfoConfig("", "Guardo Correctamente");                       
//                            config.display = 2000;
//                            
//                            Info i = new Info();
//                            i.setBodyStyle("background-color: #000000; color:#FFFFFF; text-align: center; border: 0x solid black; padding: 10px; font-size: 14px; font-weight: bold;");
//                            i.show(config);
//
//                            i.setAutoWidth(false);
//                            i.setWidth(i.getWidth() + 30);
//                            Point p = i.getPosition(true);
//                            p.x = ((p.x + i.getWidth()) / 2) - (i.getWidth() / 2);
//                            p.y = ((p.y + i.getHeight()) / 2) - (i.getHeight() / 2);
//                            i.setPosition(p.x, p.y);


//                            forGS.beforeLoad(null, PUN.MENU_GRABAR);

                        }else{
//                            box.close();
//                            MessageBox.alert(_mensaje.header(), _mensaje.msg() , null);
//                            forGS.beforeLoad(null, ParamUtilNames.MENU_GRABAR_FAULT);
                        }
                    }
                });
        
            }
        };
    
    }
    
    public static ClickHandler clickHAnderVentanaEmergente(boolean hijo){
        return new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {                
                MaterialIcon macInside = (MaterialIcon)event.getSource();
                MaterialContainer mcRoot = SGUI.buMCRootParent(macInside);        
                MaterialDataTableCuston mTable = SGUI.buMCTableParent(macInside);        
                      
                PropsGridPG pG = mTable.getPgpg();
                ObjWindow om = null;
                if(RegObjClient.get(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad()) != null){
                    om = (ObjWindow)RegObjClient.get(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad());
                    // get Form
                    MaterialRow mrForm = SGUI.buFormVentaEmer(om.getMaterialContent());
                    // clean Form     
                    BaseModelData bmd = new BaseModelData();
                    bmd.set(PUN.EST_REG, PUN.EST_REG_PERSISTENCE);
                    SGUI.setClearForm(mrForm, bmd);  
                }else{                
                    om = SWGE.armarModalMACForm(mcRoot, pG.getRecurso().getSsControlador(), pG.getStrEntidad(), mTable, hijo);   
                    RegObjClient.register(PUN.VENTANA_EMER+"_"+pG.getRecurso().getSsControlador()+"_"+pG.getStrEntidad(), om);
                    MaterialRow mrForm = SWGE.armarFormHijoModal(pG.getRecurso().getSsControlador(), pG.getStrEntidad()
                        , pG.getIds(), pG.getTitulos()
                        , pG.getTiposCampos(),pG.getTrigger()
                        , pG.getEditables(), pG.getGridCL(), pG.getRecurso()
                        , pG.getBmdVentanaLoad(), pG.getBmdPB()
                        , hijo);                                
                    om.getMaterialContent().add(mrForm);     
                    
                    BaseModelData bmd = new BaseModelData();
                    bmd.set(PUN.EST_REG, PUN.EST_REG_PERSISTENCE);
                    SGUI.setClearForm(mrForm, bmd);
                }                                                                                                                                                       
                om.getMaterialWindow().open();                
            }
        };
    }
}
