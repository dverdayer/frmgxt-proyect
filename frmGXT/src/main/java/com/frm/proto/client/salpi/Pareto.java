/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

/**
 *
 * @author diegohernandez
 */
public class Pareto {
    private String source;
    private String target;

    // source es la ventana padre , target es la ventana que se abre!!! la emergente
    public Pareto(String source, String target) {
        this.source = source;
        this.target = target;
    }

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }

}
