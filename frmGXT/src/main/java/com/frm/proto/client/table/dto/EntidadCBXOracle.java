/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.table.dto;

import com.frm.proto.client.salpi.AdmUsuariosAplicaLigth;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.RegObjClient;
import com.frm.proto.client.util.ModelType;
import com.frm.proto.client.util.MyJsonReader;
import com.frm.proto.client.util.PropsGridPG;
import com.frm.proto.client.widget.serv.JsonBuilder;
import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.user.client.ui.SuggestOracle;
import gwt.material.design.addins.client.autocomplete.base.MaterialSuggestionOracle;
import gwt.material.design.client.ui.MaterialRow;
import gwt.material.design.client.ui.MaterialToast;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author diego
 */
public class EntidadCBXOracle extends MaterialSuggestionOracle{

    private String strEntidad;
    private String strIdComponente;
    private MaterialRow mr;
    
    public EntidadCBXOracle(String strEntidad, String strIdComponente) {
        this.strEntidad = strEntidad;
        this.strIdComponente = strIdComponente;
    } 
    
    public EntidadCBXOracle(String strEntidad, String strIdComponente, MaterialRow mr) {
        this.strEntidad = strEntidad;
        this.strIdComponente = strIdComponente;
        this.mr = mr;
    }       
    
    @Override
    public void requestSuggestions(final SuggestOracle.Request request2, final SuggestOracle.Callback callback) {                           
        JsonBuilder.refreshEntidad(request2, callback, strEntidad, strIdComponente, mr);
//        final SuggestOracle.Response resp = new SuggestOracle.Response();
//        int offset = 0;
//        int nlCall = RegObjClient.get(PUN.CALL_BACK);
//        final String callback2 = "jsonp_callbal"+PUN.SEPARA+nlCall;       
//        RegObjClient.register(PUN.CALL_BACK, nlCall+1);    
//        
//        final AdmUsuariosAplicaLigth aual = (AdmUsuariosAplicaLigth) RegObjClient.get(PUN.STR_USER_ACTUAL); 
////        final String strUrl = GWT.getHostPageBaseURL()+ PUN.URL_JSON_LIST                        
////                    +"&"+PUN.APLU+"="+aual.getKaNlUsuario()
////                    +"&"+PUN.ENTIDAD+"="+"admUsuarios"
////                    +"&"+PUN.CONTROLADOR+"="+"CONTROLADOR_AMESTRO"     
////                    +"&"+PUN.XML_RECURSO+"="+"NINININININI";      
//        String strModuloActual =  RegObjClient.get(PUN.MODULO_ACTUAL);
//        String srtXmlRecuros = strModuloActual.split(":")[2];
//        
//        final String strEntidadI[] = strIdComponente.split(":");        
//        String text = request2.getQuery();
//        if(text != null && text.length() > 3){
//            
//            String strUrl = GWT.getHostPageBaseURL()+PUN.URL_JSON_COMBO_BOX_AJAX
//                    +PUN.XML_RECURSO+"="+srtXmlRecuros+"&"
//                    +PUN.ENTIDAD+"="+strEntidadI[0]+"&"
//                    +PUN.CAMPO_VISUAL+"="+strEntidadI[1]+"&"
//                    +PUN.QUERY+"="+text+"&"
//                    +PUN.ENTIDAD_PADRE+"="+strEntidad;
//
//
////            RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, strUrl
////                                +"&"+PUN.CALL_BACK+"="+callback2
////                                +"&"+PUN.START_PADDIN+"="+offset
////                                +"&"+PUN.PAGE_PADDIN+"="+0);
//            RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, strUrl
//                                +"&"+PUN.CALL_BACK+"="+callback2
//                                +"&"+PUN.START_PADDIN+"="+offset
//                                +"&"+PUN.PAGE_PADDIN+"="+0);
//            builder.setRequestData(strEntidad);
//            try {
//                com.google.gwt.http.client.Request request1 = builder.sendRequest(strEntidad, new RequestCallback() {
//                    @Override
//                    public void onError(com.google.gwt.http.client.Request request, Throwable exception) {
//                       MaterialToast.fireToast("Error en el escuchador: "+strUrl);
//                    }
//
//                    @Override
//                    public void onResponseReceived(com.google.gwt.http.client.Request request, com.google.gwt.http.client.Response response) {
//                        if (200 == response.getStatusCode()) {
//
//                            
////                            String strEntidad, String[] ids, String[] tiposCampos
//                            PropsGridPG pgpg = new PropsGridPG(null, strEntidadI[0], new String [] {strEntidadI[1]}
//                                    , null, new String []{PUN.CBX_NN});
//                            ModelType mt = DataPagerRemote.buildMT(pgpg);
////                            ModelType mt = DataPagerRemote.buildMT(strEntidadI[0], new String [] {strEntidadI[1]}
////                                            , new String []{PUN.CBX_NN});
//
//                            String strJsonP = response.getText().substring(response.getText().indexOf("(")+1, response.getText().length()-1);
//                            MyJsonReader jr = new MyJsonReader(mt);
//                            List<BaseModelData> lstB = (List<BaseModelData>)jr.read(null, strJsonP);
//
//                            List<EntidadCBX> list = new ArrayList<>();
//                            int i=0;
//
//                            for(BaseModelData bmd : lstB){                                
//                                    list.add(new EntidadCBX(bmd, strEntidadI[1]));                                                        
//                                i++;
//                                if(i>9){
//                                    break;
//                                }
//                            }                        
//                            resp.setSuggestions(list);
//                            callback.onSuggestionsReady(request2, resp);
//                        }else{
//
//                        }
//                    }
//                });
//            } catch (RequestException e) {            
//                MaterialToast.fireToast("Error la transmisión AJAX" );
//            }
//        }else{
//            resp.setSuggestions(new ArrayList<>());
//        }
    }
    
    
}