/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.util;

import com.frm.proto.client.RegObjClient;
import com.frm.proto.client.salpi.AdmUsuariosAplicaLigth;
import com.frm.proto.client.salpi.PUN;

/**
 *
 * @author diego
 */
public final class VarReadObject {
    
    public static String getStrXmlRecurso(){
        String strModuloActual =  RegObjClient.get(PUN.MODULO_ACTUAL);
        String srtXmlRecuros = strModuloActual.split(":")[2];
        return srtXmlRecuros;
    }
    
    public static AdmUsuariosAplicaLigth getUsuarioActual(){
        AdmUsuariosAplicaLigth aual = (AdmUsuariosAplicaLigth) RegObjClient.get(PUN.STR_USER_ACTUAL); 
        return aual;
    }
    
    
}
