/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.frm.proto.client.salpi;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;

/**
 *
 * @author diegohernandez
 */
public class HijoBaseModel implements IsSerializable{
    private String strEntidadHijo;
    private BaseModelData baseModelG;
    private List<BaseModelData> lstBaseModelG;
    private List<BaseModelData> lstBaseModelD;

    public List<BaseModelData> getLstBaseModelD() {
        return lstBaseModelD;
    }

    public void setLstBaseModelD(List<BaseModelData> lstBaseModelD) {
        this.lstBaseModelD = lstBaseModelD;
    }

    public List<BaseModelData> getLstBaseModelG() {
        return lstBaseModelG;
    }

    public void setLstBaseModelG(List<BaseModelData> lstBaseModelG) {
        this.lstBaseModelG = lstBaseModelG;
    }

    public String getStrEntidadHijo() {
        return strEntidadHijo;
    }

    public void setStrEntidadHijo(String strEntidadHijo) {
        this.strEntidadHijo = strEntidadHijo;
    }

    public BaseModelData getBaseModelG() {
        return baseModelG;
    }

    public void setBaseModelG(BaseModelData baseModelG) {
        this.baseModelG = baseModelG;
    }

}
