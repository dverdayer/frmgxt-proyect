/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.util;
 
//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.FormaGUI;
import com.frm.proto.client.salpi.BaseModelData;
import com.frm.proto.client.salpi.PUN;
import com.frm.proto.client.Permisos;
import com.frm.proto.client.RegObjClient;
import com.frm.proto.client.init.FachaSWGE;
import com.frm.proto.client.init.FormType;
import com.frm.proto.client.init.GridType;
import com.frm.proto.client.init.SERVICES;
import com.frm.proto.client.salpi.AdmUsuariosAplicaLigth;
import com.frm.proto.client.salpi.EntidadIndepen;
import com.frm.proto.client.salpi.MsgFinalUser;
import com.frm.proto.client.salpi.NestedModelUtilDimo;
import com.frm.proto.client.salpi.Pareto;
import com.frm.proto.client.salpi.VentanaLoad;
import com.frm.proto.client.widget.render.CustomRenderer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.ValueBoxBase;
import com.frm.proto.client.table.dto.DataPagerRemote;
import com.frm.proto.client.table.dto.EntidadCBX;
import com.frm.proto.client.table.dto.EntidadCBXOracle;
import com.frm.proto.client.ui.MaterialDataTableCuston;
import com.frm.proto.client.widget.serv.JsonBuilder;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.Widget;
import gwt.material.design.addins.client.autocomplete.MaterialAutoComplete;
import gwt.material.design.addins.client.autocomplete.constants.AutocompleteType;
import gwt.material.design.addins.client.combobox.MaterialComboBox;
import gwt.material.design.addins.client.dnd.MaterialDnd;
import gwt.material.design.addins.client.fileuploader.MaterialFileUploader;
import gwt.material.design.addins.client.fileuploader.base.UploadFile;
import gwt.material.design.addins.client.fileuploader.constants.FileMethod;
import gwt.material.design.addins.client.fileuploader.events.SuccessEvent;
import gwt.material.design.addins.client.window.MaterialWindow;
import gwt.material.design.client.base.MaterialWidget;
import gwt.material.design.client.constants.ButtonSize;
import gwt.material.design.client.constants.ButtonType;
import gwt.material.design.client.constants.Color;
import gwt.material.design.client.constants.HideOn;
import gwt.material.design.client.constants.IconSize;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.constants.InputType;
import gwt.material.design.client.constants.TextAlign;
import gwt.material.design.client.constants.WavesType;
import gwt.material.design.client.data.SelectionType;
import gwt.material.design.client.data.component.RowComponent;
import gwt.material.design.client.data.events.RowSelectEvent;
import gwt.material.design.client.data.events.RowSelectHandler;
import gwt.material.design.client.ui.MaterialAnchorButton;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialCheckBox;
import gwt.material.design.client.ui.MaterialColumn;
import gwt.material.design.client.ui.MaterialContainer;
import gwt.material.design.client.ui.MaterialDatePicker;
import gwt.material.design.client.ui.MaterialDoubleBox;
import gwt.material.design.client.ui.MaterialFAB;
import gwt.material.design.client.ui.MaterialFABList;
import gwt.material.design.client.ui.MaterialFooter;
import gwt.material.design.client.ui.MaterialLink;
import gwt.material.design.client.ui.MaterialListValueBox;
import gwt.material.design.client.ui.MaterialNavBar;
import gwt.material.design.client.ui.MaterialNavBrand;
import gwt.material.design.client.ui.MaterialPanel;
import gwt.material.design.client.ui.MaterialRadioButton;
import gwt.material.design.client.ui.MaterialRow;
import gwt.material.design.client.ui.MaterialTab;
import gwt.material.design.client.ui.MaterialTabItem;
import gwt.material.design.client.ui.MaterialTextArea;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.MaterialTitle;
import gwt.material.design.client.ui.MaterialToast;
import gwt.material.design.client.ui.table.cell.TextColumn;
import gwt.material.design.client.ui.table.cell.WidgetColumn;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author diego
 */
public final class SWGE {
     public static String numerosCero(String strNumerCeros){
        String[] tiposCampos = strNumerCeros.split("_");
        String strCadenaNumber = "0";
        if( tiposCampos.length > 1 && tiposCampos[2].length() > 1){
            int strNumero = Integer.valueOf(tiposCampos[2].substring(1)).intValue();
            String strCadenaNumberMini = "";
            for(int i=0; i<strNumero; i++){
                strCadenaNumberMini+="0";
            }
            strCadenaNumber = strCadenaNumberMini.equals("")?strCadenaNumber:strCadenaNumber+"."+strCadenaNumberMini;
        }
        return strCadenaNumber;
    }     
    
    public static MaterialRow armarFormPrincipal(String strControlador, String strEntidad
            , String[] ids, String[] titulos, String[] tipoCampos, boolean[] trigger
            , boolean[] vtEditables, String [] GridCL, Recurso recurso
            , BaseModelData bmdVentanaLoad, BaseModelData bmdPB){
        
        return armarFormGENE(strControlador, strEntidad, ids, titulos, tipoCampos, trigger
            , vtEditables, GridCL, recurso, bmdVentanaLoad, bmdPB, FormType._FORM_TRANS, false);
    }
    
    public static MaterialRow armarFormHijo(String strControlador, String strEntidad
            , String[] ids, String[] titulos, String[] tipoCampos, boolean[] trigger
            , boolean[] vtEditables, String [] GridCL, Recurso recurso
            , BaseModelData bmdVentanaLoad, BaseModelData bmdPB){        
        return armarFormGENE(strControlador, strEntidad, ids, titulos, tipoCampos, trigger
            , vtEditables, GridCL, recurso, bmdVentanaLoad, bmdPB, FormType._FORM_TRANS_HIJO, false);
    }
    
    public static MaterialRow armarFormHijoModal(String strControlador, String strEntidad
            , String[] ids, String[] titulos, String[] tipoCampos, boolean[] trigger
            , boolean[] vtEditables, String [] GridCL, Recurso recurso
            , BaseModelData bmdVentanaLoad, BaseModelData bmdPB, boolean hijo){        
         MaterialRow mr = armarFormGENE(strControlador, strEntidad, ids, titulos, tipoCampos, trigger
            , vtEditables, GridCL, recurso, bmdVentanaLoad, bmdPB, FormType._FORM_TRANS_HIJO, hijo);
//         mr.setPaddingTop(80);
         return mr;
    }
    
    static MaterialRow armarFormGENE(final String strControlador, final String strEntidad
            , final String[] ids, final String[] titulos, final String[] tipoCampos, final boolean[] trigger
            , final boolean[] vtEditables, final String [] GridCL, Recurso recurso
            , final BaseModelData bmdVentanaLoad, final BaseModelData bmdPB, FormType formType, boolean hijo){
                
        Permisos p = new Permisos(recurso.getNlPermisos());    
        MaterialRow mp = new MaterialRow();        
        mp.setMarginTop(10);
        List<MaterialColumn> lstMCHD = new ArrayList<>();                                                                                                      
        
        for(int i=0; i<ids.length; i++){
            
            boolean swAgregarCM = true;
            String idsR = ids[i].replace(":", "_");
            final String strId = ids[i];
            MaterialColumn mc = new MaterialColumn();            
            if(hijo){
                mc.setGrid(PUN.MEDIUM);
//                mc.setGrid(PUN.LARGE);
            }else{
                if(!tipoCampos[i].split("_")[0].equals(PUN.HDD)){
                    mc.setGrid(GridCL[i]);
                }
            }
            
            if(tipoCampos[i].split("_")[0].equals(PUN.TXT)){
                MaterialTextBox mtb = new MaterialTextBox();
                mtb.setLabel(titulos[i]);
                mtb.setId(idsR);
//                mtb.setMaxLength(10);                                                                

                if(tipoCampos[i].split("_")[1].equals(PUN.NN)){
                    mtb.setAllowBlank(false);
                }
                if(tipoCampos[i].split("_").length == 3 
                        && tipoCampos[i].split("_")[2].equals(PUN.PW)){
                    mtb.setType(InputType.PASSWORD);
                }
                if(!vtEditables[i]){
                    mtb.setEnabled(false);
                }
                
                if(bmdPB != null && bmdPB.get(ids[i]) != null){
                    final String strValueCampoBorrar = bmdPB.get(ids[i]);
                    mtb.addChangeHandler(LsMethod.listenerDenpen(strValueCampoBorrar));
                }                                
                
                if(trigger[i]){
                    mtb.addChangeHandler(LsMethod.listenerOnchange("OnChangeForm"));
                }
                mc.add(mtb);
            }else if (tipoCampos[i].split("_")[0].equals(PUN.PPP)) {
                String[] vtIds = ids[i].split(":");

                MaterialAutoComplete mac = new MaterialAutoComplete(); 
                mac.setPaddingBottom(4);
                mac.setPaddingTop(3);
                mac.setPlaceholder(titulos[i]);
                mac.setLimit(1);
                mac.setId(idsR);
                AutocompleteType at = AutocompleteType.CHIP;
                mac.setType(at);
                mac.setSuggestions(new EntidadCBXOracle(strEntidad, ids[i]));                
                if(!vtEditables[i]){
                    mac.setEnabled(false);
                }                
                if(tipoCampos[i].split("_")[1].equals(PUN.NN)){
                    mac.setAllowBlank(false);
                }                
                if(bmdPB != null && bmdPB.get(ids[i]) != null){
                    final String strValueCampoBorrar = bmdPB.get(ids[i]);
                    mac.addValueChangeHandler(LsMethod.listenerDenpenAC(strValueCampoBorrar));
                }                               
                                
                mac.addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        
                        final MaterialAutoComplete macInside = (MaterialAutoComplete)event.getSource();
                        final MaterialContainer mcRoot = SGUI.buMCRootParent(macInside);                             
                        final ObjWindow om = armarModalMAC(mcRoot);
                                                
                        BaseModelData bmdVentanaLoad1 = bmdVentanaLoad;
                        String strId1 = strId;
                        final VentanaLoad vl = (VentanaLoad)bmdVentanaLoad1.get(strId1);
                        
                        if(vl == null){
                            MaterialToast.fireToast("Evento Ventana load: error de desarrollo no tiene la ventana load ", 20000);
                        }                        
                        
                        List<? extends SuggestOracle.Suggestion> values = macInside.getValue();
                        if(values == null || values.isEmpty()){                            
                            BaseModelData bmd1 = new BaseModelData();
                            for(Pareto p: vl.getVtPareto()){                                
                                bmd1.set(p.getSource(), PUN.DATA_VACIO_NO_ORIGEN);
                            }
                            MaterialRow mrForm = SGUI.buFormByWidget(macInside);
                            SGUI.setDataForm(mrForm, bmd1);
                        }
                                                                        
                        int height = Window.getClientHeight()-260;
                        PropsGridPG pgpg = new PropsGridPG(recurso, vl.getStrEntidad(), vl.getVtIds(), vl.getVtTitles(), vl.getVtTiposCampos());                            
                        MaterialDataTableCuston mdt = SWGE.armarGridGENE(pgpg, om.getMaterialContent(), height, GridType.PAGINADO_PPP, 0);   
 
                        Panel panelTop1 = mdt.getScaffolding().getToolPanel();
                        FachaSWGE.cleanTopPanelMC(panelTop1);
                        FachaSWGE.colocarFilterMC(panelTop1);
                        FachaSWGE.colocarFileUploadMC(panelTop1);
                        
                        mdt.addRowSelectHandler(new RowSelectHandler() {
                            @Override
                            public void onRowSelect(RowSelectEvent event) {
                                MaterialDataTableCuston mtAct = (MaterialDataTableCuston)event.getSource();
                                BaseModelData bmd = (BaseModelData)event.getModel();
                                                                                                                                
                                FormaGUI frmGUI = SGUI.buGuiParent(mtAct);                                                                          
                                frmGUI.getTrigger(bmd, "1", PUN.SELECT_ROW);                                                    
                                MaterialRow mr = SGUI.buFormByWidget(macInside);
                                
                                BaseModelData bmd1 = new BaseModelData();
                                for(Pareto p: vl.getVtPareto()){
                                    if(bmd.get(p.getTarget()) != null){
                                        bmd1.set(p.getSource(), bmd.get(p.getTarget()));
                                    }else{
                                        bmd1.set(p.getSource(), PUN.DATA_VACIO_NO_ORIGEN);
                                    }
                                }                                
                                SGUI.setDataForm(mr, bmd1);
                                om.getMaterialWindow().close();                                
                            }
                        });                      
                        om.getMaterialWindow().open();                                               
                    }
                });     
                
                if(trigger[i]){
//                    mta.addChangeHandler(LsMethod.listenerOnchangeNCon("OnChangeForm"));
                }
               
                mc.add(mac); 


            }else if(tipoCampos[i].split("_")[0].equals(PUN.HDD)){

                if(tipoCampos[i].equals(PUN.HDD_NU)){
                    MaterialDoubleBox mdb = new MaterialDoubleBox();                    
                    mdb.setId(idsR);
                    mdb.setVisible(false); 
                    mc.add(mdb);
                    lstMCHD.add(mc);
                }else{
                   MaterialTextBox mtb = new MaterialTextBox();                    
                    mtb.setId(idsR);
                    mtb.setVisible(false); 
                    mc.add(mtb);
                    lstMCHD.add(mc);
                }
                swAgregarCM = false;
                     
            }else if(tipoCampos[i].split("_")[0].equals(PUN.DATE)){
                MaterialDatePicker mdp = new MaterialDatePicker();
                mdp.setPlaceholder(titulos[i]);
                mdp.setId(idsR);
                mdp.setYearsToDisplay(100);
                
                mc.add(mdp);

                if(!vtEditables[i]){
                    mdp.setEnabled(false);
                }
            }else if(tipoCampos[i].split("_")[0].equals(PUN.TXN)){
                MaterialDoubleBox mdb = new MaterialDoubleBox();
                mdb.setLabel(titulos[i]);
                mdb.setId(idsR);
                
                if(tipoCampos[i].split("_")[1].equals(PUN.NN)){
                    mdb.setAllowBlank(false);
                }
                                
                mdb.setAlignment(ValueBoxBase.TextAlignment.RIGHT); 
                
                if(!vtEditables[i]){
                    mdb.setEnabled(false);
                } 
                
                if(bmdPB != null && bmdPB.get(ids[i]) != null){
                    final String strValueCampoBorrar = bmdPB.get(ids[i]);
                    mdb.addChangeHandler(LsMethod.listenerDenpen(strValueCampoBorrar));
                }
                
                if(trigger[i]){
                    mdb.addChangeHandler(LsMethod.listenerOnchange("OnChangeForm"));
                }
                mc.add(mdb);
//            }else if(tipoCampos[i].split("_")[0].equals(PUN.SPINNERINT)){
//                SpinnerField sField = new SpinnerField();
//                sField.setFieldLabel(titulos[i]);
//                sField.setId(idsR);
//                sField.setMaxLength(sizeMax[i]);
//                if(tipoCampos[i].split("_")[1].equals(ParamUtilNames.NN)){
//                    sField.setAllowBlank(false);
//                }
//
//                sField.setIncrement(1);
//                sField.setFormat(NumberFormat.getFormat("0"));
//                if(!vtEditables[i]){
//                    sField.setEnabled(false);
//                }
//
//                if(tipoCampos[i].split("_").length == 4){
//                    sField.setValue(Integer.valueOf(tipoCampos[i].split("_")[2]));
//                    sField.setMinValue(Integer.valueOf(tipoCampos[i].split("_")[2]));
//                    sField.setMaxValue(Integer.valueOf(tipoCampos[i].split("_")[3]));
//                }
//                int posVt = iSoobra % numColumn;
//                lc = vtLC[posVt];
//                lc.add(sField, new FormData(longCampos[i], 22));
            }else if(tipoCampos[i].split("_")[0].equals(PUN.TXA)){
                MaterialTextArea mta = new MaterialTextArea();                
                mta.setLabel(titulos[i]);
                mta.setId(idsR);
                mta.setLength(10);
                
                if(tipoCampos[i].split("_")[1].equals(PUN.NN)){
                    mta.setAllowBlank(false);
                }
                if(!vtEditables[i]){
                    mta.setEnabled(false);
                }
                if(bmdPB != null && bmdPB.get(ids[i]) != null){
                    final String strValueCampoBorrar = bmdPB.get(ids[i]);
                    mta.addChangeHandler(LsMethod.listenerDenpen(strValueCampoBorrar));
                }
                
                if(trigger[i]){
                    mta.addChangeHandler(LsMethod.listenerOnchange("OnChangeForm"));
                }
                mc.add(mta);               
            }else if(tipoCampos[i].split("_")[0].equals(PUN.CBE)){
                MaterialComboBox<BaseModelData> cbx = new MaterialComboBox<>();        
                cbx.setPlaceholder("Selecione "+titulos[i]);            
                cbx.setId(idsR);
                cbx.getElement().setId(idsR);
                
                cbx.setAllowClear(true);                
                cbx.setAllowBlank(true);                
                cbx.setSingleValue(null);                
//                cbx.setTabIndex(-1);
                
//                llenarCbxFilter(lstCampos, cbx, PUN.DATA_VACIO_NO_ORIGEN);
                JsonBuilder.refreshCBE(strEntidad, ids[i], null,  cbx);
                
                if(!vtEditables[i]){
                    cbx.setEnabled(false);
                } 
                
                String tipoCamposAUX = tipoCampos[i];
                if(tipoCampos[i].split("_")[1].equals(PUN.NN)){
                    cbx.setAllowBlank(false);
                }
                
                if(bmdPB != null && bmdPB.get(ids[i]) != null){
                    final String strValueCampoBorrar = bmdPB.get(ids[i]);
//                    mta.addChangeHandler(LsMethod.listenerDenpen(strValueCampoBorrar));
                }
                
                if(trigger[i]){
                    cbx.addValueChangeHandler(LsMethod.listenerOnSelect("OnSelectForm"));
                }
                mc.add(cbx);
            }else if(tipoCampos[i].split("_")[0].equals(PUN.CBX)){
                MaterialComboBox<BaseModelData> cbx = new MaterialComboBox<>();        
                cbx.setPlaceholder("Selecione "+titulos[i]);            
                cbx.setId(idsR);
                cbx.getElement().setId(idsR);
                
//                llenarCbxFilter(lstCampos, cbx, PUN.DATA_VACIO_NO_ORIGEN);
                JsonBuilder.refreshLisboxORCbx(strEntidad, tipoCampos[i],  cbx);
                
                if(!vtEditables[i]){
                    cbx.setEnabled(false);
                } 
                
                String tipoCamposAUX = tipoCampos[i];
                if(tipoCampos[i].split("_")[1].equals(PUN.NN)){
                    cbx.setAllowBlank(false);
                }
                
                if(bmdPB != null && bmdPB.get(ids[i]) != null){
                    final String strValueCampoBorrar = bmdPB.get(ids[i]);
//                    mta.addChangeHandler(LsMethod.listenerDenpen(strValueCampoBorrar));
                }
                
                if(trigger[i]){
//                    mta.addChangeHandler(LsMethod.listenerOnchangeNCon("OnChangeForm"));
                }
                
                mc.add(cbx);
                
            }else if(tipoCampos[i].split("_")[0].equals(PUN.LBX)){
                
                final MaterialListValueBox<BaseModelData> mlb = new MaterialListValueBox<BaseModelData>();
                mlb.setId(idsR);
                mlb.setEmptyPlaceHolder(titulos[i]);
                
                JsonBuilder.refreshLisboxORCbx(strEntidad, tipoCampos[i],  mlb);
                //////
                if(!vtEditables[i]){
                    mlb.setEnabled(false);
                }                

                String tipoCamposAUX = tipoCampos[i];
                if(tipoCampos[i].split("_")[1].equals(PUN.NN)){
                    mlb.setAllowBlank(false);
                }
                
                if(bmdPB != null && bmdPB.get(ids[i]) != null){
                    final String strValueCampoBorrar = bmdPB.get(ids[i]);
//                    mta.addChangeHandler(LsMethod.listenerDenpen(strValueCampoBorrar));
                }
                
                if(trigger[i]){
//                    mta.addChangeHandler(LsMethod.listenerOnchangeNCon("OnChangeForm"));
                }
               
                mc.add(mlb); 
            }else if(tipoCampos[i].split("_")[0].equals(PUN.ADJUNTO)){
                
                MaterialFileUploader uploader = new MaterialFileUploader("/proMD3/servUploads", FileMethod.POST);
                uploader.setId(idsR);
                
                final AdmUsuariosAplicaLigth aual = (AdmUsuariosAplicaLigth) RegObjClient.get(PUN.STR_USER_ACTUAL);                 
                String str = uploader.getUrl();
                str = str+"?&"+PUN.NL_APL+"="+aual.getNlAplicacion()+"&"+PUN.NL_USER+"="+aual.getKaNlUsuario();
                uploader.setUrl(str);
        
                uploader.setPreview(false);
                uploader.setMaxFileSize(20);
                uploader.setShadow(2);
                uploader.setClickable("cu"+PUN.SEPARA+idsR);
                uploader.setHeight("auto");
        
                MaterialContainer mcAdj = new MaterialContainer();

                MaterialLink mlh = new MaterialLink("Adjunto", "black");
                mlh.setHref("#");        
                mcAdj.add(mlh);   
                
                if(!vtEditables[i]){                    
                    MaterialButton mb = new MaterialButton();
                    mb.setId("cu"+PUN.SEPARA+idsR);
                    mb.setLayoutPosition(Style.Position.ABSOLUTE);
                    mb.setTop(-10);
                    mb.setRight(25);
                    mb.setType(ButtonType.FLOATING);
                    mb.setBackgroundColor(Color.BLUE);
                    mb.setSize(ButtonSize.LARGE);
                    mb.setIconType(IconType.CLOUD_UPLOAD);
                    mb.setIconColor(Color.RED);
                    mcAdj.add(mb);
                }                  

                uploader.add(mcAdj);
                uploader.addSuccessHandler(new SuccessEvent.SuccessHandler<UploadFile>() {
                    @Override
                    public void onSuccess(SuccessEvent<UploadFile> event) {
//                        MaterialToast.fireToast("Event : Success (" + event.getTarget().getName() + ")    "+event.getResponse().getBody(), 20000);
                        MaterialFileUploader upl = (MaterialFileUploader)event.getSource();
                        String str = event.getResponse().getBody(); 
                        BaseModelData bmd = JsonBuilder.getBMDByJson(str);
                        if(bmd != null && bmd.get("mfu_exito") != null && (Boolean)bmd.get("mfu_exito")){
                            String vtValues[] = ((String)bmd.get("mfu_detailMsg")).split(PUN.SEPARA);                                                            
                            List<Widget> lstW  = upl.getChildrenList();
                            for(Widget wi:lstW){
                                if(wi instanceof MaterialContainer){
                                    MaterialContainer mca = (MaterialContainer)wi;
                                    MaterialLink mlh = (MaterialLink)mca.getChildren().get(0);                                        
                                    mlh.setText(vtValues[4]);
                                    mlh.setHref(GWT.getHostPageBaseURL()+"/ServDownloads?adjunto="+vtValues[4]);
                                    break;
                                }
                            }                                        
                        }else{
                            MaterialToast.fireToast("Error Adjuntando el archivos <br> detalle:"+event.getResponse().getBody(), 20000);
                        }                                                
                    }
                });
                
                mc.add(uploader);
                
            }else if(tipoCampos[i].split("_")[0].equals(PUN.ENTIDAD)
                    || tipoCampos[i].split("_")[0].equals(PUN.ENTIDADE)){

                MaterialAutoComplete mac = new MaterialAutoComplete(); 
                mac.setPaddingBottom(4);
                mac.setPaddingTop(3);
                mac.setPlaceholder(titulos[i]);
                mac.setLimit(1);
                mac.setId(idsR);
                AutocompleteType at = AutocompleteType.CHIP;
                mac.setType(at);
                if(tipoCampos[i].split("_")[0].equals(PUN.ENTIDAD)){
                    mac.setSuggestions(new EntidadCBXOracle(strEntidad, ids[i]));
                }else{
                    mac.setSuggestions(new EntidadCBXOracle(strEntidad, ids[i], mp));
                }
                
                String[] vtIds = ids[i].split(":");
                String idEntidad = "";
                for(int k=0; k<(vtIds.length-1); k++){
                    idEntidad=idEntidad+(idEntidad.equals("")?"":":")+vtIds[k];
                }
                
                final String idEntidadF = idEntidad;
                final String idCampoF = vtIds[vtIds.length-1];
                
                if(!vtEditables[i]){
                    mac.setEnabled(false);
                }                

                if(tipoCampos[i].split("_")[1].equals(PUN.NN)){
                    mac.setAllowBlank(false);
                }
                
                if(bmdPB != null && bmdPB.get(ids[i]) != null){
                    final String strValueCampoBorrar = bmdPB.get(ids[i]);                                       
                }
                
                if(trigger[i]){
//                    mta.addChangeHandler(LsMethod.listenerOnchangeNCon("OnChangeForm"));
                }
                
                mac.addSelectionHandler(new SelectionHandler(){
                    @Override
                    public void onSelection(SelectionEvent event) {
                        EntidadCBX ecbx = (EntidadCBX)event.getSelectedItem();
                        BaseModelData bmd = ecbx.getBmd();
                        BaseModelData bmdForm = new BaseModelData();
                        MaterialAutoComplete macIns = (MaterialAutoComplete)event.getSource();
                        
                        Set<String> setBMD = (Set)bmd.getPropertyNames();
                        Iterator<String> itera = setBMD.iterator();
                        while(itera.hasNext()) {          
                            String strKey = itera.next();                                                        
                            if(!idCampoF.equals(strKey)){
                                bmdForm.set(idEntidadF+":"+strKey, bmd.get(strKey));
                            }
//                            bmd.remove(strKey);
                        }                        
                        MaterialRow mrForm = SGUI.buFormByWidget(macIns);
                        SGUI.setDataForm(mrForm, bmdForm);
                        
                    }
                });
               
                mc.add(mac); 
                
            }else if(tipoCampos[i].split("_")[0].equals(PUN.RADIOB)){
                
                MaterialRadioButton mrbSi = new MaterialRadioButton("SI");
                mrbSi.setName(idsR);
                MaterialRadioButton mrbNo = new MaterialRadioButton("NO");
                mrbNo.setName(idsR);               
//
                if(!vtEditables[i]){
                    mrbSi.setEnabled(false);
                    mrbNo.setEnabled(false);
                }

                if(bmdPB != null && bmdPB.get(ids[i]) != null){
                    final String strValueCampoBorrar = bmdPB.get(ids[i]);
                    mrbSi.addValueChangeHandler(LsMethod.listenerDenpenCHK(strValueCampoBorrar));
                    mrbNo.addValueChangeHandler(LsMethod.listenerDenpenCHK(strValueCampoBorrar));
                }                
                
                if(trigger[i]){
                    mrbSi.addValueChangeHandler(LsMethod.listenerOnchangeCHK("OnChangeForm"));
                    mrbNo.addValueChangeHandler(LsMethod.listenerOnchangeCHK("OnChangeForm"));
                }
                
                mc.add(mrbSi);
                mc.add(mrbNo);
            }else if(tipoCampos[i].split("_")[0].equals(PUN.CHK)){
                
                MaterialCheckBox mcb = new MaterialCheckBox();
                mcb.getElement().setId(idsR);
                mcb.setName(idsR);
                mcb.setText(titulos[i]);                                
                
                if(!vtEditables[i]){
                    mcb.setEnabled(false);
                } 
                
                if(bmdPB != null && bmdPB.get(ids[i]) != null){
                    final String strValueCampoBorrar = bmdPB.get(ids[i]);
                    mcb.addValueChangeHandler(LsMethod.listenerDenpenCHK(strValueCampoBorrar));
                }                
                
                if(trigger[i]){
                    mcb.addValueChangeHandler(LsMethod.listenerOnchangeCHK("OnChangeForm"));
                }
                
                if(Window.getClientWidth() > 992){
                    Style st = mcb.getElement().getStyle();
                    st.setPaddingBottom(26, Style.Unit.PX);
                    st.setPaddingTop(25, Style.Unit.PX);
                }else{
                    Style st = mcb.getElement().getStyle();
                    st.setPaddingBottom(22, Style.Unit.PX);
                    st.setPaddingTop(22, Style.Unit.PX);
                }
//                mcb.setPaddingTop(26.0);
//                HTMLPanel mco = new HTMLPanel("<div></div>");
//                mco.setPaddingBottom(23.0);
//                mco.setPaddingTop(23.0);
//                mco.add(mcb);
//                MaterialRadioButton mcb = new MaterialRadioButton();
//                MaterialSwitch mcb = new MaterialSwitch();
//                mcb.setLabel(new Label("asdfasd"));                
//                HTMLPanel mco1 = new HTMLPanel("<span aria-hidden=\"true\" class=\"material-label\" style=\"display: none;\"></span>");
//                HTMLPanel mco2= new HTMLPanel("<span aria-hidden=\"true\" class=\"material-label\" style=\"display: none;\"></span>");
//                mc.add(mco1);
                mc.add(mcb);
//                mc.add(mco2);
            }
            if(swAgregarCM){
                mp.add(mc);
            }
        }
        
        
        
        MaterialColumn mcHDD = new MaterialColumn();            
        mcHDD.setGrid(PUN.SMALL);
        MaterialTextBox mtb = new MaterialTextBox();                    
        mtb.setId(PUN.EST_REG);
        mtb.setVisible(false); 
        mcHDD.add(mtb);
        lstMCHD.add(mcHDD);

        mcHDD = new MaterialColumn();
        mcHDD.setGrid(PUN.SMALL);
        MaterialDoubleBox mdb = new MaterialDoubleBox();                    
        mdb.setId(PUN.PK_REG_NATIVE);
        mdb.setVisible(false); 
        mcHDD.add(mdb);
        lstMCHD.add(mcHDD);
        
        
        for(MaterialColumn mc :lstMCHD){
            mc.setHideOn(HideOn.HIDE_ON_SMALL);
            mc.setVisible(false);
            mp.add(mc);
            mc.setGrid(PUN.SMALL);
        }
        if(formType.equals(FormType._FORM_TRANS)){
            mp.setId(PUN._FORM_TRANS+strControlador+PUN.SEPARAY+strEntidad);
        }else if(formType.equals(FormType._FORM_TRANS_HIJO)){
            mp.setId(PUN._FORM_TRANS_HIJO+strControlador+PUN.SEPARAY+strEntidad);
        }
        return mp;
    }
    
    
    static MaterialColumn armarComponGENE(final String strPrefi, final String id
            , final String titulo, final String tipoCampo, final String strEntidad
            , BaseModelData bmdVentanaLoad, Recurso recurso){
                                    
        String idsR = id.replace(":", "_");
        String strPrefijo = strPrefi.replace(":", "_");
        MaterialColumn mc = new MaterialColumn();
        if(tipoCampo.split("_")[0].equals(PUN.TXT)){
            MaterialTextBox mtb = new MaterialTextBox();
            mtb.setLabel(titulo);
            mtb.setId(strPrefijo+idsR);
//                mtb.setMaxLength(10);                                                                
            mc.add(mtb);
        }else if (tipoCampo.split("_")[0].equals(PUN.PPP)) {
            MaterialAutoComplete mac = new MaterialAutoComplete(); 
            mac.setPaddingTop(3);
            mac.setPaddingBottom(4);
            mac.setPlaceholder(titulo);
            mac.setLimit(1);
            mac.setId(strPrefijo+idsR);
            AutocompleteType at = AutocompleteType.CHIP;
            mac.setType(at);
            mac.setSuggestions(new EntidadCBXOracle(strEntidad, id));

            mac.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {

                    final MaterialAutoComplete macInside = (MaterialAutoComplete)event.getSource();
                    final MaterialContainer mcRoot = SGUI.buMCRootParent(macInside);                             
                    final ObjWindow om = armarModalMAC(mcRoot);

                    BaseModelData bmdVentanaLoad1 = bmdVentanaLoad;
                    String strId1 = id;
                    final VentanaLoad vl = (VentanaLoad)bmdVentanaLoad1.get(strId1);

                    if(vl == null){
                        MaterialToast.fireToast("Evento Ventana load: error de desarrollo no tiene la ventana load ", 20000);
                    }                        

                    List<? extends SuggestOracle.Suggestion> values = macInside.getValue();
                    if(values == null || values.isEmpty()){                            
                        BaseModelData bmd1 = new BaseModelData();
                        for(Pareto p: vl.getVtPareto()){                                
                            bmd1.set(p.getSource(), PUN.DATA_VACIO_NO_ORIGEN);
                        }
                        MaterialRow mrForm = SGUI.buFormByWidget(macInside);
                        SGUI.setDataForm(mrForm, bmd1, strPrefi);
                    }

                    int height = Window.getClientHeight()-250;
                    PropsGridPG pgpg = new PropsGridPG(recurso, vl.getStrEntidad(), vl.getVtIds(), vl.getVtTitles(), vl.getVtTiposCampos());                            
                    MaterialDataTableCuston mdt = SWGE.armarGridGENE(pgpg, om.getMaterialContent(), height, GridType.PAGINADO_PPP, 0);   

                    mdt.addRowSelectHandler(new RowSelectHandler() {
                        @Override
                        public void onRowSelect(RowSelectEvent event) {
                            MaterialDataTableCuston mtAct = (MaterialDataTableCuston)event.getSource();
                            BaseModelData bmd = (BaseModelData)event.getModel();

                            FormaGUI frmGUI = SGUI.buGuiParent(mtAct);                                                                          
                            frmGUI.getTrigger(bmd, "1", PUN.SELECT_ROW);                                                    
                            MaterialRow mr = SGUI.buFormByWidget(macInside);

                            BaseModelData bmd1 = new BaseModelData();
                            for(Pareto p: vl.getVtPareto()){
                                if(bmd.get(p.getTarget()) != null){
                                    bmd1.set(p.getSource(), bmd.get(p.getTarget()));
                                }else{
                                    bmd1.set(p.getSource(), PUN.DATA_VACIO_NO_ORIGEN);
                                }
                            }                                
                            SGUI.setDataForm(mr, bmd1, strPrefi);
                            om.getMaterialWindow().close();                                
                        }
                    });                      
                    om.getMaterialWindow().open();                                               
                }
            });                                                   
            mc.add(mac); 

        }else if(tipoCampo.split("_")[0].equals(PUN.DATE)){
            MaterialDatePicker mdp = new MaterialDatePicker();
            mdp.setPlaceholder(titulo);
            mdp.setId(strPrefijo+idsR);
            mdp.setYearsToDisplay(100);                
            mc.add(mdp);               

        }else if(tipoCampo.split("_")[0].equals(PUN.TXN)){
            MaterialDoubleBox mdb = new MaterialDoubleBox();
            mdb.setLabel(titulo);
            mdb.setId(strPrefijo+idsR);                                                                
            mdb.setPaddingRight(2);                                                                
            mdb.setAlignment(ValueBoxBase.TextAlignment.RIGHT);                 
            mc.add(mdb);            
        }else if(tipoCampo.split("_")[0].equals(PUN.CBX)){
        }else if(tipoCampo.split("_")[0].equals(PUN.LBX)){                
            final MaterialListValueBox<BaseModelData> mlb = new MaterialListValueBox<BaseModelData>();
            mlb.setId(strPrefijo+idsR);
            mlb.setEmptyPlaceHolder("Selecciones su parte");                
            JsonBuilder.refreshLisboxORCbx(strEntidad, tipoCampo,  mlb);               
            mc.add(mlb); 
        }else if(tipoCampo.split("_")[0].equals(PUN.ENTIDAD)
                || tipoCampo.split("_")[0].equals(PUN.ENTIDADE)){

            MaterialAutoComplete mac = new MaterialAutoComplete(); 
            mac.setPlaceholder(titulo);
            mac.setPaddingTop(3);
            mac.setPaddingBottom(4);
            mac.setLimit(1);
            mac.setId(strPrefijo+idsR);
            AutocompleteType at = AutocompleteType.CHIP;
            mac.setType(at);

            mac.setSuggestions(new EntidadCBXOracle(strEntidad, id));

            String[] vtIds = id.split(":");
            String idEntidad = "";
            for(int k=0; k<(vtIds.length-1); k++){
                idEntidad=idEntidad+(idEntidad.equals("")?"":":")+vtIds[k];
            }

            final String idEntidadF = idEntidad;
            final String idCampoF = vtIds[vtIds.length-1];               

            mac.addSelectionHandler(new SelectionHandler(){
                @Override
                public void onSelection(SelectionEvent event) {
                    EntidadCBX ecbx = (EntidadCBX)event.getSelectedItem();
                    BaseModelData bmd = ecbx.getBmd();
                    BaseModelData bmdForm = new BaseModelData();
                    MaterialAutoComplete macIns = (MaterialAutoComplete)event.getSource();

                    Set<String> setBMD = (Set)bmd.getPropertyNames();
                    Iterator<String> itera = setBMD.iterator();
                    while(itera.hasNext()) {          
                        String strKey = itera.next();                                                        
                        if(!idCampoF.equals(strKey)){
                            bmdForm.set(idEntidadF+":"+strKey, bmd.get(strKey));
                        }
//                            bmd.remove(strKey);
                    }                        
                    MaterialRow mrForm = SGUI.buFormByWidget(macIns);
                    SGUI.setDataForm(mrForm, bmdForm);

                }
            });

            mc.add(mac); 

        }else if(tipoCampo.split("_")[0].equals(PUN.CHK)){

            MaterialCheckBox mcb = new MaterialCheckBox();
            mcb.getElement().setId(strPrefijo+idsR);
            mcb.setText(titulo);                                

            if(Window.getClientWidth() > 992){
                Style st = mcb.getElement().getStyle();
                st.setPaddingBottom(26, Style.Unit.PX);
                st.setPaddingTop(26, Style.Unit.PX);
            }else{
                Style st = mcb.getElement().getStyle();
                st.setPaddingBottom(24, Style.Unit.PX);
                st.setPaddingTop(23, Style.Unit.PX);
            }
            mc.add(mcb);
        } 

        return mc;
    }
    
//    public static MaterialDataTable armarGridGENE(PropsGridPG pgpg, HTMLPanel gRoot){
    
    public static MaterialDataTableCuston armarGridGENE(PropsGridPG pgpg, MaterialWidget gRoot, int height, int tab){
        return armarGridGENE(pgpg, gRoot, height, GridType.PAGINADO_SELECT, tab);
    }
    // para las ventanas transaccionales
    public static MaterialDataTableCuston armarGridGENEPage(PropsGridPG pgpg, MaterialWidget gRoot, int tab){
        int height = Window.getClientHeight()-254;
        return armarGridGENE(pgpg, gRoot, height, GridType.PAGINADO_SELECT, tab);
    }
    
    public static MaterialDataTableCuston armarGridGENEPageOnly(PropsGridPG pgpg, MaterialWidget gRoot, int tab){
        int height = Window.getClientHeight();
        return armarGridGENE(pgpg, gRoot, height, GridType.PAGINADO_SELECT_ONLY, tab);
    }
    
    public static MaterialDataTableCuston armarGridPageMaeTipoB(PropsGridPG pgpg, MaterialWidget gRoot){
        int height = Window.getClientHeight()-210;
        return armarGridGENE(pgpg, gRoot, height, GridType.PAGINADO_MAESTRO_TIPO_B, 0);
    }
    
    public static MaterialDataTableCuston armarGridGENE(PropsGridPG pgpg, MaterialWidget gRoot
            , int height, GridType gridType, Integer tab){
         
        MaterialDataTableCuston<BaseModelData> table = new MaterialDataTableCuston<>();
//        table.setMarginTop(20);
        table.setShadow(1);
        table.setUseStickyHeader(true);
        table.setUseCategories(false);                        
        table.setVisibleRange(1, 10); 
        table.setId(PUN.MATE_TABLE+PUN.SEPARA+pgpg.getRecurso().getSsControlador()+PUN.SEPARA+pgpg.getStrEntidad());
//        table.setHeight("calc(100vh - 400px)");
//        int height = Window.getClientHeight()-140;
//        Double divi = Window.getClientHeight()/19.5;
//        table.setHeight("calc("+height+"px - 34vh)");

//        table.setHeight("300px");
//        table.getScaffolding().getTableBody().setHeight("300px");

//        table.setWidth("80%");
        table.setPaddingBottom(6);
        table.setPaddingTop(6);
        table.setPaddingRight(5);
        table.setPaddingLeft(5);
        
        if(gRoot != null){
            gRoot.add(table);
        }
             
        if(gridType.equals(GridType.PAGINADO_PPP) || gridType.equals(GridType.PAGINADO_PPP_GRID) 
                || gridType.equals(GridType.PAGINADO_SELECT)
                || gridType.equals(GridType.PAGINADO_SELECT_ONLY)
                || gridType.equals(GridType.PAGINADO_MAESTRO_TIPO_B)){ 
            DataPagerRemote<BaseModelData> pager = new DataPagerRemote<>(table, pgpg);
            table.add(pager);
            table.setPgpg(pgpg);
        }else{
            List<BaseModelData> people = new ArrayList<>();                        
            table.setRowData(0, people);
            table.setPgpg(pgpg);
            JsonBuilder.loadGridSencilloAll(table, null);
            table.setId(PUN.MATE_TABLE_HIJO+PUN.SEPARA+pgpg.getRecurso().getSsControlador()+PUN.SEPARA+pgpg.getStrEntidad());
        }
                                                               
//        table.setRowFactory(new BaseModelRowFactory());
        table.setRenderer(new CustomRenderer<>());                        
        
        if(GridType.SIMPLE_FORM.equals(gridType) || GridType.PAGINADO_MAESTRO_TIPO_B.equals(gridType)
                || GridType.SIMPLE_GRID_TO_GRID.equals(gridType)){ 
            WidgetColumn wc = new WidgetColumn<BaseModelData, MaterialButton>() {
                @Override
                public MaterialButton getValue(final BaseModelData bmd) {

                    MaterialButton mb = new MaterialButton();
                    mb.setType(ButtonType.FLOATING);
//                    mb.setIconType(IconType.REMOVE);
                    mb.setIconType(IconType.DELETE);
//                    mb.setBackgroundColor(Color.RED_LIGHTEN_1);
                    mb.setBackgroundColor(Color.BLUE_DARKEN_4);
                    if(GridType.SIMPLE_FORM.equals(gridType) || GridType.SIMPLE_GRID_TO_GRID.equals(gridType)){
                        mb.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent event) {  
                                event.getNativeEvent().stopPropagation();
    //                          MaterialToast.fireToast("evento eliminarrrrrrrr", 20000);  
                                MaterialDataTableCuston mTable = SGUI.buMCTableParent((MaterialButton)event.getSource());
                                if(bmd.get(PUN.EST_REG) != null && 
                                    (bmd.get(PUN.EST_REG).equals(PUN.PERSISTENCE) || bmd.get(PUN.EST_REG).equals(PUN.EST_REG_MERGE)) ){
                                    removeBMDtoMDCT(mTable, bmd);

                                    List<EntidadIndepen> lstInde = null;
                                    if(RegObjClient.get(PUN._LST_STORE_DELE_INDEPEN) != null){
                                        lstInde = RegObjClient.get(PUN._LST_STORE_DELE_INDEPEN);                                        
                                    }else{
                                        lstInde = new ArrayList<EntidadIndepen>();      
                                        RegObjClient.register(PUN._LST_STORE_DELE_INDEPEN, lstInde);
                                    }
                                    EntidadIndepen ei = new EntidadIndepen();
                                    ei.setBmd(bmd);
                                    bmd.set(PUN.EST_REG, PUN.EST_REG_BORRAR);
                                    ei.setStrEntidad(pgpg.getStrEntidad());
                                    ei.setStrRelacion(pgpg.getRecurso().getSsEntidad());
                                    ei.setStrControlador(pgpg.getRecurso().getSsControlador());
                                    lstInde.add(ei);
                                    
                                }else{
                                    removeBMDtoMDCT(mTable, bmd);
                                }                           
                            }
                        });
                    }else{
                        mb.addClickHandler(new ClickHandler() {
                            @Override
                            public void onClick(ClickEvent event) {  
                                event.getNativeEvent().stopPropagation();
                                MaterialDataTableCuston mTable = SGUI.buMCTableParent((MaterialButton)event.getSource());                                
                                List<BaseModelData> lstBMDDel = new ArrayList<>();
                                bmd.set(PUN.EST_REG, PUN.EST_REG_BORRAR);
                                lstBMDDel.add(bmd);
                                NestedModelUtilDimo.serializeClient(lstBMDDel);
                                SERVICES.getService().grabarEntidad(pgpg.getRecurso().getSsControlador(), pgpg.getStrEntidad()
                                        , pgpg.getRecurso().getSsRecurso(), new ArrayList<BaseModelData>()
                                        , lstBMDDel, new ArrayList<EntidadIndepen>(), new AsyncCallback<MsgFinalUser>() {
                                    @Override
                                    public void onFailure(Throwable caught) {
                                        MaterialToast.fireToast("error de comunicacion", 1000);                        
                                    }
                                    @Override
                                    public void onSuccess(final MsgFinalUser msg) {
                                        if(msg.isExito()){                                                        
                                            // cargar el grid principal                              
                                            DataPagerRemote dpr = SGUI.buDataPagerRemoteByTable(mTable);                            
                                            dpr.firstPage();
                                            MaterialToast.fireToast("Guardo Correctamente "+msg, 1000);
                                            // mostrar mensaje de guardo correctamente
                                             // cerrar la ventana 
                                        }else{
                                            // Mostrar mensaje de error        
                                            MaterialToast.fireToast("Error Guardardno "+msg, 1000);
                                        }
                                    }                    
                                });  
                            }
                        });
                    }
                    return mb;
                }
            };
            table.addColumn(wc);
        }             
        
        for (int i = 0; i<pgpg.getIds().length; i++) { 
            String [] ids = pgpg.getIds();
            String [] titulos = pgpg.getTitulos();
            String [] tipos = pgpg.getTiposCampos();

            String idsFalse = ids[i]; 
            if (ids[i].split(":").length == 2 && ids[i].split(":")[0].toUpperCase().equals(pgpg.getStrEntidad().toUpperCase())
                    && (ids[i].split(":")[1].equals(PUN.CAMPO_VISUAL_1) || ids[i].split(":")[1].equals(PUN.CAMPO_VISUAL_2)
                    || ids[i].split(":")[1].equals(PUN.CAMPO_VISUAL_INT1))) {
                idsFalse = ids[i].split(":")[1];                                                                                                                              
            } 
            final String idsFalseSID = idsFalse; 
            
            TextColumn tc = new TextColumn<BaseModelData>() {
//            table.addColumn(new TextColumn<BaseModelData>() {
                @Override    
                public Comparator<? super RowComponent<BaseModelData>> sortComparator() {                        
                    return (o1, o2) -> o1.getData().get(idsFalseSID).toString().compareToIgnoreCase(o2.getData().get(idsFalseSID).toString());
                }
                @Override
                public String getValue(BaseModelData object) {
                    if(object.get(idsFalseSID) != null){
                        return object.get(idsFalseSID).toString();
//                        return object.get(idsFalseSID).toString().substring(0,2);
                    }
                    return null;
                }
            };
            
            if (tipos[i].split("_")[0].equals(PUN.HDD)){
                tc.setHideOn(HideOn.HIDE_ON_LARGE);
            }else if (tipos[i].split("_")[0].equals(PUN.TXN)){
                tc.setTextAlign(TextAlign.RIGHT);
            }
              
            table.addColumn(tc, titulos[i]);
//        table.addColumn(new WidgetColumn<BaseModelData, MaterialImage>() {
//            @Override
//            public MaterialImage getValue(BaseModelData object) {
//                MaterialImage profile = new MaterialImage();
//                profile.setUrl("http://joashpereira.com/templates/material_one_pager/img/avatar1.png");
//                profile.setWidth("40px");
//                profile.setHeight("40px");
//                profile.setPadding(4);
//                profile.setMarginTop(8);
//                profile.setBackgroundColor(Color.GREY_LIGHTEN_2);
//                profile.setCircle(true);
//                return profile;
//            }
//        }, "Imegansitaaaaaa");
                                                
        }
                
        // con esto se coloca el panel header en azul        
        Panel panelTop = table.getScaffolding().getTopPanel();            
        panelTop.getElement().getStyle().setBackgroundColor("#1a237e");   

        Panel panelBody = table.getScaffolding().getTableBody();
//        panelBody.getElement().getStyle().setBackgroundColor("#1a237e");
        panelBody.getElement().getStyle().setHeight(height,Style.Unit.PX);
        
        if(GridType.PAGINADO_SELECT.equals(gridType)){           
            panelTop = table.getScaffolding().getToolPanel();            
//            FachaSWGE.cleanTopPanelMC(panelTop);
            FachaSWGE.colocarFileUploadMC(panelTop);
            FachaSWGE.colocarFilterMC(panelTop);
//            FachaSWGE.colocarAddMC(panelTop);         
            FachaSWGE.colocarAddMCFormasGUI(panelTop, tab);
            table.addRowSelectHandler(LsMethod.listenerSetFilaSelectLista(tab));
        }else if(GridType.PAGINADO_SELECT_ONLY.equals(gridType)){
        }else if(GridType.PAGINADO_PPP.equals(gridType)){
        }else if(GridType.PAGINADO_PPP_GRID.equals(gridType)){
            
            table.setSelectionType(SelectionType.MULTIPLE);
            
            
        }else if(GridType.PAGINADO_MAESTRO_TIPO_B.equals(gridType)){            
        }else if(GridType.SIMPLE_FORM.equals(gridType) || GridType.SIMPLE_GRID_TO_GRID.equals(gridType)){ 
            panelTop = table.getScaffolding().getToolPanel();
//            FachaSWGE.cleanTopPanelMC(panelTop);
            FachaSWGE.colocarFileUploadMC(panelTop);
            if(GridType.SIMPLE_GRID_TO_GRID.equals(gridType)){
                FachaSWGE.colocarAddMCGrid(panelTop, pgpg);                
            }else{
                FachaSWGE.colocarAddMCHijo(panelTop);
            }
            FachaSWGE.colocarSelectFilaMCHijo(table);            
        }
        table.setTitle("");
        return table;
    }

    public static void armarTabsGENE(Recurso r, MaterialRow mrPest){

//        MaterialRow mrPest = new MaterialRow(); 
        MaterialColumn mcPest = new MaterialColumn();         
        mcPest.setGrid("s12");        
        mcPest.setPadding(0);                    
        mrPest.add(mcPest);        
                  
//        mcPest.add(mtabsHeader);        
        mcPest.setId(PUN.MATE_TAB+PUN.SEPARA+r.getSsEntidad()+PUN.SEPARA+r.getSsControlador());        
//        mtabsHeader.setBackgroundColor(Color.BLACK);
//        mtabsHeader.setIndicatorColor(Color.GREEN_LIGHTEN_4);
        
//        mtabsHeader.setTabIndex(1);
        
    }
    
    /**
     * solo se condiren lao objetso de negocio con la nomencla 'NE'
     * @Recurso --> es el objecto de la navegacion de identificacion de las ventanas     
     * @mrNE2 --> es el contend pane l padre de la ventana
     * return @mrNEItem es el manejador del objeto proncipal o artefacto de navegacion
    */
    public static MaterialTab getTabsMG(Recurso r, HTMLPanel gRoot){                
        MaterialTab mtabsHeader = new MaterialTab();
        mtabsHeader.setId(PUN.TAB+PUN.SEPARAY+r.getSsControlador()+PUN.SEPARA+r.getSsEntidad());
//        mtabsHeader.setIndicatorColor(Color.GREEN_LIGHTEN_3);
        mtabsHeader.setIndicatorColor(Color.RED_ACCENT_3);
        mtabsHeader.setBackgroundColor(Color.BLUE_DARKEN_3);
        mtabsHeader.setStyle("position: fixed; z-index: 997;");
        gRoot.add(mtabsHeader);        
        return mtabsHeader;
    }
    
    /**
     * solo se condiren lao objetso de negocio con la nomencla 'NE'
     * @Recurso --> es el objecto de la navegacion de identificacion de las ventanas     
     * @mrNE2 --> es el contend pane l padre de la ventana
     * return @mrNEItem es el manejador del objeto proncipal o artefacto de navegacion
    */
    public static MaterialContainer getPestCT(Recurso r, HTMLPanel gRoot){                
        MaterialContainer mrPest = new MaterialContainer();
        mrPest.setId(PUN.MT_ROOT+PUN.SEPARAY+r.getSsControlador()+PUN.SEPARA+r.getSsEntidad());
        mrPest.setPaddingTop(46);
        gRoot.add(mrPest);
        return mrPest;
        
    }
    
    
    /**
     * solo se condiren lao objetso de negocio con la nomencla 'NE'
     * @Recurso --> es el objecto de la navegacion de identificacion de las ventanas     
     * @mrNE2 --> es el contend pane l padre de la ventana
     * return @mrNEItem es el manejador del objeto proncipal o artefacto de navegacion
    */
    public static MaterialRow getItemPestMG(Recurso r, MaterialTab mtPest, MaterialContainer mrNE
            , String strPest, String strID, boolean enabled){
        
        MaterialRow mrNEItem = new MaterialRow(); 
        MaterialTabItem mtPestItem = new MaterialTabItem();        
        mtPestItem.setId(PUN._ITEM_TAB+r.getSsControlador()+PUN.SEPARA+r.getSsEntidad()+PUN.SEPARA+strID);
        MaterialLink ml1 = new MaterialLink(strPest, "black");
        ml1.setGrid("s6");
        ml1.setHref("#"+PUN._ITEM_TAB_BODY+r.getSsControlador()+PUN.SEPARA+r.getSsEntidad()+PUN.SEPARA+strID);
        ml1.setWidth("100%");
        ml1.setTextColor(Color.WHITE);
        ml1.addStyleName("waves-effect waves-" + "default"); 
        mtPestItem.add(ml1);
        mtPest.add(mtPestItem);
        mrNEItem.setId(PUN._ITEM_TAB_BODY+r.getSsControlador()+PUN.SEPARA+r.getSsEntidad()+PUN.SEPARA+strID);        
        mrNEItem.setMarginBottom(0);
        mrNE.add(mrNEItem);       
        mtPestItem.setEnabled(enabled);
        return mrNEItem;
    }
    
    
    
    
    public static MaterialContainer armarMrPest(Recurso r){
        MaterialContainer mrPest = new MaterialContainer();
        mrPest.setId(PUN.MT_ROOT+PUN.SEPARAY+r.getSsControlador()+PUN.SEPARA+r.getSsEntidad()+PUN.SEPARA);
        mrPest.setPaddingTop(50);  
        return mrPest;
    }
    
    public static MaterialTab armarMrTabsHeader(Recurso r){
        MaterialTab mtabsHeader = new MaterialTab();
        mtabsHeader.setIndicatorColor(Color.GREEN_LIGHTEN_3);
        mtabsHeader.setBackgroundColor(Color.BLUE_DARKEN_3);
        mtabsHeader.setStyle("position: fixed; z-index: 997;");        
        mtabsHeader.setId(PUN.TAB+PUN.SEPARAY+r.getSsControlador()+PUN.SEPARA+r.getSsEntidad()+PUN.SEPARA);
        return mtabsHeader;
    }
    
    public static ObjWindow armarModalMAC(MaterialContainer mcRoot){
        return armarModalMAC( mcRoot, null, null);
    }
    
    public static ObjWindow armarModalMAC(MaterialContainer mcRoot, MaterialDataTableCuston mTablePadre, VentanaLoad vl){
        
        final MaterialWindow mm = new MaterialWindow();
        mm.setMaximize(true);
        mm.setTitle("hola");
        mcRoot.add(mm);                            
        
        MaterialPanel mmc = new MaterialPanel();                            
        mmc.setPadding(0);
        mm.add(mmc);
        
        MaterialFooter mmf = new MaterialFooter();
        mmf.setPadding(0);
        mmf.setBackgroundColor(Color.WHITE);
        mm.add(mmf);
        
        MaterialRow mr = new MaterialRow();
        mr.setFloat(Style.Float.RIGHT);
//        mr.setPaddingLeft(5);
//        mr.setPaddingRight(5);
        mmf.add(mr);
        
        MaterialButton mb = new MaterialButton(ButtonType.RAISED);
        mb.setBackgroundColor(Color.BLUE_DARKEN_4);       
        mb.setWaves(WavesType.DEFAULT);
        mb.setText("CANCELAR");
//        mmf.add(mb);
                        
        final ObjWindow om = new ObjWindow(mm, mmc, mmf);        
        
        mb.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mm.close();
            }
        }); 
        
        MaterialColumn mc1 = new MaterialColumn();
        mc1.setPadding(2);        
        mc1.add(mb);                        
        mr.add(mc1);
        
        if(mTablePadre != null && mTablePadre instanceof MaterialDataTableCuston){
            MaterialButton mbA = new MaterialButton(ButtonType.RAISED);
            mbA.setBackgroundColor(Color.BLUE_DARKEN_4);       
            mbA.setWaves(WavesType.DEFAULT);
            mbA.setText("ACEPTAR");
            
            MaterialColumn mc2 = new MaterialColumn();
            mc2.setPadding(2);
            mc2.add(mbA);
            mr.add(mc2);  
            
            om.setMaterialButtonAceptar(mbA);
            
            mbA.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {

                    MaterialDataTableCuston mtAct = (MaterialDataTableCuston)om.getMaterialContent().getChildren().get(0);
                                                    
                    /* EVENTO DE LAS FORMAS gUI*/
                    List<BaseModelData> lstSelect = mtAct.getSelectedRowModels(true);
                    FormaGUI frmGUI = SGUI.buGuiParent(mTablePadre);                                   
                    frmGUI.getTrigger(lstSelect, "2", PUN.SELECT_ROW);                      
                    
                    /* PROCESO QUE TIENE QUE CAMBIAR PARA SER MAS FUNCIONAL EL PROCESO D ELA SLISTAS*/ 
                    List<BaseModelData> lstBMD = new ArrayList<>();
                    Double pkrefMax = 0.0;
;                    for(int i=0; i<mTablePadre.getRows().size(); i++){
                        RowComponent<BaseModelData> rc = mTablePadre.getRow(i);
                        BaseModelData bmd1 = rc.getData();
                        lstBMD.add(bmd1);                        
                        
                        if(bmd1.get(PUN.PK_REG_NATIVE) == null){
                            // error desarrollo
                        }else{
                            Double pkref = bmd1.get(PUN.PK_REG_NATIVE);
                            if(pkref.doubleValue() > pkrefMax.doubleValue()){
                                pkrefMax = pkref;
                            }
                        }
                    }                    
                    /* FIN PROCESO QUE TIENE QUE CAMBIAR PAR ASER MAS FUNCIONAL EL PROCESO D ELA SLISTAS*/                                                         
                    
                    /** SE PASAN LOS VALORES DE LAS LISTA EMERGENTE A LA LISTA HIJO*/
                    List<BaseModelData> lstSelectBMD = new ArrayList<>();
                    for(BaseModelData bmd: lstSelect){
                        BaseModelData bmd1 = new BaseModelData();
                        bmd1.set(PUN.PK_REG_NATIVE,++pkrefMax);
                        bmd1.set(PUN.EST_REG, PUN.EST_REG_VENTA_EMERGE);
                        for(Pareto p: vl.getVtPareto()){
                            if(bmd.get(p.getTarget()) != null){                                
                                bmd1.set(p.getSource(), bmd.get(p.getTarget()));
                            }
                        } 
                        lstSelectBMD.add(bmd1);                                                                     
                    }
                    /** FIN SE PASAN LOS VALORES DE LAS LISTA EMERGENTE A LA LISTA HIJO*/
                    
                    
                    lstBMD.addAll(lstSelectBMD);                    
                    mTablePadre.clearRows(true);
                    mTablePadre.setVisibleRange(0, lstBMD.size());
                    mTablePadre.loaded(0, lstBMD);
                    mm.close();   
                  
                    mm.close();
                }
            });                
        }                       
        return om;
    }
        
    
    public static ObjWindow armarModalGeneric(MaterialContainer mcRoot, final String strControlador
            , final String strEntidad, final Boolean fullScreen){
        
        final MaterialWindow mv = new MaterialWindow();        
        if(fullScreen){
            mv.setMaximize(true);
        }
        mcRoot.add(mv);                            
        
        MaterialPanel mmc = new MaterialPanel();          
        mmc.setPadding(0);
        mv.add(mmc);        
        mv.setTitle(strEntidad);             
        
        MaterialFooter mmf = new MaterialFooter();       
        mmf.setPadding(0);
        mmf.setBackgroundColor(Color.WHITE);
        mv.add(mmf);                                
        
        MaterialRow mr = new MaterialRow();
        mr.setFloat(Style.Float.RIGHT);
        mmf.add(mr);
        
        MaterialButton mb = new MaterialButton(ButtonType.RAISED);
        mb.setText("CANCELAR");
        mb.setBackgroundColor(Color.BLUE_DARKEN_4);       
        mb.setWaves(WavesType.DEFAULT);        
        
        MaterialColumn mc1 = new MaterialColumn();
        mc1.setPadding(2);
        mr.add(mc1);
        mc1.add(mb);
        mb.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mv.close();
            }
        });                
                         
        MaterialButton mbA = new MaterialButton(ButtonType.RAISED);
        mbA.setText("ACEPTAR");       
        mbA.setBackgroundColor(Color.BLUE_DARKEN_4);       
        mbA.setWaves(WavesType.DEFAULT);

        MaterialColumn mc2 = new MaterialColumn();
        mc2.setPadding(2);
        mr.add(mc2);
        mc2.add(mbA);
        mbA.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mv.close();
            } 
        });                        
        return new ObjWindow(mv, mmc, mmf);
    }
        
    public static ObjWindow armarModalMACForm(MaterialContainer mcRoot, final String strControlador, final String strEntidad,
        final MaterialDataTableCuston mTable, final Boolean hijo){
        
        final MaterialWindow mm = new MaterialWindow();
        if(!hijo){
            mm.setMaximize(true);
        }
        mcRoot.add(mm);                            
        
        MaterialWidget mwc = mm.getContent();          
        mwc.setPadding(0);
        mm.setTitle(strEntidad);                               
        
        MaterialPanel mpc = new MaterialPanel();
        mpc.setHeight("200px");
        mwc.add(mpc);
        
        MaterialFooter mmf = new MaterialFooter();       
        mmf.setBackgroundColor(Color.WHITE);
//        mmf.setPadding(5);
        mwc.add(mmf);                                
        
        MaterialRow mr = new MaterialRow();
        mr.setFloat(Style.Float.RIGHT);
        mmf.add(mr);
        
        MaterialButton mb = new MaterialButton(ButtonType.RAISED);
        mb.setText("CANCELAR");
        mb.setBackgroundColor(Color.BLUE_DARKEN_4);       
        mb.setWaves(WavesType.DEFAULT);        
        
        MaterialColumn mc1 = new MaterialColumn();
        mc1.setPadding(2);
        mr.add(mc1);
        mc1.add(mb);
        mb.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mm.close();
            }
        });                
                         
        MaterialButton mbA = new MaterialButton(ButtonType.RAISED);
        mbA.setText("ACEPTAR");       
        mbA.setBackgroundColor(Color.BLUE_DARKEN_4);       
        mbA.setWaves(WavesType.DEFAULT);

        MaterialColumn mc2 = new MaterialColumn();
        mc2.setPadding(2);
        mr.add(mc2);
        mc2.add(mbA);
        mbA.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                
                MaterialWindow mm =  SGUI.buModalByWidget((Widget)event.getSource());  
                MaterialPanel mmCon = (MaterialPanel)mm.getContent().getChildren().get(0);                
                MaterialRow mrForm = SGUI.buFormVentaEmer(mmCon);
                               
                BaseModelData bmd = SGUI.getDataForm(mrForm);                               
                String strXmlRecurso = VarReadObject.getStrXmlRecurso();
                List<BaseModelData> lstBMD = new ArrayList();
                
                boolean agregar = false;
                if(bmd.get(PUN.EST_REG) != null && bmd.get(PUN.EST_REG).equals(PUN.PERSISTENCE)){
                    bmd.set(PUN.EST_REG, PUN.EST_REG_MERGE);                   
                }else if(bmd.get(PUN.PK_REG_NATIVE) == null){
                    agregar = true;
                }
                
                if(!hijo){
                    lstBMD.add(bmd);                 
                    NestedModelUtilDimo.serializeClient(lstBMD);
                    SERVICES.getService().grabarEntidad(strControlador, strEntidad, strXmlRecurso, lstBMD
                            , new ArrayList(), new ArrayList(), new AsyncCallback<MsgFinalUser>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            mm.close();
                            MaterialToast.fireToast("error de comunicacion", 1000);                        
                        }
                        @Override
                        public void onSuccess(final MsgFinalUser msg) {
                            if(msg.isExito()){                                                        
                                // cargar el grid principal                              
                                DataPagerRemote dpr = SGUI.buDataPagerRemoteByTable(mTable);                            
                                dpr.firstPage();
                                MaterialToast.fireToast("Guardo Correctamente "
                                        +msg, 1000);
                                // mostrar mensaje de guardo correctamente
                                 // cerrar la ventana 
                                mm.close();       
                            }else{
                                // Mostrar mensaje de error        
                                MaterialToast.fireToast("Error Guardardno "+msg, 1000);
                            }
                        }                    
                    });       
                }else{
// hay que mirar                     
                    Double pkrefMax = -1.0;
                    for(int i=0; i<mTable.getRows().size(); i++){
                        RowComponent<BaseModelData> rc = mTable.getRow(i);
                        BaseModelData bmd1 = rc.getData();
                        if(bmd1.get(PUN.PK_REG_NATIVE) != null && bmd.get(PUN.PK_REG_NATIVE) != null 
                                && bmd1.get(PUN.PK_REG_NATIVE).equals(bmd.get(PUN.PK_REG_NATIVE))){
                            lstBMD.add(bmd);                            
                        }else{                            
                            lstBMD.add(bmd1);
                        }
                        
                        if(bmd1.get(PUN.PK_REG_NATIVE) == null){
                            // error desarrollo
                        }else{
                            Double pkref = bmd1.get(PUN.PK_REG_NATIVE);
                            if(pkref.doubleValue() > pkrefMax.doubleValue()){
                                pkrefMax = pkref;
                            }
                        }                                                
                    }
                    if(agregar){
                        bmd.set(PUN.PK_REG_NATIVE,pkrefMax+1); 
                        lstBMD.add(bmd);
                    }
                    mTable.clearRows(true);
                    mTable.setVisibleRange(0, lstBMD.size());
                    mTable.loaded(0, lstBMD);
                    mm.close();   
                }                
            }
        });                        
        return new ObjWindow(mm, mpc, mmf);
    }
    
    public static ObjWindow armarModalFilter(MaterialContainer mcRoot, final MaterialDataTableCuston mTable){
        final String strControlador = mTable.getPgpg().getRecurso().getSsControlador();
        final String strEntidad = mTable.getPgpg().getStrEntidad();
        final MaterialWindow mm = new MaterialWindow();
        mm.setMarginTop(0);
        mm.setTitle("Filtros");
        mm.getToolbar().setFontSize("18px");
        mm.setTop(0);
                       
        final List<String> lstCampos = new ArrayList<>();        
        for(int j=0; j<mTable.getPgpg().getTitulos().length; j++){            
            if(!mTable.getPgpg().getTiposCampos()[j].contains(PUN.HDD)){
                lstCampos.add(mTable.getPgpg().getTitulos()[j]);
            }        
        }
           
        MaterialWidget mmc = mm.getContent();
        mm.getContent().setPadding(10);
        
        MaterialRow mrFormMM = new MaterialRow();        
        mrFormMM.setId(PUN._FORM_TRANS+strControlador+PUN.SEPARAY+strEntidad+PUN._FILTER);
        mmc.add(mrFormMM);
        
        MaterialColumn mcCBX1 = new MaterialColumn();
        mcCBX1.setPadding(2);
        mcCBX1.setGrid("s9 m10 l10");               
        mrFormMM.add(mcCBX1);
        
//        MaterialColumn mcCBX2 = new MaterialColumn();
//        mcCBX2.getElement().getStyle().setTextAlign(Style.TextAlign.CENTER);
//        mcCBX2.setPadding(2);
//        mcCBX2.setGrid("s3 m2 l2");
//        mrFormMM.add(mcCBX2);
        
        MaterialComboBox<String> cbx = new MaterialComboBox<>();        
        cbx.setPlaceholder("Adicione Filtro");            
        llenarCbxFilter(lstCampos, cbx, PUN.DATA_VACIO_NO_ORIGEN);
        
        cbx.setAllowClear(true);                
        cbx.setAllowBlank(true);                
        cbx.setSingleValue(null);                
        cbx.setTabIndex(-1);
        
        cbx.addSelectionHandler(selectionEvent -> {               
            MaterialPanel mmCon = (MaterialPanel)mm;                
            MaterialRow mrForm = SGUI.buFormVentaEmer(mmCon);
            MaterialComboBox<BaseModelData> cbxInside = (MaterialComboBox)selectionEvent.getSource();  
            
            for (String str : selectionEvent.getSelectedValues()) {
                BaseModelData bmd = new BaseModelData();                
                for(int i=0; i<mTable.getPgpg().getTitulos().length; i++){
                    if( str.equals(mTable.getPgpg().getTitulos()[i]) ){
                        bmd.set("id", mTable.getPgpg().getIds()[i]);
                        bmd.set("titulo", mTable.getPgpg().getTitulos()[i]);
                        bmd.set("tipo", mTable.getPgpg().getTiposCampos()[i]);
                        break;
                    }                   
                }
                MaterialColumn mc = armarComponGENE(PUN.FILTER_, bmd.get("id")
                    , bmd.get("titulo"), bmd.get("tipo"), mTable.getPgpg().getStrEntidad()
                    , mTable.getPgpg().getBmdVentanaLoad(), mTable.getPgpg().getRecurso());                                
                mc.setGrid("s9 m10 l10");                                
                mrForm.add(mc);
                
                MaterialColumn mcDlt = new MaterialColumn();
                mcDlt.getElement().getStyle().setTextAlign(Style.TextAlign.CENTER);
                mcDlt.setPadding(2);
                mcDlt.setGrid("s3 m2 l2");               
                MaterialButton mbDel = new MaterialButton();       
                mbDel.setType(ButtonType.FLOATING);
                mbDel.setIconType(IconType.DELETE);        
                mbDel.setSize(ButtonSize.LARGE);        
                mbDel.setWaves(WavesType.DEFAULT); 
                mbDel.setBackgroundColor(Color.RED_ACCENT_3); 
                mbDel.setId(PUN.FILTERBTN_+bmd.get("id").toString().replace(":", "_")); 
                mbDel.addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        MaterialButton mbDelInside = (MaterialButton)event.getSource();
                        String strIdInside = mbDelInside.getId().replace(PUN.FILTERBTN_, "");
                        
                        MaterialRow mrForm = SGUI.buFormByWidget(mbDelInside);
                        Widget w = SGUI.buWidgetChild(mrForm, PUN.FILTER_.replace(":", "_")+strIdInside.replace(":", "_"));
                        MaterialColumn wp2 =  (MaterialColumn)mbDelInside.getParent();
                        wp2.remove(mbDelInside);
                        mrForm.remove(wp2);
                        MaterialColumn wp1 =  (MaterialColumn)w.getParent();
                        wp1.remove(w);
                        mrForm.remove(wp1);
                        
                        String str = SGUI.getOnlyTitlesByComponente(w);
                        if(str != null){
                            lstCampos.add(str);
                            cbxInside.clear();
                            llenarCbxFilter(lstCampos, cbx, PUN.DATA_VACIO_NO_ORIGEN);
                            cbx.setSingleValue(null);  
                        }                        
                    }
                });                                     
                mcDlt.add(mbDel);                
                mrForm.add(mcDlt);
                
                cbxInside.clear();
                llenarCbxFilter(lstCampos, cbx, str);
                cbx.setSingleValue(null);  
            }
        });
        mcCBX1.add(cbx);
        
//        MaterialButton mbAdd = new MaterialButton();       
//        mbAdd.setType(ButtonType.FLOATING);
//        mbAdd.setIconType(IconType.ADD);        
//        mbAdd.setSize(ButtonSize.LARGE);        
//        mbAdd.setWaves(WavesType.DEFAULT);                
//        mcCBX2.add(mbAdd);
                                                      
        MaterialFooter mmf = new MaterialFooter();              
        mmf.setBackgroundColor(Color.WHITE);
        mm.add(mmf);                                
        
        MaterialRow mr = new MaterialRow();
        mr.setFloat(Style.Float.RIGHT);
        mmf.add(mr);
        
        MaterialButton mb = new MaterialButton();
        mb.setText("Cancelar");                                                
        mb.setType(ButtonType.RAISED);
        mb.setBackgroundColor(Color.BLUE_DARKEN_4);
        mb.setWaves(WavesType.DEFAULT);
        
        MaterialColumn mc1 = new MaterialColumn();
        mr.add(mc1);
        mc1.add(mb);
        mb.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                mm.close();
            }
        });                
//        mmf.add(mc1);
        
        MaterialButton mbA = new MaterialButton();
        mbA.setText("Filtrar"); 
        mbA.setType(ButtonType.RAISED);
        mbA.setBackgroundColor(Color.BLUE_DARKEN_4);
        mb.setWaves(WavesType.DEFAULT);
        
        MaterialColumn mc2 = new MaterialColumn();        
        mr.add(mc2);
        mc2.add(mbA);
        
        mbA.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {                
                MaterialRow mrForm = SGUI.buFormVentaEmer(mm);                                
                BaseModelData bmd = SGUI.getDataForm(mrForm);    
                DataPagerRemote dpr = SGUI.buDataPagerRemoteByTable(mTable);
                dpr.setBMDFilter(bmd);
                dpr.firstPage();                                                
                mm.close();                                                   
            }
        });           
        return new ObjWindow(mm, mmc, mmf);
    }
    
    public static void llenarCbxFilter(List<String> lstCampos, MaterialComboBox<String> cbx, String strDel){
        cbx.addItem("");
        for(int i=0; i<lstCampos.size(); i++){            
            if(lstCampos.get(i).equals(strDel)){
                lstCampos.remove(i);
                i--;
            }else{
                cbx.addItem(lstCampos.get(i));
            }
        }
    }
    
    public static void dibujarFabGuardar(){
        MaterialButton mbActios = new MaterialButton("te amo");        
        mbActios.setIconType(IconType.MORE_VERT);
        mbActios.setSize(ButtonSize.LARGE);
        mbActios.setBackgroundColor(Color.RED_ACCENT_3);
//        iconType="POLYMER" size="LARGE" backgroundColor="RED"
        MaterialFAB mFab = new MaterialFAB();
        mFab.add(mbActios);
        
        MaterialFABList mFabLst = new MaterialFABList();
        MaterialAnchorButton mab = new MaterialAnchorButton();
//        mab.setType(ButtonType.FLOATING);
        mab.setIconType(IconType.DONE_ALL);
//        mab.setSize(ButtonSize.LARGE);
        
        MaterialButton mab1 = new MaterialButton();
//        mab1.setType(ButtonType.FLOATING);
        mab1.setIconType(IconType.REPLAY);
//        mab1.setSize(ButtonSize.LARGE);
        
        mFabLst.add(mab1);
        mFabLst.add(mab);        
        mFab.add(mFabLst);
//        contentPanel.add(mFab);
    }

    public static void removeBMDtoMDCT(MaterialDataTableCuston mTable, BaseModelData bmd){
        List<BaseModelData> lstBMD = new ArrayList<>();
        for(int i=0; i<mTable.getRows().size(); i++){
            RowComponent<BaseModelData> rc = mTable.getRow(i);
            BaseModelData bmd1 = rc.getData();
            if(!bmd.equals(bmd1)){
                lstBMD.add(bmd1);
            }            
        }
        mTable.clearRows(true);
        mTable.setVisibleRange(0, lstBMD.size());
        mTable.loaded(0, lstBMD);
        
    }
    
    
    public static MaterialButton armarButtonGrabar(MaterialTab mtabsHeader, MaterialFAB mFab, Recurso recurso, MaterialContainer mrPest){
        MaterialButton mbActios = new MaterialButton(ButtonType.FLOATING);                
//        mbActios.setBackgroundColor(Color.RED_ACCENT_2);
        mbActios.setBackgroundColor(Color.RED_ACCENT_3);
        mbActios.setSize(ButtonSize.LARGE);
        mbActios.setIconType(IconType.DONE);
        mbActios.setIconSize(IconSize.LARGE);
               
        mFab.add(mbActios);
        mrPest.add(mFab);        
        mbActios.addClickHandler(LsMethod.grabarTrans(mtabsHeader, mFab, recurso));               
        mFab.setVisible(false);
        
        return mbActios;
    }
    
}
