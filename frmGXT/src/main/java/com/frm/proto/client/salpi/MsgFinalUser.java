/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.List;

/**
 *
 * @author diegohernandez
 */
public final class MsgFinalUser implements IsSerializable{
    private String detailMsg;
    private String evento;
    private int codigoError;
    private String entidad;
    private String recurso;
    private String modulo;
    private boolean exito;
    private String paramvalue0;
    private String paramvalue1;
    private Integer paramvalueInt0;
    private BaseModelData bmd;
    private List<BaseModelData> lstBMD;

    public MsgFinalUser(){
        codigoError = 1;
        exito = true;
    }

    public MsgFinalUser(boolean exito, String evento, String detailMsg){
        this.exito = exito;
        this.evento = evento;
        this.detailMsg = detailMsg;
    }

    public static MsgFinalUser noExito(String srtEvento, String setailMsg){
        return new MsgFinalUser(false, srtEvento, setailMsg);
    }

    public MsgFinalUser(String detailMsg, String evento, int codigoError, String entidad, String recurso, String modulo, boolean exito) {
        this.detailMsg = detailMsg;
        this.evento = evento;
        this.codigoError = codigoError;
        this.entidad = entidad;
        this.recurso = recurso;
        this.modulo = modulo;
        this.exito = exito;
    }

    public MsgFinalUser(MsgFinalUser mfu) {
        this.detailMsg = mfu.detailMsg;
        this.evento = mfu.evento;
        this.codigoError = mfu.codigoError;
        this.entidad = mfu.entidad;
        this.recurso = mfu.recurso;
        this.modulo = mfu.modulo;
        this.exito = mfu.exito;

    }

    public String header() {
        if(isExito()){
            return "<h4>"+evento!=null?evento:""+"</h4>";
        }else{
            return evento!=null?"<h4>Error en el evento "+evento+"</h4>":"";
        }
    }

    public String msg() {
        if(isExito()){
            return  evento!=null?"<h4>Ejecucion correcta del "+evento+"</h4>":"";
        }else{
            String strCodError = "<p>Codigo Error: "+codigoError+"</p><br>";
            String strEntidad = entidad!=null?"<p>Entidad: "+entidad+"</p><br>":"";
            String strRecurso = recurso!=null?"<p>Recurso: "+recurso+"</p><br>":"";
            String strModulo = modulo!=null?"<p>Modulo: "+modulo+"</p><br>":"";
            String strDetailMsg = detailMsg!=null?"<p>Message: "+detailMsg+"</p><br>":"";
            return strCodError+strEntidad+strRecurso+strModulo+strDetailMsg;
        }
        
    }

    public int getCodigoError() {
        return codigoError;
    }

    public void setCodigoError(int codigoError) {
        this.codigoError = codigoError;
    }

    public String getDetailMsg() {
        return detailMsg;
    }

    public void setDetailMsg(String detailMsg) {
        this.detailMsg = detailMsg;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getEvento() {
        return evento;
    }

    public void setEvento(String evento) {
        this.evento = evento;
    }

    public boolean isExito() {
        return exito;
    }

    public void setExito(boolean exito) {
        this.exito = exito;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getRecurso() {
        return recurso;
    }

    public void setRecurso(String recurso) {
        this.recurso = recurso;
    }

    public String getParamvalue0() {
        return paramvalue0;
}

    public void setParamvalue0(String paramvalue0) {
        this.paramvalue0 = paramvalue0;
    }

    public String getParamvalue1() {
        return paramvalue1;
    }

    public void setParamvalue1(String paramvalue1) {
        this.paramvalue1 = paramvalue1;
    }

    public BaseModelData getBmd() {
        return bmd;
    }

    public void setBmd(BaseModelData bmd) {
        this.bmd = bmd;
    }

    public List<BaseModelData> getLstBMD() {
        return lstBMD;
    }

    public void setLstBMD(List<BaseModelData> lstBMD) {
        this.lstBMD = lstBMD;
    }

    public Integer getParamvalueInt0() {
        return paramvalueInt0;
    }

    public void setParamvalueInt0(Integer paramvalueInt0) {
        this.paramvalueInt0 = paramvalueInt0;
    }
    
}
