/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.table.dto;

//import com.extjs.gxt.ui.client.data.BaseModelData;
import com.frm.proto.client.salpi.BaseModelData;
import com.google.gwt.user.client.ui.SuggestOracle;

/**
 *
 * @author diego
 */
public class EntidadCBX implements SuggestOracle.Suggestion {

    private BaseModelData bmd;
    private String strKey;

    public EntidadCBX(BaseModelData bmd, String strKey) {
        this.bmd = bmd;
        this.strKey = strKey;
    }

    @Override
    public String getDisplayString() {
        return getReplacementString();
    }

    @Override
    public String getReplacementString() {
        return bmd.get(strKey);
    }

    public BaseModelData getBmd() {
        return bmd;
    }

    public void setBmd(BaseModelData bmd) {
        this.bmd = bmd;
    }

    public String getStrKey() {
        return strKey;
    }

    public void setStrKey(String strKey) {
        this.strKey = strKey;
    }
   
}