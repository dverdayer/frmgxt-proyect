/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frm.proto.client.salpi;

/**
 *
 * @author diego
 */
/**
 *
 * @author diegohernandez
 */

/**
 * creo que el mundo al reves  el targe es la ventana load 
 * y el source es la ventana donde van a ir los camspo en referencia a campos
 */
public class VentanaLoad {
    private String strEntidad;
    private String strControlador;
    private String vtIds[];
    private String vtTitles[];
    private String vtTiposCampos[];
    private Pareto vtPareto[];
    private String [] filter;

    public VentanaLoad(){}

    public VentanaLoad(String strEntidad, String strControlador, String[] vtIds, String[] vtTitles,
            String[] vtTiposCampos, String[] filter,Pareto[] vtPareto) {
        this.strEntidad = strEntidad;
        this.strControlador = strControlador;
        this.vtIds = vtIds;
        this.vtTitles = vtTitles;        
        this.vtTiposCampos = vtTiposCampos;
        this.vtPareto = vtPareto;
        this.filter = filter;
    }

    public String getStrEntidad() {
        return strEntidad;
    }

    public void setStrEntidad(String strEntidad) {
        this.strEntidad = strEntidad;
    }

    public String[] getVtIds() {
        return vtIds;
    }

    public void setVtIds(String[] vtIds) {
        this.vtIds = vtIds;
    }

    public Pareto[] getVtPareto() {
        return vtPareto;
    }

    public void setVtPareto(Pareto[] vtPareto) {
        this.vtPareto = vtPareto;
    }

    public String[] getVtTitles() {
        return vtTitles;
    }

    public void setVtTitles(String[] vtTitles) {
        this.vtTitles = vtTitles;
    }

    public String getStrControlador() {
        return strControlador;
    }

    public void setStrControlador(String strControlador) {
        this.strControlador = strControlador;
    }   

    public String[] getVtTiposCampos() {
        return vtTiposCampos;
    }

    public void setVtTiposCampos(String[] vtTiposCampos) {
        this.vtTiposCampos = vtTiposCampos;
    }
  
    public String[] getFilter() {
        return filter;
    }

    public void setFilter(String[] filter) {
        this.filter = filter;
    }

}

